#!/usr/bin/env bash
export NODE_ENV=development && pm2 -f start src/server.js --name="WebApp" --node-args="--expose-gc"
