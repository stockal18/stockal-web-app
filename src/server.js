/* eslint no-console: 0 */

const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.js');
var bodyParser = require('body-parser');
/*var auth = require('http-auth');*/
/*var basic = auth.basic({
    realm: "private area",
    file: __dirname + "/../htpasswd"
});*/
const serverApi = require('./app/api/api.server');
const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 3000 : process.env.PORT;
const app = express();
/*app.use(auth.connect(basic));*/
app.get('/api/trending', serverApi.getTrending);
app.get('/api/signals/:code', serverApi.getSignals);
app.get('/api/info/:code', serverApi.getInfo);
app.get('/api/filters', serverApi.getFilters);
app.get('/api/allFilters', serverApi.getAllFilters);
app.get('/api/ratiotable/:code', serverApi.getRatioTable);

if (isDeveloping) {
    const compiler = webpack(config);
    const middleware = webpackMiddleware(compiler, {
        publicPath: config.output.publicPath,
        contentBase: 'src',
        stats: {
            colors: true,
            hash: false,
            timings: true,
            chunks: false,
            chunkModules: false,
            modules: false
        }
    });
    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());
    app.get('*', function response(req, res) {
        res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
        res.end();
    });
    app.use('/libs', express.static(path.join(__dirname, 'libs')));
} else {
    app.use(express.static(__dirname + '/dist'));
    app.get('*', function response(req, res) {
        res.sendFile(path.join(__dirname, 'dist/index.html'));
    });
}

app.post('/api/sendContactMail', serverApi.contactUs);
app.post('/api/sendPopupContactMail', serverApi.popupContactUs);
app.post('/api/social-login', serverApi.socialLogin);
app.post('/api/recommendation', serverApi.getRecommend);
app.post('/api/search', serverApi.getSearchResults);
app.post('/api/fullsearch', serverApi.getFullSearchResults);

app.listen(port, '0.0.0.0', function onStart(err) {
    if (err) {
        console.log(err);
    }
    console.info('==> Listening on port %s. Open up http://0.0.0.0:%s/ in your browser.', port, port);
});
