/**
 * Created by Rajan on 04/07/16.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import AppContent from './components/common/appContent.view';


/** Render **/
/**
 * Start with Routing
 */

ReactDOM.render(<AppContent/>, document.getElementById('app'));