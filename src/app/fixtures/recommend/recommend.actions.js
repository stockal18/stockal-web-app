/**
 * Created by Rajasekaran on 15/12/16.
 */
import alt from '../../../alt';

/**
 * TodoActions Class Definitions
 */
class RecommendActions {
    constructor() {
        this.generateActions(
            'fetch',
            'update',
            'loadFailedWithError',
            'clear'
        )
    }
}
export default alt.createActions(RecommendActions);