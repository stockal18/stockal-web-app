/**
 * Created by Rajasekaran on 15/12/16.
 */

import alt from '../../../../src/alt';
import RecommendActions from './recommend.actions';
import ApiClient from './../../api/api.client';
import _ from 'lodash';

class RecommendStore {
    constructor() {
        this.bindActions(RecommendActions);
        this.state = {
            recommend: [],
            loadingError: {},
            recommendLoaded: false
        };
    }

    onFetch() {
        ApiClient.getRecommend()
            .then(function (result) {
                RecommendActions.update(result.docs);
            })
            .catch(function (error) {
                RecommendActions.loadFailedWithError(error);
            });
    }

    onUpdate(update) {
        if(update){
            /** parse only the required data **/
            var items = update.map(function(item, i){
                return _.pick(item,  ['code', 'company', 'lastTradePrice', 'change', 'confidenceMeter']);
            });
            this.setState({
                    recommend: this.state.recommend.concat(items),
                    recommendLoaded: true
                }
            );
        }
    }

    onLoadFailedWithError(error) {
        this.setState({loadingError: error});
    }

    onClear() {
        this.setState({recommend: [], recommendLoaded: false});
    }
}

export default alt.createStore(RecommendStore, 'RecommendStore');
