/**
 * Created by sarovar on 11/07/16.
 */

import alt from '../../../../src/alt';
import TrendingStocksActions from './trendingstocks.actions';
import ApiClient from './../../api/api.client';
import _ from 'lodash';
/**
 * Class TodoStore Definition
 */
class TrendingStocksStore {

    /**
     * Constructor
     */
    constructor() {
        this.bindActions(TrendingStocksActions);
        this.state = {
            trendingStocks: [],
            loadingError: {},
            trendingStocksLoaded: false
        };
    }

    /**
     * Fetch Call
     */
    onFetch() {
        /** Initialize the trending stocks **/
        ApiClient.getTrendingStocks()
            .then(function (result) {
                TrendingStocksActions.update(result.docs);
            })
            .catch(function (error) {
                TrendingStocksActions.loadFailedWithError(error);
            });
    }

    /**
     * update
     * @param update
     */
    onUpdate(update) {
        if(update){
            /** parse only the required data **/
            var items = update.map(function(item, i){
                return _.pick(item,  ['code', 'company', 'lastTradePrice', 'benzingaBuyPercent', 'change']);
            });
            /** Set the state **/
            this.setState({
                    trendingStocks: this.state.trendingStocks.concat(items),
                    trendingStocksLoaded: true
                }
            );    
        }
    }
    /**
     * Load Failed with Error
     * @param error
     */
    onLoadFailedWithError(error) {
        this.setState({loadingError: error});
    }

    /**
     * Clear the trending stocks list
     */
    onClear() {
        this.setState({trendingStocks: [], trendingStocksLoaded: false});
    }
}

export default alt.createStore(TrendingStocksStore, 'TrendingStocksStore');