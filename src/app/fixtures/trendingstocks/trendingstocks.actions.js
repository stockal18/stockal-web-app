/**
 * Created by sarovar on 11/07/16.
 */
import alt from '../../../alt';

/**
 * TodoActions Class Definitions
 */
class TrendingStocksActions {
    constructor() {
        this.generateActions(
            'fetch',
            'update',
            'loadFailedWithError',
            'clear'
        )
    }
}

export default alt.createActions(TrendingStocksActions);