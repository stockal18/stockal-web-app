/**
 * Created by prayag on 04/08/16.
 */

import alt from '../../../../src/alt';
import SignalsActions from './signals.actions';
import ApiClient from './../../api/api.client';
import _ from 'lodash';
/**
 * Class TodoStore Definition
 */
class SignalsStore {

    /**
     * Constructor
     */
    constructor() {
        this.bindActions(SignalsActions);
        this.state = {
            stockData: {},
            loadingError: {},
            signalsLoaded: false
        };
    }

    /**
     * Fetch Call
     */
    onFetch(code) {
        /** Initialize the trending stocks **/
        ApiClient.getSignals(code)
            .then(function (result) {
                SignalsActions.update(result.docs);
            })
            .catch(function (error) {
                SignalsActions.loadFailedWithError(error);
            });
    }

    /**
     * update
     * @param update
     */
    onUpdate(update) {
        if(update){
            /** parse only the required data **/
            var data = _.pick(update[0],  ['code', 'company', 'lastTradePrice', 'change', 'averageTargetPrice', 'benzingaBuyPercent', 'historicalPrice', 'socialVelocity', 'sentimentIndex', 'revenuePrediction', 'estimize']);
            /** Set the state **/
            this.setState({
                    stockData: data,
                    signalsLoaded: true
                }
            );
        }
    }

    /**
     * Load Failed with Error
     * @param error
     */
    onLoadFailedWithError(error) {
        this.setState({loadingError: error});
    }

    /**
     * Clear the trending stocks list
     */
    onClear() {
        this.setState({stockData: {}, signalsLoaded: false});
    }
}

export default alt.createStore(SignalsStore, 'SignalsStore');