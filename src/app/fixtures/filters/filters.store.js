/**
 * Created by prayag on 19/12/16.
 */

import alt from './../../../../src/alt';
import FiltersActions from './filters.actions';
import ApiClient from './../../api/api.client';

/**
 * Class TodoStore Definition
 */
class FiltersStore {

    /**
     * Constructor
     */
    constructor() {
        this.bindActions(FiltersActions);
        this.state = {
            filters: [],
            loadingError: {},
            filterLoaded: false
        };
    }

    /**
     * Fetch Call
     */
    onFetch() {
        ApiClient.getFilters()
            .then(function (result) {
                FiltersActions.update(result.docs);
            })
            .catch(function (error) {
                FiltersActions.loadFailedWithError(error);
            });
    }

    /**
     * update
     * @param update
     */
    onUpdate(update) {
        if(update){
            /** parse only the required data **/
            /*var data = _.pick(update,  ['sectors', 'themes', 'selectionTags', 'marketCapTypes', 'signals', 'ratios']);*/
            /** Set the state **/
            this.setState({
                    filters: update,
                    filterLoaded: true
                }
            );
        }
    }

    /**
     * Load Failed with Error
     * @param error
     */
    onLoadFailedWithError(error) {
        this.setState({loadingError: error});
    }

    /**
     * Clear the trending stocks list
     */
    onClear() {
        this.setState({
            filters: [],
            loadingError: {},
            filterLoaded: false
        });
    }
}

export default alt.createStore(FiltersStore, 'FiltersStore');