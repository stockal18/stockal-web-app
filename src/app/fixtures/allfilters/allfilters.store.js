/**
 * Created by prayag on 19/12/16.
 */

import alt from './../../../../src/alt';
import AllFiltersActions from './allfilters.actions';
import ApiClient from './../../api/api.client';

/**
 * Class TodoStore Definition
 */
class AllFiltersStore {

    /**
     * Constructor
     */
    constructor() {
        this.bindActions(AllFiltersActions);
        this.state = {
            allFilters: [],
            loadingError: {},
            allFiltersLoaded: false
        };
    }

    /**
     * Fetch Call
     */
    onFetch() {
        ApiClient.getAllFilters()
            .then(function (result) {
                AllFiltersActions.update(result.docs);
            })
            .catch(function (error) {
                AllFiltersActions.loadFailedWithError(error);
            });
    }

    /**
     * update
     * @param update
     */
    onUpdate(update) {
        if (update) {
            /** Set the state **/
            this.setState({
                    allFilters: update,
                    allFiltersLoaded: true
                }
            );
        }
    }

    /**
     * Load Failed with Error
     * @param error
     */
    onLoadFailedWithError(error) {
        this.setState({loadingError: error});
    }

    /**
     * Clear the trending stocks list
     */
    onClear() {
        this.setState({
            allFilters: [],
            loadingError: {},
            allFiltersLoaded: false
        });
    }
}

export default alt.createStore(AllFiltersStore, 'AllFiltersStore');