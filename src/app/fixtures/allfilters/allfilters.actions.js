/**
 * Created by prayag on 19/12/16.
 */

import alt from '../../../alt';

/**
 * TodoActions Class Definitions
 */
class AllFiltersActions {
    constructor() {
        this.generateActions(
            'fetch',
            'update',
            'loadFailedWithError',
            'clear'
        )
    }
}

export default alt.createActions(AllFiltersActions);