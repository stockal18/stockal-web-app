/**
 * Created by prayag on 04/08/16.
 */

import alt from '../../../alt';

/**
 * TodoActions Class Definitions
 */
class InfoActions {
    constructor() {
        this.generateActions(
            'fetch',
            'update',
            'loadFailedWithError',
            'clear'
        )
    }
}

export default alt.createActions(InfoActions);