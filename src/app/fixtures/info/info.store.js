/**
 * Created by prayag on 04/08/16.
 */

import alt from './../../../../src/alt';
import InfoActions from './info.actions';
import ApiClient from './../../api/api.client';
import _ from 'lodash';
/**
 * Class TodoStore Definition
 */
class InfoStore {

    /**
     * Constructor
     */
    constructor() {
        this.bindActions(InfoActions);
        this.state = {
            data: [],
            loadingError: {},
            infoLoaded: false
        };
    }

    /**
     * Fetch Call
     */
    onFetch(code) {
        ApiClient.getInfo(code)
            .then(function (result) {
                InfoActions.update(result.info);
            })
            .catch(function (error) {
                InfoActions.loadFailedWithError(error);
            });
    }

    /**
     * update
     * @param update
     */
    onUpdate(update) {
        if(update){
            /** parse only the required data **/
            var data = _.pick(update,  ['symbol','chatters', 'wires', 'opinions']);
            /** Set the state **/
            this.setState({
                    data: data,
                    infoLoaded: true
                }
            );
        }
    }

    /**
     * Load Failed with Error
     * @param error
     */
    onLoadFailedWithError(error) {
        this.setState({loadingError: error});
    }

    /**
     * Clear the trending stocks list
     */
    onClear() {
        this.setState({
            symbol:"",
            chatters: [],
            wires: [],
            opinions: [],
            loadingError: {},
            infoLoaded: false
        });
    }
}

export default alt.createStore(InfoStore, 'InfoStore');