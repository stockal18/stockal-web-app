/**
 * Created by Rajasekaran on 11/01/17.
 */

import alt from '../../../../src/alt';
import RatioTableActions from './ratiotable.actions';
import ApiClient from './../../api/api.client';

class RatioTableStore {
    constructor() {
        this.bindActions(RatioTableActions);
        this.state = {
            ratioTable: {},
            loadingError: {},
            ratioTableLoaded: false
        };
    }

    onFetch(code) {
        ApiClient.getRatioTable(code)
            .then(function (result) {
                RatioTableActions.update(result.docs);
            })
            .catch(function (error) {
                RatioTableActions.loadFailedWithError(error);
            });
    }

    onUpdate(update) {
        if(update){
            var data = update[0];
            this.setState({
                    ratioTable: data,
                    ratioTableLoaded: true
                }
            );
        }
    }

    onLoadFailedWithError(error) {
        this.setState({loadingError: error});
    }

    onClear() {
        this.setState({
            ratioTable: {},
            ratioTableLoaded: false
        });
    }
}
export default alt.createStore(RatioTableStore, 'RatioTableStore');