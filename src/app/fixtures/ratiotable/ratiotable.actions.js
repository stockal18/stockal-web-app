/**
 * Created by Rajasekaran on 11/01/17.
 */

import alt from '../../../alt';

/**
 * TodoActions Class Definitions
 */
class RatioTableActions {
    constructor() {
        this.generateActions(
            'fetch',
            'update',
            'loadFailedWithError',
            'clear'
        )
    }
}

export default alt.createActions(RatioTableActions);
