/**
 * Created by sarovar on 11/07/16.
 */

import React from 'react';
import TrendingStocksStore from './../../../fixtures/trendingstocks/trendingstocks.store';
import TrendingStocksAction from './../../../fixtures/trendingstocks/trendingstocks.actions';
import connectToStores from 'alt-utils/lib/connectToStores';
import TrendingStockSegment from './trendingstocks.segment';

/**
 * TrendingStocksView
 */
class TrendingStocksView extends React.Component {

    static getStores() {
        return [TrendingStocksStore];
    }

    static getPropsFromStores() {
        return TrendingStocksStore.getState();
    }

    /**
     * Constructor
     * @param props - properties
     */
    constructor(props) {
        super(props);
    }

    /**
     * componentDidMount
     */
    componentDidMount() {
        TrendingStocksAction.fetch();
    }

    /**
     * componentWillUnmount
     */
    componentWillUnmount() {
        TrendingStocksAction.clear();
    }

    /**
     * Render
     * @returns {XML}
     */
    render() {
        /** prepared the content **/
        let stockData;
        let loader;
        if (this.props.trendingStocksLoaded) {
            stockData = this.props.trendingStocks;
        } else {
            loader = (
                <div className="ui active inverted dimmer">
                    <div className="ui centered text loader">Loading</div>
                </div>
            )
        }
        let i = 0;
        return (
            <div className="ui middle aligned centered grid">
                <div className="sixteen wide column">
                    <div className="row">

                        <div className="ui middle aligned centered grid">
                            <div className="twelve wide computer fourteen wide tablet fourteen wide mobile column">
                                <div className="row">
                                    <div className="trending_header"> TRENDING STOCKS</div>
                                </div>
                            </div>
                            <div className="twelve wide computer fourteen wide tablet fourteen wide mobile column">
                                <div className="row">
                                    <div className="trending_sub_header">Based on social media chatter,
                                        sentiment changes & Wall St. analyst ratings
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="ui middle aligned centered grid">
                            <div className="fifteen wide column">
                                <div className="row">
                                    <div
                                        className={"ui grid container trending_container " + (this.props.trendingStocksLoaded ? "trending_box_green_border" : "trending_box_white_border")}>
                                        {loader}
                                        <div
                                            className="sixteen wide mobile sixteen wide tablet six wide computer column">
                                            <div className="ui grid">
                                                <div
                                                    className="sixteen wide mobile sixteen wide tablet eight wide computer column">
                                                    <div className="ui grid">
                                                        <div
                                                            className="nine wide mobile nine wide tablet sixteen wide computer column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                        <div
                                                            className="seven wide mobile seven wide tablet sixteen wide computer column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    className="sixteen wide mobile sixteen wide tablet eight wide computer column">
                                                    <div className="ui grid">
                                                        <div
                                                            className="five wide mobile five wide tablet sixteen wide computer column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                        <div
                                                            className="five wide mobile five wide tablet sixteen wide computer column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                        <div
                                                            className="six wide mobile six wide tablet sixteen wide computer column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            className="sixteen wide mobile sixteen wide tablet ten wide computer column">
                                            <div className="ui grid">
                                                <div className="six wide column">
                                                    <TrendingStockSegment index={i++} data={stockData}/>
                                                </div>
                                                <div className="five wide column">
                                                    <TrendingStockSegment index={i++} data={stockData}/>
                                                </div>
                                                <div className="five wide column">
                                                    <div className="toprightbox">
                                                        <TrendingStockSegment index={i++} data={stockData}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="ui grid">
                                                <div
                                                    className="sixteen wide mobile ten wide tablet ten wide computer column">
                                                    <div className="ui grid">
                                                        <div className="nine wide column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                        <div className="seven wide column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                    </div>
                                                    <div className="ui grid">
                                                        <div className="seven wide column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                        <div className="nine wide column">
                                                            <div className="ui grid">
                                                                <div className="sixteen wide column">
                                                                    <TrendingStockSegment index={i++} data={stockData}/>
                                                                </div>
                                                                <div className="sixteen wide column">
                                                                    <TrendingStockSegment index={i++} data={stockData}/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    className="sixteen wide mobile six wide tablet six wide computer column">
                                                    <div className="ui grid">
                                                        <div
                                                            className="five wide mobile sixteen wide tablet sixteen wide computer column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                        <div
                                                            className="six wide mobile sixteen wide tablet sixteen wide computer column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                        <div
                                                            className="five wide mobile sixteen wide tablet sixteen wide computer column">
                                                            <TrendingStockSegment index={i++} data={stockData}/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connectToStores(TrendingStocksView);