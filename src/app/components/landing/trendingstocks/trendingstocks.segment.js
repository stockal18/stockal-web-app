/**
 * Created by sarovar on 12/07/16.
 */

import React from 'react';
import {Component} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {Link} from 'react-router';

/**
 * TrendingStockSegment
 */
class TrendingStockSegment extends Component {

    /**
     * Render the TrendingStockSegment
     * @returns {XML}
     */
    render(){
        let content
            if(this.props.data) {
                const data = this.props.data[this.props.index];
                content = (
                    <Link to={"/stock-details/" + data.code} className="trending_box_link">
                        <ReactCSSTransitionGroup transitionName="trending-transition" transitionAppear = {true}
                                                 transitionAppearTimeout = {this.props.index * 5000}
                                                 transitionEnter = {false} transitionLeave = {false}>
                            <div className="trending_box_container" key={this.props.index}>
                                <div className="left_container">
                                    <div className="confidence_meter">{data.benzingaBuyPercent}%</div>
                                    <div className="symbol"><span>{data.code}</span></div>
                                </div>
                                <div className="right_container">
                                    <div className="price">${data.lastTradePrice}</div>
                                    <div className={"price_change " + (data.change > 0 ? "green" : (data.change < 0 ? "red" : ""))}>{data.change.length > 0 ? data.change : ""}</div>
                                </div>
                                <div className="name">{data.company}</div>
                            </div>
                        </ReactCSSTransitionGroup>
                    </Link>
                )
            }
        return (
            <div className={"ui grid trending_box " + (this.props.data ? "trending_box_green_border" : "trending_box_gray trending_box_white_border")} data-index={this.props.index}>
                {content}
            </div>
        );
    }
}

export default TrendingStockSegment;