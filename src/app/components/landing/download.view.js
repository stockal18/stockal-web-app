/**
 * Created by Rajan on 04/07/16.
 */
import React from 'react';
/** Import the style to give the scope **/
//noinspection JSUnresolvedVariable

import IphoneAppScreenUrl from './../../../public/assets/images/iphone_app_screen.png';
import BtnAppStoreUrl from '../../../public/assets/images/btn_appstore.png';
import BtnPlayStoreUrl from '../../../public/assets/images/btn_playstore.png';

/**
 * Download Class
 */
class Download extends React.Component {
    /**
     * Render Download Component
     * @returns {XML}
     */
    render() {
        return (
            <div className="ui grid download_section">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="ui grid container">
                            <div className="sixteen wide mobile six wide tablet six wide computer column iphone">
                                <div className="row">
                                    <img src={IphoneAppScreenUrl}/>
                                </div>
                            </div>
                            <div className="one wide computer only column iphone"></div>
                            <div className="sixteen wide mobile nine wide tablet seven wide computer column">
                                <div className="row">
                                    <div className="ui grid download-section-text-container">
                                        <div className="sixteen wide column">
                                            <div className="row">
                                                <div className="download_section_heading">
                                                    Do more with our app.
                                                </div>
                                            </div>
                                        </div>
                                        <div className="sixteen wide column">
                                            <div className="row">
                                                <div className="download_section_text">
                                                    Combine investing indicators with recommendations & ideas from peers
                                                    and experts on Stockal Chat. Seamlessly place trades via your
                                                    favorite broker.
                                                </div>
                                            </div>
                                        </div>
                                        <div className="sixteen wide column">
                                            <div className="app_section">
                                                <a href="http://bit.ly/stockal-apple" target="_blank"><img
                                                    src={BtnAppStoreUrl} className="app_img"/>
                                                </a>
                                            </div>
                                            <div className="app_section">
                                                <a href="http://bit.ly/stockal-android" target="_blank"><img
                                                    src={BtnPlayStoreUrl} className="app_img"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Download;
