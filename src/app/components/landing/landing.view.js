/**
 * Created by prayag on 08/08/16.
 */
//noinspection JSUnresolvedVariable
import React, {Component} from 'react';
import DefaultLayout from '../defaultlayout.view';
import Search from './search.view';
import AppPromo from './apppromo.view';
import Infographic from './infographic.view';
import Download from './download.view';

import TrendingStockView from './trendingstocks/trendingstocks.view.js'

/**
 * App Class Definition - Starting Point
 */
class Landing extends Component {

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Set the defaultProps
     * @type {{initialCount: number, title: string, todoSegmentTitle: string}}
     */
    static defaultProps = {
        title: 'Stockal'
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
    }

    componentDidMount(){
        if(this.props.location.query.ref && this.props.location.query.ref != "producthunt") {
            //this.props.route.showSocialLoginPopup();
        }
    }


    /**
     * App Rendering Starts Here
     * @returns {XML}
     */
    render(){
        return (
            <div title={this.props.title}>
                <Search/>
                <AppPromo/>
                <TrendingStockView/>
                <Infographic/>
                <Download/>
            </div>
        );
    }
}

export default Landing;