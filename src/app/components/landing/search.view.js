/**
 * Created by prayag on 25/07/16.
 */
import React from 'react';
import IllustrationImageUrl from './../../../public/assets/images/hero_illustration.png';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlight from 'autosuggest-highlight';
import {Link} from 'react-router';
var data = require('./../../../public/assets/others/newSymbolsListLatest.csv');
var stockList = [];
for(var i = 1; i < data.length; i++) {
    var obj = {};
    obj = {code: data[i][0], company: data[i][1]}
    stockList.push(obj);
}

function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function getSuggestions(value) {
    const escapedValue = escapeRegexCharacters(value.trim());
    if (escapedValue === '') {
        return [];
    }
    const regex = new RegExp('\\b' + escapedValue, 'i');
    return stockList.filter(stockList => regex.test(`${stockList.code}, ${stockList.company}`));
}

function getSuggestionValue(suggestion) {
    return `${suggestion.code}`;
}

function renderSuggestion(suggestion, { query }) {
    const suggestionText = `${suggestion.code}, ${suggestion.company}`;
    const matches = AutosuggestHighlight.match(suggestionText, query);
    const parts = AutosuggestHighlight.parse(suggestionText, matches);
    return (
        <span className="name">
        {
            parts.map((part, index) => {
                const className = part.highlight ? 'highlight' : null;

                return (
                    <span className={className} key={index}>{part.text}</span>
                );
            })
        }
        </span>
    );
}

/* Search className */
class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            suggestions: getSuggestions(''),
            search: false,
            selectedCode : ''
        };
    }

    onChange = (event, { newValue, method }) => {
        this.setState({
            value: newValue
        });
    };

    onSuggestionsUpdateRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected = (event, { suggestion, suggestionValue, sectionIndex, method }) => {
        this.setState({
            search: true,
            selectedCode : suggestionValue
        });
    }

    /**
     * Render Header Component
     * @returns {XML}
     */
    render() {
        const { value, suggestions } = this.state;
        const inputProps = {
            placeholder: "Enter symbol or company name",
            value,
            onChange: this.onChange
        };

        let search = (
            <div className="search_btn">SEARCH</div>
        )

        if(this.state.search){
            search = (
                <Link to={"/stock-details/"+this.state.selectedCode}>
                    <div className="search_btn">SEARCH</div>
                </Link>
            )
        }
        return (
                <div className="ui middle aligned centered grid container">
                    <div className="row">
                        <div className="sixteen wide tablet ten wide computer fifteen wide mobile column">
                            <div className="row">
                                <div className="sixteen wide column hero_section_heading">Investing Simplified</div>
                                <div className="sixteen wide column hero_section_subheading">Intelligence and expertise <br/>
                                    at your finger tips
                                </div>
                                <div className="sixteen wide column hero_section_search">
                                    <Autosuggest
                                        suggestions={suggestions}
                                        onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested}
                                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                        onSuggestionSelected={this.onSuggestionSelected}
                                        getSuggestionValue={getSuggestionValue}
                                        renderSuggestion={renderSuggestion}
                                        focusFirstSuggestion= {true}
                                        inputProps={inputProps}/>
                                    {search}
                                </div>
                            </div>
                        </div>
                        <div className="six wide right floated computer only column hero_section_spiral">
                            <img src={IllustrationImageUrl}/>
                        </div>
                    </div>
                <div className="ui section_space"></div>
            </div>
        );
    }
}
export default Search;
