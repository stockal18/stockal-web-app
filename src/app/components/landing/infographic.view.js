/**
 * Created by Rajan on 04/07/16.
 */
import React from 'react';
/** Import the style to give the scope **/
//noinspection JSUnresolvedVariable

import InfographicImage1Url from './../../../public/assets/images/infographic_1.png';
import InfographicImage2Url from './../../../public/assets/images/infographic_2.png';
import InfographicImage3Url from './../../../public/assets/images/infographic_3.png';
import IconChevronUrl from './../../../public/assets/images/icon_chevron.png';

/**
 * Infographic Class
 */
class Infographic extends React.Component {

    /**
     * Render Header Component
     * @returns {XML}
     */
    render() {
        return (
            <div className="ui middle aligned centered grid infographic_section">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="ui middle aligned centered grid">
                            <div className="sixteen wide computer only column infographic_section_heading">
                                <div className="row">
                                    <div className="sixteen wide column">Get analyst reports, social conversations, news
                                        &
                                    </div>
                                    <div className="sixteen wide column">company predictions - all in one place.</div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid">
                            <div className="sixteen wide computer only column infographic_parts">
                                <div className="ui centered grid container">
                                    <div className="four wide column"><img src={InfographicImage1Url}/></div>
                                    <div className="one wide column chevron"><img src={IconChevronUrl}/></div>
                                    <div className="four wide column"><img src={InfographicImage2Url}/></div>
                                    <div className="one wide column chevron"><img src={IconChevronUrl}/></div>
                                    <div className="four wide column"><img src={InfographicImage3Url}/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Infographic;
