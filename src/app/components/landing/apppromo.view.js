/**
 * Created by Rajasekaran on 19/09/16.
 */
import React from 'react';

/** Scroll require for using scrollToBottom **/
var Scroll = require('react-scroll');

/* AppPromo Class */
class AppPromo extends React.Component {
    scrollToBottom() {
        var scroll = Scroll.animateScroll;
        scroll.scrollToBottom();
    }
    render() {
        return (
            <a onClick={this.scrollToBottom}>
                <div className="ui middle aligned centered grid apppromo_section">
                    <div className="fourteen wide column">
                        <div className="row">
                            <div className="app_link">Do a lot more with Stockal app</div>
                            <div className="app_statement">Once you’ve got a glimpse of our
                                platform here, download the app for a more holistic investing experience.
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        );
    }
}
export default AppPromo;

