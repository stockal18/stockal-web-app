/**
 * Created by Rajan on 04/07/16.
 */
import React from 'react';
import { Link } from 'react-router';
import IconFacebookUrl from '../../../public/assets/images/icon_fb.png';
import IconLinkedInUrl from '../../../public/assets/images/icon_li.png';
import IconTwitterUrl from '../../../public/assets/images/icon_twitter.png';
import IconContactUrl from '../../../public/assets/images/icon_contact.png';

/**
 * Header Class
 */
class Header extends React.Component {

    /**
     * Render Header Component
     * @returns {XML}
     */
    render() {
        return (
            <div className="ui middle aligned centered grid footer_section">
                <div className="sixteen wide column">
                    <div className="row">

                        <div className="ui centered grid container">
                            <div className="four wide computer five wide tablet sixteen wide mobile column">
                                <div className="row">
                                    <div className="menutabs">
                                        <Link to='/about'>ABOUT</Link>
                                        <Link to='/enterprise'>ENTERPRISE</Link>
                                        <Link to='/faq'>FAQ</Link>
                                        <a href="http://blog.stockal.com/" target="_blank">BLOG</a>
                                    </div>
                                </div>
                            </div>
                            <div className="four wide computer four wide tablet sixteen wide mobile column">
                                <div className="row">
                                    <div className="socialnw">
                                        <a href="https://twitter.com/getStockal" target="_blank"><img
                                            src={IconTwitterUrl}/></a>
                                        <a href="https://www.facebook.com/getstockal" target="_blank"><img
                                            src={IconFacebookUrl}/></a>
                                        <a href="https://www.linkedin.com/company/stockal" target="_blank"><img
                                            src={IconLinkedInUrl}/></a>
                                    </div>
                                </div>
                            </div>
                            <div className="four wide computer four wide tablet sixteen wide mobile column">
                                <div className="row">
                                    <div className="stockalmail">
                                        <img src={IconContactUrl}/>support@stockal.com
                                    </div>
                                </div>
                            </div>
                            <div className="four wide computer three wide tablet sixteen wide mobile column">
                                <div className="row">
                                    <div className="copyright"> &copy; Stockal 2016</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;
