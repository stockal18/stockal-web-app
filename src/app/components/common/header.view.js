/**
 * Created by Rajan on 04/07/16.
 */
import React from 'react';
import {Link} from 'react-router';

/** Import the style to give the scope **/
//noinspection JSUnresolvedVariable
require('./../../../public/assets/css/style.css');
require('./../../../public/assets/css/stock_details.css');
require('./../../../public/assets/css/menus.css');
require('./../../../public/assets/css/earnings.css');
require('./../../../public/assets/css/earnings-upcoming.css');
require('./../../../public/assets/css/earnings-announced.css');
require('./../../../public/assets/css/recommend.css');
require('./../../../public/assets/css/search-popup.css');
require('./../../../public/assets/css/search-filter.css');
require('./../../../public/assets/css/search-result.css');
import 'react-day-picker/lib/style.css';
import blackLogo from './../../../public/assets/images/stockalblacklogo.png';
import whiteLogo from './../../../public/assets/images/white_&_Colored.png';

/**
 * Header Class
 */
class Header extends React.Component {
    /**
     * Render Header Component(logo and menu items)
     * @returns {XML}
     */

    render() {
        var pathname = window.location.pathname;
        let userLogin = !(Object.keys(this.props.user).length === 0 && this.props.user.constructor === Object) ? (
            <div className="ui simple dropdown item nav_item">
                <div className="text">
                    <i className="user icon"></i>HI, {this.props.user.fullname.split(' ')[0].toUpperCase()}
                </div>
                <i className="dropdown icon"></i>
                <div className="menu">
                    <div className="item"><i className="star icon"></i>WATCHLIST</div>
                    <div className="item" onClick={this.props.logoutUser}><i className="sign out icon"></i>LOGOUT</div>
                </div>
            </div>) : (
            <a className="item nav_item" onClick={this.props.showLoginPopup}><i className="sign in icon"></i>LOGIN</a>
        );
        return (
            <div
                className={"ui centered grid header_section" + (pathname === '/enterprise' ? " active_enterprise" : "")}>
                <div className="fifteen wide column">
                    <div className="row">
                        <div className="ui grid container">
                            <div className="six wide computer four wide tablet sixteen wide centered mobile column logo">
                                <div className="row">
                                    <Link to="/"><img src={pathname === '/enterprise' ? whiteLogo : blackLogo }/></Link>
                                </div>
                            </div>
                            <div className="ten wide computer twelve wide tablet sixteen wide mobile column menu_tab">
                                <div className="row">
                                    <div className="nav_menuitem">
                                        <Link to='/about' className="item nav_item"
                                              activeClassName="active">ABOUT</Link>
                                        <Link to='/enterprise' className="item nav_item" activeClassName="active">ENTERPRISE</Link>
                                        <Link to='/faq' className="item nav_item" activeClassName="active">FAQ</Link>
                                        <a className="item nav_item" href="http://blog.stockal.com/"
                                           target="_blank">BLOG</a>
                                        {userLogin}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Header;
