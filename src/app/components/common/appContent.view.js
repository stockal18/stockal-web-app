/**
 * Created by prayag on 20/10/16.
 */
import React from 'react';
import {Router, IndexRoute, Route, Link, browserHistory, Redirect} from 'react-router';
import Default from './../defaultlayout.view';
import Landing from './../landing/landing.view';
import About from './../static/about.view';
import Enterprise from './../enterprise/enterprise.view';
import India from '../india/india.view';
import Earnings from '../earnings/earnings.view';
import Upcoming from '../earnings/earnings.upcoming.view';
import Announced from '../earnings/earnings.announced.view';
import Recommendation from '../recommendations/recommendation.view';
import Faq from './../static/faq.view';
import StockDetail from './../stockdetails/stockdetails.view';
import Search from './../search/search.view';
import Suggestion from './../search/suggestion/suggestions.view';
import MainFilter from '../search/filtersandresults/filtersandresults.view';
import Detail from './../search/detail.view';
import TestComposedGraph from '../test/testcomposedgraph';
import ComposedGraph from '../test/composedgraph';
import PageNotFound from '../pagenotfound/pagenotfound.view';

/**
 * Header Class
 */

class AppContent extends React.Component {

    /**
     * Render AppContent Component
     * @returns {XML}
     */

    render() {
        return (
            <div>
                <Router history={browserHistory}>
                    <Route path="/" component={Default}>
                        <IndexRoute component={Landing}/>
                        <Route path="about" component={About}/>
                        <Route path="enterprise" component={Enterprise}/>
                        <Route path="india" component={India}/>
                        <Route path="earnings" component={Earnings}/>
                        <Route path="earnings/upcoming" component={Upcoming}/>
                        <Route path="earnings/announced" component={Announced}/>
                        <Route path="recommendation" component={Recommendation}/>
                        <Route path="faq" component={Faq}/>
                        <Route path="stock-details/:code" component={StockDetail}/>
                        <Route path="/search" component={Search}>
                            <IndexRoute component={Suggestion}/>
                            <Route path="/search/filter" component={MainFilter}/>
                            <Route path="/search/filter/:category/:value" component={MainFilter}/>
                        </Route>
                        <Route path="/composedgraph" component={ComposedGraph} />
                        <Route path="/testcomposedgraph" component={TestComposedGraph} />
                        <Route path="/error404" component={PageNotFound} />
                        <Redirect from="*" to="/error404" />
                    </Route>
                </Router>
            </div>
        );
    }
}

export default AppContent;
