/**
 * Created by prayag on 19/10/16.
 */
import React from 'react';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import APIClient from './../../api/api.client';
import CloseImg from './../../../public/assets/images/closebtnimg.png';

var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};

/**
 * Header Class
 */
class SocialLogin extends React.Component {

    /**
     * Render Header Component
     * @returns {XML}
     */
    constructor(props) {
        super(props);
        this.state = {
            displaySocialLoginPopup: this.props.displaySocialLoginPopup,
            displayLogin: true,
            shareUrl: "",
            shareText: "I found Stockal pretty interesting for tracking my stock investments. Why don\'t you check it out too",
            copyButtonText: "Copy Link",
            registeredCount: ""
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            displaySocialLoginPopup: nextProps.displaySocialLoginPopup,
            displayLogin: true,
            copyButtonText: "Copy Link"
        });
    }

    socialLogin(data) {
        APIClient.socialLogin(data).then((response) => {
            if (response.data.status === 200) {
                this.props.setUser(response.data.user);
                this.setState({
                    displayLogin: false,
                    shareUrl: "http://www.stockal.com?ref=" + response.referralToken,
                    registeredCount: response.registeredCount
                })
            }
        });
    }

    responseFacebook(response) {
        var data = {
            fullname: response.name,
            username: response.email,
            provider: "facebook",
            referralToken: getQueryString("ref")
        };
        this.socialLogin(data);
    };

    responseGoogle(response) {
        var data = {
            fullname: response.profileObj.name,
            username: response.profileObj.email,
            provider: "google",
            referralToken: getQueryString("ref")
        };
        this.socialLogin(data);
    }

    onTwitterShare() {
        window.open("https://twitter.com/intent/tweet?url=" + encodeURI(this.state.shareUrl) + "&text=" + this.state.shareText, 'stockal_twitter', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
        return false;
    }

    onFacebookShare() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=" + this.state.shareUrl + "&p[images][0]=http://stockal.com/images/Black_&_Colored.png", 'stockal_twitter', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
        return false;
    }

    onLinkedInShare() {
        window.open("https://www.linkedin.com/shareArticle?mini=true&url=" + this.state.shareUrl + "&title=&summary=" + this.state.shareText + " " + this.state.shareUrl + "&source=Stockal", 'stockal_twitter', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0');
        return false;
    }

    copyToClipboard() {
        var aux = document.createElement("input");
        aux.setAttribute("value", this.state.shareUrl);
        document.body.appendChild(aux);
        aux.select();
        document.execCommand("copy");
        document.body.removeChild(aux);
        this.setState({
            copyButtonText: "Copied"
        })
        setTimeout(() => {
            this.closeSocialLoginPopup();
        }, 500)
    }

    closeSocialLoginPopup() {
        this.setState({
            displaySocialLoginPopup: false
        });
    }

    render() {
        let segment = this.state.displayLogin ? (
            <div className="ui segment">
                <div className="segment-head">
                    Hey! It’s so nice to see you
                </div>
                <div className="popup-content">
                    Get analyst reports, social conversations, news & company predictions - all in one place.
                </div>

                <GoogleLogin
                    clientId="710929209192-fck7v1l5vth8tjkv4fq2ruv1hj67r0gs.apps.googleusercontent.com"
                    onSuccess={this.responseGoogle.bind(this)}
                    onFailure={this.responseGoogle.bind(this)}
                    className="social-button google-button">
                    <i className="fa fa-google"/>Login with Google
                </GoogleLogin>

                <FacebookLogin
                    appId="952932651504845"
                    autoLoad={false}
                    fields="name,email"
                    callback={this.responseFacebook.bind(this)}
                    cssClass="social-button facebook-button"
                    icon="fa-facebook"/>

                <div className="popup-content">
                    Over 5 Million Data Points Crunched daily.
                    Just 10 Sec to Sign-up (free!). No database farming happening here. Promise!
                </div>
            </div>
        ) : (
            <div className="ui segment success">
                <div className="segment-head">
                    Thanks!
                </div>
                <div className="popup-content">
                    More than {this.state.registeredCount} members have saved over USD60 on our premium membership.
                </div>
                <div className="popup-content">
                    Refer your friends & get 4 months of free subscription to our premium version when we launch this
                    November.
                </div>
                <div className="share-links">
                    <div onClick={this.onTwitterShare.bind(this)}>
                        <i className="fa fa-twitter"/>
                        Tweet
                    </div>
                    <div onClick={this.onFacebookShare.bind(this)}>
                        <i className="fa fa-facebook" aria-hidden="true"></i>
                        Share
                    </div>
                    <div onClick={this.onLinkedInShare.bind(this)}>
                        <i className="fa fa-linkedin" aria-hidden="true"></i>
                        Share
                    </div>
                    <a href={"mailto:?&subject=Best Mobile App on Investing Tips for Stock Investors www.stockal.com&body="
                    + this.state.shareText + " " + this.state.shareUrl}>
                        <i className="fa fa-envelope" aria-hidden="true"></i>
                        Email
                    </a>
                </div>
                <div className="popup-content referral-link">
                    Or share this link we created just for you {this.state.shareUrl}
                </div>
                <div className="action-buttons">
                    <div onClick={this.copyToClipboard.bind(this)}>
                        {this.state.copyButtonText}
                    </div>
                    <div onClick={this.props.closeSocialLoginPopup.bind(this)}>
                        Not Interested
                    </div>
                </div>
            </div>
        )
        return (
            <div className="ui grid">
                <div className="sixteen wide column">
                    <div
                        className={"row popup-overlay" + (this.state.displaySocialLoginPopup ? " sociallogin_popup_show" : "")}/>
                    <div
                        className={"row popup-segment-overlay" + (this.state.displaySocialLoginPopup ? " sociallogin_popup_show" : "")}>
                        <div className="ui middle aligned centered grid popup-segment-container">
                            <div className="six wide computer column ten wide tablet fifteen wide mobile column">
                                <div className="row">
                                    <div className="closeimg" onClick={this.closeSocialLoginPopup.bind(this)}>
                                        <img src={CloseImg}/>
                                    </div>
                                    {segment}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SocialLogin;
