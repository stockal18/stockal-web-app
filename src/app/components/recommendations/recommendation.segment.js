/**
 * Created by Rajasekaran on 15/12/16.
 */
import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router';

class RecommendationSegment extends Component {

    render() {
        return (
            <Link to={"/api/recommend"}>
                <div className="search_btn_text">Recommend</div>
            </Link>
        );
    }
}
export default RecommendationSegment;