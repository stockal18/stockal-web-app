/**
 * Created by Rajasekaran on 15/12/16.
 */

import React from 'react';
// import RecommendStore from '../../fixtures/recommend/recommend.store';
// import RecommendActions from '../../fixtures/recommend/recommend.actions';
// import RecommendationSegment from './recommendation.segment';
import APIClient from "./../../api/api.client";
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlight from 'autosuggest-highlight';

var data = require('./../../../public/assets/others/newSymbolsListLatest.csv');
var stockList = [];
for (var i = 1; i < data.length; i++) {
    var obj = {};
    obj = {code: data[i][0], company: data[i][1]}
    stockList.push(obj);
}

function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function getSuggestions(value) {
    const escapedValue = escapeRegexCharacters(value.trim());
    if (escapedValue === '') {
        return [];
    }
    const regex = new RegExp('\\b' + escapedValue, 'i');
    return stockList.filter(stockList => regex.test(`${stockList.code}, ${stockList.company}`));
}

function getSuggestionValue(suggestion) {
    return `${suggestion.code}`;
}

function renderSuggestion(suggestion, {query}) {
    const suggestionText = `${suggestion.code}, ${suggestion.company}`;
    const matches = AutosuggestHighlight.match(suggestionText, query);
    const parts = AutosuggestHighlight.parse(suggestionText, matches);
    return (
        <span className="name">
        {
            parts.map((part, index) => {
                const className = part.highlight ? 'highlight' : null;

                return (
                    <span className={className} key={index}>{part.text}</span>
                );
            })
        }
        </span>
    );
}
/**
 * Recommendation className
 */
class Recommendation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            suggestions: getSuggestions(''),
            search: false,
            selectedCode: '',
            symbols: []
        };
    }

    onChange = (event, {newValue, method}) => {
        this.setState({
            value: newValue,
        });
    };

    onSuggestionsUpdateRequested = ({value}) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    onSuggestionsFetchRequested = ({value}) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected = (event, {suggestion, suggestionValue, sectionIndex, method}) => {
        let symbols = this.state.symbols;
        symbols.push(suggestionValue);
        this.setState({
            search: true,
            symbols: symbols,
            value: '',
        });
    };

    getRecommendation(data) {
        APIClient.getRecommend(data).then((data) => {
            if (data.status === 200) {
                console.log(data);
            }
        });
    }

    render() {
        let symbols = this.state.symbols;
        let selectedSymbols = symbols.map(function (item, i) {
            return (<div>{item}<br/></div>);
        });
        const {value, suggestions} = this.state;
        const inputProps = {
            placeholder: "Enter Stock Symbols",
            value,
            onChange: this.onChange,
        };
        return (
            <div className="recomm_section">
                <div className="row">
                    <div className="ui centered grid">
                        <div className="six wide computer ten wide tablet fourteen wide mobile column">
                            <div className="row">
                                <div className="recomm_search_heading">
                                    Recommendations for Stock
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="ui centered grid recomm_section_suggest">
                        <div className="six wide computer ten wide tablet fourteen wide mobile column">
                            <div className="recomm_section_search">
                                <Autosuggest
                                    suggestions={suggestions}
                                    onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested}
                                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                    onSuggestionSelected={this.onSuggestionSelected}
                                    getSuggestionValue={getSuggestionValue}
                                    renderSuggestion={renderSuggestion}
                                    focusFirstSuggestion={true}
                                    inputProps={inputProps}
                                />
                            </div>
                            <div className="recomm_search_btn" onClick={this.getRecommendation}>
                                Search
                            </div>
                        </div>
                    </div>
                    <div className="ui centered grid">
                        <div className="six wide computer ten wide tablet fourteen wide mobile column">
                            <div className="selected_symbols">
                                <div className="selected_symbols_title">Selected Symbols:</div>
                                <div>{selectedSymbols}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Recommendation;