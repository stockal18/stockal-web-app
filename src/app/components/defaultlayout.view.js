/**
 * Created by Rajan on 04/07/16.
 */
var React = require('react');
import DocumentTitle from 'react-document-title';
import Header from './common/header.view';
import Footer from './common/footer.view';
import SocialLogin from './common/socialLogin.view';

/**
 * Default Layout
 */
class DefaultLayout extends React.Component {

    /**
     * Rendering part of DefaultLayout
     * @returns {XML}
     */

    componentDidUpdate(){
        window.scrollTo(0,0);
    }

    static defaultProps = {
        title: 'Stockal'
    };

    constructor(props) {
        super(props);
        this.state = {
            displaySocialLoginPopup: false,
            user: {}
        }
    }

    componentWillMount() {
        if (localStorage.getItem("stockal-user")) {
            this.setUser(JSON.parse(localStorage.getItem("stockal-user")))
        }
    }


    closeSocialLoginPopup() {
        this.setState({
            displaySocialLoginPopup: false
        });
    }

    showSocialLoginPopup(time) {
        if (!localStorage.getItem("stockal-user") ||
            (Object.keys(JSON.parse(localStorage.getItem("stockal-user"))).length === 0
            && JSON.parse(localStorage.getItem("stockal-user")).constructor === Object)) {
            setTimeout(() => {
                this.setState({
                    displaySocialLoginPopup: true
                });
            }, time)
        }
    }

    logoutUser() {
        this.setUser({});
    }

    setUser(user) {
        if (!(Object.keys(user).length === 0 && user.constructor === Object)) {
            localStorage.setItem("stockal-user", JSON.stringify(user));
        } else{
            localStorage.removeItem("stockal-user")
        }
        this.setState({
            user: user
        })
    }

    render() {
        return !this.props.error ? (
            <div>
                <Header showLoginPopup={this.showSocialLoginPopup.bind(this, 100)} user={this.state.user}
                        logoutUser={this.logoutUser.bind(this)}/>
                <DocumentTitle title={this.props.title}>
                    <div>
                        {this.props.children}
                    </div>
                </DocumentTitle>
                <Footer/>
                <SocialLogin setUser={this.setUser.bind(this)}
                             displaySocialLoginPopup={this.state.displaySocialLoginPopup}
                             closeSocialLoginPopup={this.closeSocialLoginPopup.bind(this)}/>
            </div>
        ) : (
            <div>
                {this.props.children}
            </div>
        );
    }
}

module.exports = DefaultLayout;