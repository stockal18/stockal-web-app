/**
 * Created by Rajasekaran on 28/12/16.
 */

import React from 'react';

class TestComposedGraphLegend extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showSocialMediaPulse: false
        };
    }
    toggleSocialMediaPulse() {
        this.setState({showSocialMediaPulse: !this.state.showSocialMediaPulse})
    }
    render() {
        return (
            <div className="ui grid">
                <div className="sixteen wide column">
                    <div className="row">

                        <div className="ui middle aligned centered grid graph_title_section">
                            <div className="row">
                                <div className="three wide column">
                                    <input type="checkbox" id="checkboxG1" className="css-checkbox"
                                           onClick={this.toggleSocialMediaPulse.bind(this)}/>
                                    <label htmlFor="checkboxG1" className="css-label">Social Media Pulse</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}
export default TestComposedGraphLegend;