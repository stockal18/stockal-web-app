/**
 * Created by Rajasekaran on 02/01/17.
 */
import React from 'react';
class CustomizedAxis extends React.Component {
    render () {
        const {x, y, payload} = this.props;
        return (
            <text x={x} y={y} dx={-25} dy={5} textAnchor="middle">
                {payload.value}
            </text>
        );
    }
}
export default CustomizedAxis;