/**
 * Created by Rajasekaran on 27/12/16.
 */

import React, {Component} from 'react';
import {ResponsiveContainer, ComposedChart, Line, Area, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import TestComposedGraphLegend from './testcomposedgraph.legend';


// jQuery(function () {
//     //register the click handler to an element with id mybutton
//     $('#mybutton').click(function () {
//         $("#toggle").toggle("slide");
//     });
// })
const data = [{name: 'Page A', socialMediaPulse: 590, sentiment: 100, price: 1400, confidenceMeter: 800},
              {name: 'Page B', socialMediaPulse: 868, sentiment: 500, price: 1506, confidenceMeter: 967},
              {name: 'Page C', socialMediaPulse: 1397, sentiment: 1000, price: 989, confidenceMeter: 1398},
              {name: 'Page D', socialMediaPulse: 1480, sentiment: 600, price: 1228, confidenceMeter: 1200},
              {name: 'Page E', socialMediaPulse: 1520, sentiment: 700, price: 1100, confidenceMeter: 1108},
              {name: 'Page F', socialMediaPulse: 1400, sentiment: 200, price: 1700, confidenceMeter: 680}];

const LabelWithValue = ({name, value}) =>
    (<div className="legend_value" key={name}>
            {value}
    </div>);

const LegendWithValues = ({payload}) =>
    (<div className="ui grid">
        <div className="sixteen wide column">
            <div className="row">

                <div className="ui middle aligned centered grid legend_section">
                    <div className="fourteen wide computer sixteen wide tablet sixteen wide mobile column">
                        <div className="row">
                            <div className="legend_text">
                                {payload.map(LabelWithValue)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>);

class Test extends React.Component {
    render() {
        return (
            <div className="ui grid">
                <div className="sixteen wide column">
                    <div className="row">

                        <div className="ui middle aligned centered grid">
                            <div className="fourteen wide computer sixteen wide tablet sixteen wide mobile column">
                                <div className="row">
                                    <div className="ir_chart_container">
                                        <ResponsiveContainer>
                                            <ComposedChart data={data} margin={{top: 20, right: 10, bottom: 20, left: 10}}>
                                                <XAxis dataKey="name"/>
                                                <YAxis />
                                                <Tooltip/>
                                                <Legend content={LegendWithValues}/>
                                                <CartesianGrid stroke='#f5f5f5'/>
                                                <Line type='monotone' dataKey='socialMediaPulse' name='Social Media Pulse' stroke='#ff0000' strokeWidth={2} dot={{r: 3}}/>
                                                <Area type='monotone' dataKey='sentiment' name='Sentiment' fill='#c1287f' stroke='#c1287f'/>
                                                <Line type='monotone' dataKey='price' name='Price' stroke='#2893c1' strokeWidth={2} dot={{r: 3}}/>
                                                <Bar dataKey='confidenceMeter' name='Confidence Meter' barSize={50} fill='#7ed321'/>
                                            </ComposedChart>
                                        </ResponsiveContainer>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <TestComposedGraphLegend/>

                    </div>
                </div>
            </div>
        );
    }
}
export default Test;