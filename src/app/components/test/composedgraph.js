/**
 * Created by prayag on 09/11/16.
 */

import React, {ReactDom} from 'react';
import CustomizedAxis from './customizedaxistick';
import {
    ResponsiveContainer,
    ComposedChart,
    Line,
    Area,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend
} from 'recharts';

const data = [{
    name: 'Page A',
    socialMediaPulse: 590,
    stockSentiment: 100,
    price: 1400,
    confidenceMeter: 800,
    commodities: 600
},
    {name: 'Page B', socialMediaPulse: 868, stockSentiment: 500, price: 1506, confidenceMeter: 967, commodities: 680},
    {name: 'Page C', socialMediaPulse: 1397, stockSentiment: 1000, price: 989, confidenceMeter: 1398, commodities: 600},
    {name: 'Page D', socialMediaPulse: 1480, stockSentiment: 600, price: 1228, confidenceMeter: 1200, commodities: 700},
    {name: 'Page E', socialMediaPulse: 1520, stockSentiment: 700, price: 1100, confidenceMeter: 1108, commodities: 650},
    {name: 'Page F', socialMediaPulse: 1400, stockSentiment: 200, price: 1700, confidenceMeter: 680, commodities: 800},
    {name: 'Page G', socialMediaPulse: 1068, stockSentiment: 500, price: 1506, confidenceMeter: 967, commodities: 680},
    {name: 'Page H', socialMediaPulse: 1397, stockSentiment: 1000, price: 1289, confidenceMeter: 1398, commodities: 600},
    {name: 'Page I', socialMediaPulse: 1480, stockSentiment: 600, price: 1228, confidenceMeter: 1200, commodities: 700},
    {name: 'Page J', socialMediaPulse: 1520, stockSentiment: 700, price: 1100, confidenceMeter: 1108, commodities: 650},
    {name: 'Page K', socialMediaPulse: 1268, stockSentiment: 500, price: 1506, confidenceMeter: 967, commodities: 680},
    {name: 'Page L', socialMediaPulse: 1397, stockSentiment: 1000, price: 989, confidenceMeter: 1398, commodities: 600},
    {name: 'Page M', socialMediaPulse: 1480, stockSentiment: 600, price: 1228, confidenceMeter: 1200, commodities: 700},
    {name: 'Page N', socialMediaPulse: 1520, stockSentiment: 700, price: 1100, confidenceMeter: 1108, commodities: 650},
    {name: 'Page O', socialMediaPulse: 1168, stockSentiment: 500, price: 1506, confidenceMeter: 967, commodities: 680},
    {name: 'Page P', socialMediaPulse: 1397, stockSentiment: 1000, price: 989, confidenceMeter: 1398, commodities: 600},
    {name: 'Page Q', socialMediaPulse: 1480, stockSentiment: 600, price: 1228, confidenceMeter: 1200, commodities: 700},
    {name: 'Page R', socialMediaPulse: 1520, stockSentiment: 700, price: 1100, confidenceMeter: 1108, commodities: 650}];

class ComposedGraph extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showSocialMediaPulse: false,
            showSentiment: false,
            showPrice: true,
            showConfidenceMeter: false,
            showCommodities: true
        };
    }

    toggleSocialMediaPulse() {
        this.setState({showSocialMediaPulse: !this.state.showSocialMediaPulse});
    }

    toggleSentiment() {
        this.setState({showSentiment: !this.state.showSentiment})
    }

    togglePrice() {
        this.setState({showPrice: !this.state.showPrice})
    }

    toggleConfidenceMeter() {
        this.setState({showConfidenceMeter: !this.state.showConfidenceMeter})
    }

    render() {
        let price = this.state.showPrice ?
            (<Line type='monotone' dataKey='price' name='Price' stroke='#2893c1' strokeWidth={2} dot={{r: 3}}/>) :
            (<Line name='Price' stroke='#2893c1'/>);
        let confidenceMeter = this.state.showConfidenceMeter ?
            (<Bar dataKey='confidenceMeter' name='Confidence meter' opacity={0.7} barSize={50} fill='#54d6c2'/>) :
            (<Bar name='Confidence meter' fill='#54d6c2'/>);
        let stockSentiment = this.state.showSentiment ?
            (<Area type='monotone' dataKey='stockSentiment' name='Stock Sentiment' fill='#c1287f' stroke='#c1287f'/>) :
            (<Area name='Stock Sentiment' stroke='#c1287f'/>);
        let socialMediaPulse = this.state.showSocialMediaPulse ?
            (<Line type='monotone' dataKey='socialMediaPulse' name='Social media pulse' stroke='#ff0000' strokeWidth={2}
                   dot={{r: 3}}/>) :
            (<Line name='Social media pulse' stroke='#ff0000'/>);
        let commodities = this.props.showCommodities ?
            (<Line type='monotone' dataKey='commodities' name={this.props.selectedCommodities} stroke='#9C10ED'
                  strokeWidth={2} dot={{r: 3}}/>) :
            (<Line name='Commodities' stroke='#9C10ED'/>);

        return (
            <div className="ui grid">
                <div className="sixteen wide column">
                    <div className="row">

                        <div className="ui middle aligned centered grid container">
                            <div className="sixteen wide computer sixteen wide tablet sixteen wide mobile column">
                                <div className="row">
                                    <div className="ir_chart_container">
                                        <ResponsiveContainer>
                                            <ComposedChart data={data}
                                                           margin={{top: 20, right: 10, bottom: 20, left: 10}}>
                                                <XAxis dataKey="name"/>
                                                <YAxis tick={<CustomizedAxis/>} />
                                                <Tooltip/>
                                                <Legend/>
                                                <CartesianGrid stroke='#f5f5f5'/>
                                                {price}
                                                {confidenceMeter}
                                                {stockSentiment}
                                                {socialMediaPulse}
                                                {commodities}
                                            </ComposedChart>
                                        </ResponsiveContainer>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui centered grid graph_title_section">
                            <div className="row">

                                <div className="three wide computer three wide tablet six wide mobile column">
                                    <div className="graph_title_each" onClick={this.togglePrice.bind(this)}>
                                        <div className={"graph-btn" + (this.state.showPrice ? " active" : "")}></div>
                                        <div>Price</div>
                                    </div>
                                </div>
                                <div className="three wide computer three wide tablet six wide mobile column">
                                    <div className="graph_title_each" onClick={this.toggleConfidenceMeter.bind(this)}>
                                        <div
                                            className={"graph-btn" + (this.state.showConfidenceMeter ? " active" : "")}></div>
                                        <div>Confidence Meter</div>
                                    </div>
                                </div>
                                <div className="three wide computer three wide tablet six wide mobile column">
                                    <div className="graph_title_each" onClick={this.toggleSentiment.bind(this)}>
                                        <div
                                            className={"graph-btn" + (this.state.showSentiment ? " active" : "")}></div>
                                        <div>Stock Sentiment</div>
                                    </div>
                                </div>
                                <div className="three wide computer three wide tablet six wide mobile column">
                                    <div className="graph_title_each" onClick={this.toggleSocialMediaPulse.bind(this)}>
                                        <div
                                            className={"graph-btn" + (this.state.showSocialMediaPulse ? " active" : "")}></div>
                                        <div>Social Media Pulse</div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}
export default ComposedGraph;