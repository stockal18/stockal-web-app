/**
 * Created by Rajasekaran on 07/12/16.
 */

import React from 'react';
import EarningsLogo from  '../../../public/assets/images/stockal_logo.png';
import StockBox from './stockbox.view';
import {Component} from 'react';
import {Link} from 'react-router';
/**
 * Earnings className
 */
class Earnings extends React.Component {
    render() {
        return (
            <div className="ui centered grid">
                <div className="sixteen wide computer fifteen wide tablet fifteen wide mobile column">

                    <div className="row">
                        <div className="ui centered grid">
                            <div className="six wide computer ten wide tablet fourteen wide mobile column">
                                <div className="row">
                                    <div className="earnings_search_heading">
                                        Forward looking earning analysis, powered by the world's data
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="ui centered grid">
                            <div className="six wide computer ten wide tablet fourteen wide mobile column">
                                <div className="earnings_section_search">
                                    <input className="earnings_search_input" type="text"
                                           placeholder="Search for Stock or Symbol"/>
                                </div>
                                <div className="earnings_search_btn">SEARCH</div>
                            </div>
                        </div>
                    </div>

                    <div className="ui centered grid earning_box_section">

                        <div className="seven wide computer fourteen wide tablet sixteen wide mobile column">
                            <div className="row">
                                <div className="ui centered grid">
                                    <div className="earning_section_heading">Upcoming</div>
                                    <div className="earnings-grid-container">
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                    </div>
                                </div>
                                <div className="ui centered grid next_stock">
                                    <div className="eight wide column">
                                        <div className="next_stock_btn"><Link to="/earnings/upcoming"> Next 60
                                            days</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="seven wide computer fourteen wide tablet sixteen wide mobile column">
                            <div className="row">
                                <div className="ui centered grid">
                                    <div className="earning_section_heading">Announced</div>
                                    <div className="earnings-grid-container">
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                        <StockBox/>
                                    </div>
                                </div>
                                <div className="ui centered grid next_stock">
                                    <div className="eight wide column">
                                        <div className="next_stock_btn"><Link to="/earnings/announced"> Last 60
                                            days</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div className="ui centered grid earning_detail_section">
                        <div className="sixteen wide column">
                            <div className="earning_detail_heading">
                                What you get here
                            </div>
                        </div>
                        <div className="four wide computer four wide tablet five wide mobile column earning_detail">
                            <img src={EarningsLogo}/>
                            <div className="earning_detail_info">
                                Estimates &<br/> forecasts
                            </div>
                        </div>
                        <div className="four wide computer four wide tablet five wide mobile column earning_detail">
                            <img src={EarningsLogo}/>
                            <div className="earning_detail_info">
                                Forwared-looking<br/> signals
                            </div>
                        </div>
                        <div className="four wide computer four wide tablet five wide mobile column earning_detail">
                            <img src={EarningsLogo}/>
                            <div className="earning_detail_info">
                                Crowdsourced<br/> news & analysis
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}
export default Earnings;
