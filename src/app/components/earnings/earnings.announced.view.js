/**
 * Created by Rajasekaran on 07/12/16.
 */

import React from 'react';
import {StickyContainer, Sticky} from 'react-sticky';
import {Component} from 'react';
import moment from 'moment';
import DayPicker, {DateUtils} from 'react-day-picker';
import CalendarLogo from  '../../../public/assets/images/calendar_logo.png';
import ClickOutside from 'react-clickoutside-component';

/**
 * Announced class
 */

class Announced extends React.Component {
    constructor(props) {
        super(props);
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputFocus = this.handleInputFocus.bind(this);
        this.handleInputBlur = this.handleInputBlur.bind(this);
        this.handleContainerMouseDown = this.handleContainerMouseDown.bind(this);
    }

    state = {
        showOverlay: false,
        value: moment().format('MMM DD, YYYY').toUpperCase('MMM'),
        selectedDay: null,
        valuePrevious: moment().subtract(1, 'days').format('MMM DD, YYYY').toUpperCase('MMM')
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            showOverlay: false
        });
    }

    componentWillUnmount() {
        clearTimeout(this.clickTimeout);
    }

    input = null;
    daypicker = null;
    clickedInside = false;
    clickTimeout = null;

    handleContainerMouseDown() {
        this.clickedInside = true;
        // The input's onBlur method is called from a queue right after onMouseDown event.
        // setTimeout adds another callback in the queue, but is called later than onBlur event
        this.clickTimeout = setTimeout(() => {
            this.clickedInside = false;
        }, 0);
    }

    handleInputFocus() {
        this.setState({
            showOverlay: true
        });
    }

    handleInputBlur() {
        const showOverlay = this.clickedInside;

        this.setState({
            showOverlay,
        });

        // Force input's focus if blur event was caused by clicking on the calendar
        if (showOverlay) {
            this.input.focus();
        }
    }

    handleInputChange(e) {
        const {value} = e.target;
        const momentDay = moment(value, 'MMM DD, YYYY', true);
        if (momentDay.isValid()) {
            this.setState({
                selectedDay: momentDay.toDate(),
                value,
            }, () => {
                this.daypicker.showMonth(this.state.selectedDay);
            });
        }
        else {
            this.setState({value, selectedDay: null});
        }
    }

    handleDayClick(e, day) {
        this.setState({
            value: moment(day).format('MMM DD, YYYY').toUpperCase('MMM'),
            selectedDay: day,
            showOverlay: false,
            valuePrevious: moment(day).subtract(1, 'days').format('MMM DD, YYYY').toUpperCase('MMM'),
        });
        this.input.blur();
    }

    handleClickOutside() {
        this.setState({
            showOverlay: false
        });
    }

    render() {
        let calClick = (<ClickOutside onClickOutside={ (e) => this.handleClickOutside(e) }>
            <div onMouseDown={ this.handleContainerMouseDown }>
                <img value={ this.state.value }
                     onClick={ this.handleInputFocus }
                     onChange={ this.handleInputChange }
                     onBlur={ this.handleInputBlur }
                     ref={ (el) => {
                         this.input = el;
                     } }
                     src={CalendarLogo}/>
                { this.state.showOverlay &&
                <div className="date_view_pick" style={ {position: 'relative'} }>
                    <div className="overlay_style">
                        <DayPicker
                            ref={ (el) => {
                                this.daypicker = el;
                            } }
                            onDayClick={ this.handleDayClick }
                            selectedDays={ day => DateUtils.isSameDay(this.state.selectedDay, day) }
                        />
                    </div>
                </div>
                }
            </div>
        </ClickOutside>);
        return (
            <div className="ui grid">
                <div className="sixteen wide column">

                    <StickyContainer>
                        <div className="ui middle aligned centered grid heading_section">
                            <div className="eight wide computer twelve wide tablet eleven wide mobile column">
                                <div className="row">
                                    <div className="heading">
                                        Announced Earnings
                                    </div>
                                </div>
                            </div>
                            <div className="three wide computer two wide tablet four wide mobile column">
                                <div className="row">
                                    <div className="calendar_img">
                                        {calClick}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Sticky>
                            <div className="ui centered grid stock_heading_section">
                                <div className="twelve wide computer fourteen wide tablet fifteen wide mobile column">
                                    <div className="row">

                                        <div className="stock-row-heading-container">
                                            <div className="each-stock-heading-container">
                                                <div className="dummy-heading"><br/>
                                                </div>
                                                <div className="first-headings">
                                                    Analyst<br/>
                                                    Opinion
                                                </div>
                                                <div className="first-headings">
                                                    Social<br/>
                                                    Sentiment
                                                </div>
                                                <div className="first-headings">
                                                    Reported<br/>
                                                    Revenue
                                                </div>
                                                <div className="middle-heading">
                                                    Previous<br/>
                                                    Revenue ($M)
                                                </div>
                                                <div className="last-headings">
                                                    Current<br/>
                                                    Price
                                                </div>
                                                <div className="last-headings">
                                                    Price<br/>
                                                    Target
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Sticky>
                        <div className="ui centered grid date_section">
                            <div className="twelve wide computer fourteen wide tablet fifteen wide mobile column">
                                <div className="row">
                                    <div className="earning_date">
                                        {this.state.value}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="ui centered grid">
                            <div className="twelve wide computer fourteen wide tablet fifteen wide mobile column">
                                <div className="row">

                                    <div className="earnings-row-container">
                                        <div className="each-stock-container">
                                            <div className="stock-name-container">
                                                <div className="stock-code">
                                                    <span>FB</span>
                                                </div>
                                                <div className="stock-name">
                                                    Facebook Inc.
                                                </div>
                                            </div>
                                            <div className="analyst-opinion-container">
                                                <div className="analyst-opinion">
                                                    76%
                                                </div>
                                            </div>
                                            <div className="social-sentiment-container">
                                                <div className="social-sentiment green">
                                                    <i className="caret up icon"></i>
                                                </div>
                                            </div>
                                            <div className="reported-revenue-container">
                                                <div className="reported-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="previous-revenue-container">
                                                <div className="previous-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="current-price-container">
                                                <div className="current-price">
                                                    $43.5
                                                </div>
                                            </div>
                                            <div className="price-target-container">
                                                <div className="price-target green">
                                                    $49.6
                                                </div>
                                            </div>
                                        </div>
                                        <div className="each-stock-container">
                                            <div className="stock-name-container">
                                                <div className="stock-code">
                                                    <span>FB</span>
                                                </div>
                                                <div className="stock-name">
                                                    Facebook Inc.
                                                </div>
                                            </div>
                                            <div className="analyst-opinion-container">
                                                <div className="analyst-opinion">
                                                    76%
                                                </div>
                                            </div>
                                            <div className="social-sentiment-container">
                                                <div className="social-sentiment red">
                                                    <i className="caret down icon"></i>
                                                </div>
                                            </div>
                                            <div className="reported-revenue-container">
                                                <div className="reported-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="previous-revenue-container">
                                                <div className="previous-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="current-price-container">
                                                <div className="current-price">
                                                    $43.5
                                                </div>
                                            </div>
                                            <div className="price-target-container">
                                                <div className="price-target red">
                                                    $33.2
                                                </div>
                                            </div>
                                        </div>
                                        <div className="each-stock-container">
                                            <div className="stock-name-container">
                                                <div className="stock-code">
                                                    <span>FB</span>
                                                </div>
                                                <div className="stock-name">
                                                    Facebook Inc.
                                                </div>
                                            </div>
                                            <div className="analyst-opinion-container">
                                                <div className="analyst-opinion">
                                                    76%
                                                </div>
                                            </div>
                                            <div className="social-sentiment-container">
                                                <div className="social-sentiment green">
                                                    <i className="caret up icon"></i>
                                                </div>
                                            </div>
                                            <div className="reported-revenue-container">
                                                <div className="reported-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="previous-revenue-container">
                                                <div className="previous-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="current-price-container">
                                                <div className="current-price">
                                                    $43.5
                                                </div>
                                            </div>
                                            <div className="price-target-container">
                                                <div className="price-target green">
                                                    $43.5
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="ui centered grid date_section">
                            <div className="twelve wide computer fourteen wide tablet fifteen wide mobile column">
                                <div className="row">
                                    <div className="earning_date">
                                        {this.state.valuePrevious}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="ui centered grid">
                            <div className="twelve wide computer fourteen wide tablet fifteen wide mobile column">
                                <div className="row">

                                    <div className="earnings-row-container">
                                        <div className="each-stock-container">
                                            <div className="stock-name-container">
                                                <div className="stock-code">
                                                    <span>FB</span>
                                                </div>
                                                <div className="stock-name">
                                                    Facebook Inc.
                                                </div>
                                            </div>
                                            <div className="analyst-opinion-container">
                                                <div className="analyst-opinion">
                                                    08%
                                                </div>
                                            </div>
                                            <div className="social-sentiment-container">
                                                <div className="social-sentiment red">
                                                    <i className="caret up icon"></i>
                                                </div>
                                            </div>
                                            <div className="reported-revenue-container">
                                                <div className="reported-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="previous-revenue-container">
                                                <div className="previous-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="current-price-container">
                                                <div className="current-price">
                                                    $43.5
                                                </div>
                                            </div>
                                            <div className="price-target-container">
                                                <div className="price-target green">
                                                    $50.1
                                                </div>
                                            </div>
                                        </div>
                                        <div className="each-stock-container">
                                            <div className="stock-name-container">
                                                <div className="stock-code">
                                                    <span>FB</span>
                                                </div>
                                                <div className="stock-name">
                                                    Facebook Inc.
                                                </div>
                                            </div>
                                            <div className="analyst-opinion-container">
                                                <div className="analyst-opinion">
                                                    76%
                                                </div>
                                            </div>
                                            <div className="social-sentiment-container">
                                                <div className="social-sentiment green">
                                                    <i className="caret up icon"></i>
                                                </div>
                                            </div>
                                            <div className="reported-revenue-container">
                                                <div className="reported-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="previous-revenue-container">
                                                <div className="previous-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="current-price-container">
                                                <div className="current-price">
                                                    $43.5
                                                </div>
                                            </div>
                                            <div className="price-target-container">
                                                <div className="price-target red">
                                                    $33.2
                                                </div>
                                            </div>
                                        </div>
                                        <div className="each-stock-container">
                                            <div className="stock-name-container">
                                                <div className="stock-code">
                                                    <span>FB</span>
                                                </div>
                                                <div className="stock-name">
                                                    Facebook Inc.
                                                </div>
                                            </div>
                                            <div className="analyst-opinion-container">
                                                <div className="analyst-opinion">
                                                    76%
                                                </div>
                                            </div>
                                            <div className="social-sentiment-container">
                                                <div className="social-sentiment green">
                                                    <i className="caret up icon"></i>
                                                </div>
                                            </div>
                                            <div className="reported-revenue-container">
                                                <div className="reported-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="previous-revenue-container">
                                                <div className="previous-revenue">
                                                    321.5
                                                </div>
                                            </div>
                                            <div className="current-price-container">
                                                <div className="current-price">
                                                    $43.5
                                                </div>
                                            </div>
                                            <div className="price-target-container">
                                                <div className="price-target green">
                                                    $49.6
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </StickyContainer>

                    <div className="ui segment earning_loader">
                        <div className="ui active inverted dimmer">
                            <div className="ui centered text loader">Loading More Stocks...</div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}
export default Announced;
