/**
 * Created by Rajasekaran on 20/12/16.
 */

import React from 'react';
import {LineChart, Line} from 'recharts';
const data = [
    {name: 'Page A', uv: 300},
    {name: 'Page B', uv: 400},
    {name: 'Page C', uv: 300},
    {name: 'Page D', uv: 500},
    {name: 'Page E', uv: 390}
];

class RevenueTrendChart extends React.Component {
    render () {
        return (
            <LineChart width={150} height={70} data={data}
                       margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <Line type="monotone" dataKey="uv" stroke="#8884d8" dot={{r: 2}}/>
            </LineChart>
        );
    }
}
export default RevenueTrendChart;
