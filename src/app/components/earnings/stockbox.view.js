/**
 * Created by Rajasekaran on 07/12/16.
 */

import React from 'react';
import {Component} from 'react';

/**
 * Earnings className
 */
class StockBox extends React.Component {
    render() {
        return (
                <div className="each-stock-container">
                    <div className="stock-confidence-meter">
                        75%
                    </div>
                    <div className="stock-code">
                        <span>REGN</span>
                    </div>
                    <div className="stock-name">
                        Inovio, Inc.
                    </div>
                    <div className="stock-price">
                        $123.45
                    </div>
                </div>
        );
    }
}
export default StockBox;
