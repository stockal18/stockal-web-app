/**
 * Created by Prayag on 02/08/16.
 */
//noinspection JSUnresolvedVariable
import React, {Component} from 'react'
import TrendingStocksView from './trendingstocks/trendingstocks.view';
import Signals from './signals.view';
import Info from './info/info.tabs.view';
import ComposedGraph from '../test/composedgraph';

/**
 * StockDetails Class Definition - Starting Point
 */
class StockDetails extends Component {

    static propTypes = {
        title: React.PropTypes.string
    };

    constructor(props) {
        super(props);
        this.state = {
            code: this.props.params.code,
            showCommoditiesChart: false,
            selectedCommodities: "Commodities"
        };
    }

    /**
     * Set the defaultProps
     * @type {{initialCount: number, title: string, todoSegmentTitle: string}}
     */
    static defaultProps = {
        title: 'Stock Details'
    };

    /**
     * Constructor
     * @param props
     */

    componentWillReceiveProps(nextProps){
        this.setState({
            code: nextProps.params.code,
        });
    }

    componentWillMount(){
        console.log(this.props.params.code);
    }

    onCommoditiesSelect(selected){
        this.setState({
            showCommoditiesChart: selected==="Commodities" ? false : true,
            selectedCommodities: selected
        })
    }
    render(){
        return (
            <div title={this.props.title}>
                <TrendingStocksView code={this.state.code}/>
                <Signals code={this.state.code} showCommodities={this.state.showCommoditiesChart} commoditiesSelect={this.onCommoditiesSelect.bind(this)} showChart={this.state.showPriceChart}/>
                <ComposedGraph showCommodities={this.state.showCommoditiesChart} selectedCommodities={this.state.selectedCommodities}/>
                <Info code={this.state.code} showChart={this.state.showPriceChart}/>
            </div>
        )
    }
}

export default StockDetails