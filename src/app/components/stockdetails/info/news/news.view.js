/**
 * Created by prayag on 04/08/16.
 */

import React from 'react';
import NewsDetailsView from './news.details.view';
import NewsContentView from './news.content.view';
import NoData from './../nodata.view';

/**
 * Default Layout
 */
class News extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedArticle: this.props.article[0],
            showArticleDetails: true,
            showArticleContent: false
        }
    }

    onItemClick(item) {
        this.setState({
            selectedArticle: item,
            showArticleDetails: false,
            showArticleContent: true
        });
    }

    onBackClick() {
        this.setState({
            showArticleDetails: true,
            showArticleContent: false
        });
    }

    /**
     * Rendering part of News
     * @returns {XML}
     */
    render() {
        let newsDetailsContent;
        let newsDetails;
        newsDetails = this.props.news.map(function (item, i) {
            return (
                <NewsDetailsView key={i} news={item} type={this.props.type}
                                 onClickEvent={this.onItemClick.bind(this, item)}/>
            )
        }.bind(this));
        newsDetailsContent = this.props.news.length > 0 ? (
            <div className="ui vertically padded grid info_container">
                <div
                    className={"seven wide computer column seven wide tablet column sixteen wide mobile column" + (!this.state.showArticleDetails ? " mobile_hide" : "")}>
                    <div className="heading_container">
                        <div className="mobile_show back_link" onClick={this.props.onBackClickEvent}>
                            <i className="arrow left icon"></i>
                        </div>
                        <div className="article_heading">{this.props.type === "opinion" ? "News" : "News"}</div>
                    </div>
                    <div className="article_left_container scrollable">
                        <div className="article_list">
                            {newsDetails}
                        </div>
                    </div>
                </div>
                <div
                    className={"nine wide computer column nine wide tablet column sixteen wide mobile column" + (!this.state.showArticleContent ? " mobile_hide" : "")}>
                    <div className="heading_container">
                        <div className="mobile_show back_link" onClick={this.onBackClick.bind(this)}>
                            <i className="arrow left icon"></i></div>
                        <div
                            className="article_heading">{this.props.type === "opinion" ? "Analysts' Opinion" : "News & Sentiments"}</div>
                    </div>
                    <NewsContentView type={this.props.type} selectedArticle={this.state.selectedArticle}/>
                </div>
            </div>
        ) : (
            <div className="ui centered grid nodata_container">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="heading_container">
                            <div className="article_heading">{this.props.type === "opinion" ? "News" : "News"}</div>
                        </div>
                        <NoData onCloseClickEventButton={this.props.onCloseClickEvent}/>
                    </div>
                </div>
            </div>
        );

        return ({newsDetailsContent});
    }
}

export default News;