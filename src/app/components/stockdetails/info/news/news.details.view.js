/**
 * Created by prayag on 04/08/16.
 */

import React from 'react';
import { getCalculatedTime } from './../../../../../public/assets/js/utils';
import { decodeEntities } from './../../../../../public/assets/js/utils';

class NewsDetails extends React.Component {

    /**
     * Rendering part of NewsDetails
     * @returns {XML}
     */
    render() {
        let news = this.props.news;
        let authorInitial = "";
        if(this.props.type==="opinion"){
            authorInitial = (
                <div className="author_initial">
                    <div className="author_initial_circle">{news.author.initial}</div>
                </div>
            )
        }
        return (
            <div className="article_details" onClick={this.props.onClickEvent}>
                {authorInitial}
                <div className="article_title_time">
                    <div className="article_title">
                        {decodeEntities(news.title)}
                    </div>
                    <div className="article_time">{getCalculatedTime(news.timestamp)}</div>
                </div>
            </div>
        )
    }
}

export default NewsDetails;