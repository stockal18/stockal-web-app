/**
 * Created by prayag on 12/08/16.
 */

import React from 'react';
import { getCalculatedTime } from './../../../../../public/assets/js/utils';
import { decodeEntities } from './../../../../../public/assets/js/utils';

class NewsContent extends React.Component {
    
    /**
     * Rendering part of NewsContent
     * @returns {XML}
     */
    render() {
        let selectedNews = this.props.selectedNews;
        return (
            <div className="article_right_container">
                <div className="article_title">
                    {selectedNews.title}
                </div>
                <div>
                    <span className="article_author">
                        {this.props.type === "opinion" ? selectedNews.author.name : selectedNews.source}
                    </span>
                    &nbsp;&nbsp;
                    <span className="article_time">
                        {getCalculatedTime(selectedNews.timestamp)}
                    </span>
                </div>
                <div className="article_content_parent">
                    <div className="article_content">
                        <div dangerouslySetInnerHTML={{__html:decodeEntities(selectedNews.content.replace(/\n/g, "<br />"))}}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default NewsContent;