/**
 * Created by Rajasekaran on 05/01/17.
 */

import React from 'react';
import CloseImg from  './../../../../public/assets/images/closebtnimg.png';

class NoData extends React.Component {
    render() {
        return (
                    <div>
                        <div className="mobile_show article_close" onClick={this.props.onCloseClickEventButton}>
                            <img src={CloseImg}/>
                        </div>
                        <div className="nodata_section">
                            <div className="nodata_title"> Sorry, we don’t have this data yet</div>
                            <div className="nodata_desc">We show
                                you {this.props.type === "opinion" ? "Articles" : (this.props.type === "wire" ? "News" :
                                    (this.props.type === "social" ? "Social" : "Revenue Prediction"))} only for those
                                stocks
                                where we
                                are confident about the data. We’ll get you relevant data for this stock soon.
                            </div>
                        </div>
                    </div>
        );
    }
}
export default NoData;
