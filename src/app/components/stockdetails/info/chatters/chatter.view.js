/**
 * Created by prayag on 04/08/16.
 */

import React from 'react';
import ChatterDetails from './chatter.details.view';
import CloseImg from  './../../../../../public/assets/images/closebtnimg.png';
import NoData from './../nodata.view';

class Chatter extends React.Component {

    /**
     * Rendering part of Chatter
     * @returns {XML}
     */
    constructor(props) {
        super(props);
        /*this.state = {
         chatters : this.props.chatters
         };*/
    }

    /* componentWillReceiveProps(nextProps) {
     this.setState({
     chatters : nextProps.chatters
     });
     }*/
    render() {
        let chatterDetailContent;
        let chatterDetails = this.props.chatters.map(function (item, i) {
            return (
                <ChatterDetails key={i} chatter={item} type={this.props.type}/>
            )
        }.bind(this));
        chatterDetailContent = this.props.chatters.length > 0 ? (
            <div className="ui vertically padded grid info_container">
                <div className="fourteen wide column sixteen wide mobile column">
                    <div className="heading_container">
                        <div className="mobile_show article_close" onClick={this.props.onCloseClickEvent}>
                            <img src={CloseImg}/>
                        </div>
                        <div className="article_heading">Chatter</div>
                    </div>
                    <div className="article_left_container scrollable">
                        <div className="article_list">
                            {chatterDetails}
                        </div>
                    </div>
                </div>
            </div>
        ) : (
            <div className="ui centered grid nodata_container">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="heading_container">
                            <div className="article_heading">Chatter</div>
                        </div>
                        <NoData onCloseClickEventButton={this.props.onCloseClickEvent} type={this.props.type}/>
                    </div>
                </div>
            </div>
        );

        return (<div>{chatterDetailContent}</div>);
    }
}

export default Chatter;