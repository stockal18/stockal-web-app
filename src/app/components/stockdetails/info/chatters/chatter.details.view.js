/**
 * Created by prayag on 04/08/16.
 */

import React from 'react';
import StockTwitIcon from "../../../../../public/assets/images/icon_stocktwits_article.png"
import TwitterIcon from "../../../../../public/assets/images/icon_twitter.png"
import Linkify from 'react-linkify';
import { getCalculatedTime } from './../../../../../public/assets/js/utils';
import { decodeEntities } from './../../../../../public/assets/js/utils';

class ChatterDetails extends React.Component {

    /**
     * Rendering part of ChatterDetails
     * @returns {XML}
     */

    render() {
        let chatter = this.props.chatter;
        return (
            <div className="article_details">
                <div className="article_title_time">
                    <div className="article_title">
                        <Linkify properties={{target:"_blank"}}>{decodeEntities(chatter.text)}</Linkify>
                    </div>
                    <div className="source">
                        <img src={chatter.source === "stocktwits" ? StockTwitIcon : TwitterIcon}/>
                        <div>@{chatter.handle}</div>
                        <div className="article_time">{getCalculatedTime(chatter.timestamp)}</div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ChatterDetails;