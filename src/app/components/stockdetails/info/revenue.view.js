import React, {ReactDom} from 'react';
import {ResponsiveContainer, ComposedChart, Line, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import CloseImg from  './../../../../public/assets/images/closebtnimg.png';
import RatioTable from './ratio.table.view';
import CustomizedAxis from './../../test/customizedaxistick';
import NoData from './nodata.view';

class Revenue extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        return JSON.stringify(this.props.stockData.estimize) != JSON.stringify(nextProps.stockData.estimize);
    }
    render() {
        let revenueGraph;
        let revenueData = this.props.revenue.map(function (each) {
            return {
                period: "Q" + each.fiscal_quarter + " '" + each.fiscal_year.substring(2),
                estimate: each.consensus_revenue_estimate == isNaN(each.consensus_revenue_estimate) ? "NA" : Math.round(each.consensus_revenue_estimate),
                revenue: each.revenue == "" ? "NA" : Math.round(each.revenue)
            };
        });
        revenueGraph = this.props.revenue.length > 0 ? (
            <div className="ui grid revenue_container">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="mobile_show article_close"
                             onClick={this.props.onCloseClickEvent}>
                            <img src={CloseImg}/>
                        </div>
                        <div className="ui middle aligned centered grid">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="heading_container">
                                        <div className="article_heading">Revenue Prediction</div>
                                    </div>
                                    <div className="price-history-container_revenue">
                                        <ResponsiveContainer>
                                            <ComposedChart data={revenueData}
                                                           margin={{top: 5, right: 30, left: 0, bottom: 5}}>
                                                <XAxis dataKey="period" axisLine={{stroke: '#AAAAAA'}}
                                                       tick={{stroke: '#AAAAAA', strokeWidth: 0}}/>
                                                <YAxis axisLine={{stroke: '#AAAAAA'}}
                                                       valueTick={{stroke: '#AAAAAA', strokeWidth: 0}}
                                                       tick={<CustomizedAxis/>}/>
                                                <Tooltip />
                                                <Legend />
                                                <CartesianGrid stroke="#f5f5f5" x={45}/>
                                                <Bar dataKey="revenue" name="Actual (mn USD)" fill="#7ed321"/>
                                                <Line type="monotone" dataKey="estimate" name="Estimated (mn USD)"
                                                      stroke="#FF0006"
                                                      strokeWidth={2}/>
                                            </ComposedChart>
                                        </ResponsiveContainer>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <RatioTable code={this.props.code}/>
                    </div>
                </div>
            </div>
        ) : (
            <div className="ui centered grid revenue_container">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="heading_container">
                            <div className="article_heading">Revenue Prediction</div>
                        </div>
                        <div className="ui centered grid">
                            <div className="twelve wide computer twelve wide tablet fourteen wide mobile column">
                                <div className="row">
                        <NoData onCloseClickEventButton={this.props.onCloseClickEvent}/>
                                    </div>
                                </div>
                            </div>
                        <RatioTable code={this.props.code}/>
                    </div>
                </div>
            </div>
        );
        return (<div>{revenueGraph}</div>);
    }
}
export default Revenue;