/**
 * Created by prayag on 04/08/16.
 */

import React from 'react';
import Articles from './articles/articles.view'
import Revenue from './revenue.view'
import Chatter from './chatters/chatter.view'

/**
 * Default Layout
 */
class InfoContent extends React.Component {

    constructor(props) {
        super(props);
    }
    /*shouldComponentUpdate(nextProps, nextState){
        return (this.props.selectedTab != nextProps.selectedTab) ||
            ((this.props.data.symbol != nextProps.data.symbol ||
            this.props.stockData.code != nextProps.stockData.code) &&
            nextProps.data.symbol == nextProps.stockData.code);
    }*/

    render(){
        let data = this.props.data;
        let stockData = this.props.stockData;
        let content;
        switch (this.props.selectedTab) {
            default:
            case "opinion":
                content = (
                    <Articles type="opinion" article={data.opinions} onCloseClickEvent={this.props.onCloseButtonClick}/>
                );
                break;
            case "wire":
                content = (
                    <Articles type="wire" article={data.wires} onCloseClickEvent={this.props.onCloseButtonClick}/>
                );
                break;
            case "social":
                content = (
                    <Chatter type="social" chatters={data.chatters} onCloseClickEvent={this.props.onCloseButtonClick}/>
                );
                break;
            case "revenue":
                content = (
                    <Revenue code={this.props.code} type="revenue" revenue={stockData.estimize} onCloseClickEvent={this.props.onCloseButtonClick}/>
                );
                break;
        }
        return (
            <div>{content}</div>
        )
    }
}

export default InfoContent