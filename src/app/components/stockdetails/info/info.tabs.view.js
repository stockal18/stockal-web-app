/**
 * Created by prayag on 04/08/16.
 */

import React from 'react';
import SignalsStore from '../../../fixtures/signals/signals.store';
import InfoStore from '../../../fixtures/info/info.store';
import SignalsAction from '../../../fixtures/signals/signals.actions';
import InfoAction from '../../../fixtures/info/info.actions';
import connectToStores from 'alt-utils/lib/connectToStores';
import InfoContent from './info.content.view';
import SocialMediaPulseLower from '../../../../public/assets/images/icon_pulse_low.png';
import SocialMediaPulseHigher from '../../../../public/assets/images/icon_pulse_high.png';

/**
 * Default Layout
 */
class InfoTabs extends React.Component {

    static getStores() {
        return [SignalsStore, InfoStore];
    }

    static getPropsFromStores() {
        return {
            ...SignalsStore.getState(),
            ...InfoStore.getState()
        }
    }

    /**
     * Constructor
     * @param props - properties
     */
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: "opinion",
            showTabs: true,
            showTabContent: false,
        }
    }

    /**
     * componentDidMount
     */
    componentDidMount() {
        SignalsAction.fetch(this.props.code);
        InfoAction.fetch(this.props.code);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.code != nextProps.code) {
            this.setState({
                selectedTab: "opinion",
                showTabs: true,
                showTabContent: false
            });
            SignalsAction.clear();
            InfoAction.clear();
            SignalsAction.fetch(nextProps.code);
            InfoAction.fetch(nextProps.code);
        }
    }

    /**
     * componentWillUnmount
     */
    componentWillUnmount() {
        SignalsAction.clear();
        InfoAction.clear()
    }

    switchTab(tabName) {
        this.setState({
            selectedTab: tabName,
            showTabs: false,
            showTabContent: true
        });
    }

    /*shouldComponentUpdate(nextProps, nextState){
     if(JSON.stringify(this.state) != JSON.stringify(nextState)){
     return true
     }
     else {
     if(this.props.infoLoaded && this.props.signalsLoaded && (this.props.code == nextProps.code)){
     return false;
     } else {
     return (nextProps.infoLoaded && nextProps.signalsLoaded);
     }
     }
     }*/

    onCloseButton() {
        this.setState({
            showTabs: true,
            showTabContent: false
        });
    }

    /**
     * Rendering part of InfoTabs
     * @returns {XML}
     */
    render() {
        let stockData;
        let infocontent;
        let confidencemeter, news, socialpulse, revenueprediction;
        confidencemeter = news = socialpulse = revenueprediction = (
            <div className="infographic_circle info-loading"></div>
        );
        infocontent = (
            <div className="ui segment infocontent_load">
                <div className="ui active inverted dimmer">
                    <div className="ui centered text loader">Loading</div>
                </div>
            </div>
        );
        if (this.props.signalsLoaded) {
            stockData = this.props.stockData;
            confidencemeter = (
                <div
                    className={"infographic_circle " + (stockData.benzingaBuyPercent <= 40 ? "border_red red" : (stockData.benzingaBuyPercent > 55 ? "border_green green" : "border_blue blue"))}>
                    {stockData.benzingaBuyPercent}%<br/>
                    <div className="suggestion">
                        {stockData.benzingaBuyPercent <= 40 ? "Bearish" : (stockData.benzingaBuyPercent > 55 ? "Bullish" : "Neutral")}
                    </div>
                </div>
            );
            news = (
                <div
                    className={"infographic_circle " + (stockData.sentimentIndex.index > 0 ? "border_green green" : (stockData.sentimentIndex.index < 0 ? "border_red red" : "border_green green"))}>
                    {isNaN(stockData.sentimentIndex.index) ? (stockData.sentimentIndex.index = "N.A.") : (stockData.sentimentIndex.index = Math.round(Math.abs(stockData.sentimentIndex.index))) + "%"}
                    <br/>
                    <i className={stockData.sentimentIndex.index > 0 ? "caret up icon" : (stockData.sentimentIndex.index < 0 ? "caret down icon" : "")}></i>
                </div>
            );
            socialpulse = (
                <div
                    className={"infographic_circle " + (stockData.socialVelocity.velocity >= 50 ? "border_green green" : "border_red red")}>
                    <img
                        src={stockData.socialVelocity.velocity >= 50 ? SocialMediaPulseHigher : SocialMediaPulseLower}/>
                    <br/>
                    {Math.floor(stockData.socialVelocity.velocity)}%
                    <div className="suggestion">
                        {stockData.socialVelocity.velocity >= 50 ? "Higher" : "Lower"} than normal
                    </div>
                </div>
            );
            revenueprediction = (
                <div
                    className={"infographic_circle " + (stockData.revenuePrediction === "positive" ? "border_green green" : (stockData.revenuePrediction === "negative" ? "border_red red" : "border_green green"))}>
                    {stockData.revenuePrediction === "positive" ?
                        <i className='caret up icon'></i> : (stockData.revenuePrediction === "negative" ?
                        <i className='caret down icon'></i> : <div className="no_value">N.A.</div>)}
                </div>
            );
        }
        if (this.props.infoLoaded) {
            infocontent = (
                <div className={"ui container" + (!this.state.showTabContent ? " mobile_hide" : "")}>
                    <InfoContent code={this.props.code} selectedTab={this.state.selectedTab} data={this.props.data} stockData={stockData}
                                 onCloseButtonClick={this.onCloseButton.bind(this)}/>
                </div>
            );
        }
        return (
            <div className={this.props.showChart ? "infocontent_hide" : ""}>
                <div className={"ui bg-green" + (!this.state.showTabs ? " mobile_hide" : "")}>
                    <div className="sixteen wide column">
                        <div className="ui middle aligned vertically padded grid container">
                            <div
                                className="four wide computer column four wide tablet column sixteen wide mobile column"
                                onClick={this.switchTab.bind(this, "opinion")}>
                                <div className="row infographic confidence_meter">
                                    <span className="infographic_text">
                                        Confidence Meter
                                    </span>
                                    {confidencemeter}
                                </div>
                                <div
                                    className={"mobile_hide" + (this.state.selectedTab === "opinion" ? " arrow-down" : "")}></div>
                            </div>
                            <div
                                className="four wide computer column four wide tablet column sixteen wide mobile column"
                                onClick={this.switchTab.bind(this, "wire")}>
                                <div className="row infographic news_sentiment">
                                    <span className="infographic_text">
                                        News & Sentiment
                                    </span>
                                    {news}
                                </div>
                                <div
                                    className={"mobile_hide" + (this.state.selectedTab === "wire" ? " arrow-down" : "")}></div>
                            </div>
                            <div
                                className="four wide computer column four wide tablet column sixteen wide mobile column"
                                onClick={this.switchTab.bind(this, "social")}>
                                <div className="row infographic social_media_pulse">
                                    <span className="infographic_text">
                                        Social Media Pulse
                                    </span>
                                    {socialpulse}
                                </div>
                                <div
                                    className={"mobile_hide" + (this.state.selectedTab === "social" ? " arrow-down" : "")}></div>
                            </div>
                            <div
                                className="four wide computer column four wide tablet column sixteen wide mobile column"
                                onClick={this.switchTab.bind(this, "revenue")}>
                                <div className="row infographic revenue_prediction">
                                    <span className="infographic_text">
                                        Company Performance
                                    </span>
                                    {revenueprediction}
                                </div>
                                <div
                                    className={"mobile_hide" + (this.state.selectedTab === "revenue" ? " arrow-down" : "")}></div>
                            </div>
                        </div>
                    </div>
                </div>
                {infocontent}
            </div>
        );
    }
}
export default connectToStores(InfoTabs);