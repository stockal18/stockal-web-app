/**
 * Created by Rajasekaran on 30/12/16.
 */

import React from 'react';
import RatioTableStore from '../../../fixtures/ratiotable/ratiotable.store';
import RatioTableActions from '../../../fixtures/ratiotable/ratiotable.actions';
import connectToStores from 'alt-utils/lib/connectToStores';

class RatioTable extends React.Component {

    static getStores() {
        return [RatioTableStore];
    }

    static getPropsFromStores() {
        return RatioTableStore.getState();
    }

    componentDidMount() {
        RatioTableActions.fetch(this.props.code);
    }

    componentWillUnmount() {
        RatioTableActions.clear();
    }

    render() {
        let ratioTable = this.props.ratioTable;
        console.log(ratioTable);
        return (
            <div className="ui grid container ratio_table_section">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="ui grid">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="heading_container">
                                        <div className="article_heading">Performance Ratios</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid ratio_table_content">
                            <div className="five wide computer six wide tablet ten wide mobile column">
                                <div className="row">
                                    <div className="first_column">
                                        <div className="table_content">P/E</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">P/BV</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">P/Sales</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">EV/EBITDA</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">RoE</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">Profit Margin</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">Dividend Yield</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">Dividend Payout Ratio</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">FCF/Share</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">Current Ratio</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">Debt/Equity</div>
                                    </div>
                                    <div className="first_column">
                                        <div className="table_content">Tangible Asset BV/Share</div>
                                    </div>
                                </div>
                            </div>
                            <div className="three wide computer four wide tablet five wide mobile column">
                                <div className="row">
                                    <div className="second_column">
                                        <div className="table_content">
                                            {isNaN(ratioTable.pe1) ? (ratioTable.pe1 = "N.A.") : (ratioTable.pe1 = parseFloat(ratioTable.pe1).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div className="table_content">
                                            {isNaN(ratioTable.pb) ? (ratioTable.pb = "N.A.") : (ratioTable.pb = parseFloat(ratioTable.pb).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div className="table_content">
                                            {isNaN(ratioTable.ps1) ? (ratioTable.ps1 = "N.A.") : (ratioTable.ps1 = parseFloat(ratioTable.ps1).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div
                                            className="table_content">
                                            {isNaN(ratioTable.evebitda) ? (ratioTable.evebitda = "N.A.") : (ratioTable.evebitda = parseFloat(ratioTable.evebitda).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div className="table_content">
                                            {isNaN(ratioTable.roe) ? (ratioTable.roe = "N.A.") : (ratioTable.roe = parseFloat(ratioTable.roe).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div
                                            className="table_content">
                                            {isNaN(ratioTable.netmargin) ? (ratioTable.netmargin = "N.A.") : (ratioTable.netmargin = parseFloat(ratioTable.netmargin).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div
                                            className="table_content">
                                            {isNaN(ratioTable.divyield) ? (ratioTable.divyield = "N.A.") : (ratioTable.divyield = parseFloat(ratioTable.divyield).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div
                                            className="table_content">
                                            {isNaN(ratioTable.payoutratio) ? (ratioTable.payoutratio = "N.A.") : (ratioTable.payoutratio = parseFloat(ratioTable.payoutratio).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div className="table_content">
                                            {isNaN(ratioTable.fcfps) ? (ratioTable.fcfps = "N.A.") : (ratioTable.fcfps = parseFloat(ratioTable.fcfps).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div
                                            className="table_content">
                                            {isNaN(ratioTable.currentratio) ? (ratioTable.currentratio = "N.A.") : (ratioTable.currentratio = parseFloat(ratioTable.currentratio).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div className="table_content">
                                            {isNaN(ratioTable.de) ? (ratioTable.de = "N.A.") : (ratioTable.de = parseFloat(ratioTable.de).toFixed(3))}
                                        </div>
                                    </div>
                                    <div className="second_column">
                                        <div className="table_content">
                                            {isNaN(ratioTable.tbvps) ? (ratioTable.tbvps = "N.A.") : (ratioTable.tbvps = parseFloat(ratioTable.tbvps).toFixed(3))}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default connectToStores(RatioTable);