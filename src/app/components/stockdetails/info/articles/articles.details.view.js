/**
 * Created by prayag on 04/08/16.
 */

import React from 'react';
import { getCalculatedTime } from './../../../../../public/assets/js/utils';
import { decodeEntities } from './../../../../../public/assets/js/utils';

class ArticlesDetails extends React.Component {

    /**
     * Rendering part of ArticlesDetails
     * @returns {XML}
     */
    constructor(props) {
        super(props);
        this.state = {
            selected: this.props.selected
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            selected: nextProps.selected
        });
    }
    render() {
        let article = this.props.article;
        let authorInitial = "";
        if(this.props.type==="opinion"){
            authorInitial = (
                <div className="author_initial">
                    <div className="author_initial_circle">{article.author.initial}</div>
                </div>
            )
        }
        return (
            <div className={"article_details" + (this.state.selected ? " article_selected" : "")} onClick={this.props.onClickEvent}>
                {authorInitial}
                <div className="article_title_time">
                    <div className="article_title">
                        {decodeEntities(article.title)}
                    </div>
                    <div className="article_time">{getCalculatedTime(article.timestamp)}</div>
                </div>
            </div>
        )
    }
}

export default ArticlesDetails;