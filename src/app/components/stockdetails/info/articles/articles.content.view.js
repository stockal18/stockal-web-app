/**
 * Created by prayag on 12/08/16.
 */

import React from 'react';
import ArticlesShare from './articles.share.view';
import {getCalculatedTime} from './../../../../../public/assets/js/utils';
import {decodeEntities} from './../../../../../public/assets/js/utils';

class ArticlesContent extends React.Component {
    componentDidUpdate() {
        var articleContent = document.getElementById("scrollDIV");
        articleContent.scrollTop = 0;
    }

    render() {
        let selectedArticle = this.props.selectedArticle;
        return (
            <div className="article_right_container">
                <div className="article_title">
                    {decodeEntities(selectedArticle.title)}
                </div>
                <div>
                    <span className="article_author">
                        {this.props.type === "opinion" ? selectedArticle.author.name : selectedArticle.source}
                    </span>
                    &nbsp;&nbsp;
                    <span className="article_time">
                        {getCalculatedTime(selectedArticle.timestamp)}
                    </span>
                    <a href={selectedArticle.link} target="_blank" className="right_globeicon">
                        <i className="fa fa-globe fa-2x" title="Click to read full article" aria-hidden="true"></i></a>
                </div>
                <div className="article_content_parent scrollable" id="scrollDIV">
                    <div className="article_content">
                        <div
                            dangerouslySetInnerHTML={{__html: decodeEntities(selectedArticle.content.replace(/\n/g, "<br />"))}}/>
                        <ArticlesShare/>
                    </div>
                </div>
            </div>
        );
    }
}
export default ArticlesContent;