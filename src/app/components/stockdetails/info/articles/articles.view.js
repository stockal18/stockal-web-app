/**
 * Created by prayag on 04/08/16.
 */

import React from 'react';
import ArticlesDetailsView from './articles.details.view';
import ArticlesContentView from './articles.content.view';
// import FacebookLogin from 'react-facebook-login';
// import GoogleLogin from 'react-google-login';
import CloseImg from  './../../../../../public/assets/images/closebtnimg.png';
import NoData from './../nodata.view';

/**
 * Default Layout
 */
class Articles extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedArticle: this.props.article[0],
            showArticleDetails: true,
            showArticleContent: false
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            selectedArticle: nextProps.article[0],
            showArticleDetails: true,
            showArticleContent: false
        });
    }

    onItemClick(item) {
        this.setState({
            selectedArticle: item,
            showArticleDetails: false,
            showArticleContent: true
        });
    }

    onBackButton() {
        this.setState({
            showArticleDetails: true,
            showArticleContent: false
        });
    }

    /**
     * Rendering part of Articles
     * @returns {XML}
     */
    render() {
        let articleDetailContent;
        let articleDetails = this.props.article.map(function (item, i) {
            return (
                <ArticlesDetailsView key={i} article={item} type={this.props.type}
                                     onClickEvent={this.onItemClick.bind(this, item)}
                                     selected={this.state.selectedArticle._id === item._id}/>
            )
        }.bind(this));
        articleDetailContent = this.props.article.length > 0 ? (
            <div className="ui vertically padded grid info_container">
                <div
                    className={"seven wide computer column seven wide tablet column sixteen wide mobile column" + (!this.state.showArticleDetails ? " mobile_hide" : "")}>
                    <div className="row">
                        <div className="heading_container">
                            <div className="mobile_show article_close" onClick={this.props.onCloseClickEvent}>
                                <img src={CloseImg}/>
                            </div>
                            <div className="article_heading">{this.props.type === "opinion" ? "Articles" : "News"}</div>
                        </div>
                        <div className="article_left_container scrollable">
                            <div className="article_list">
                                {articleDetails}
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    className={"nine wide computer column nine wide tablet column sixteen wide mobile column" + (!this.state.showArticleContent ? " mobile_hide" : "")}>
                    <div className="row">
                        <div className="heading_container">
                            <div className="mobile_show back_link" onClick={this.onBackButton.bind(this)}>
                                <i className="arrow left icon"></i></div>
                            <div className="mobile_show article_close" onClick={this.props.onCloseClickEvent}>
                                <img src={CloseImg}/>
                            </div>
                            <div
                                className="article_heading">{this.props.type === "opinion" ? "Analysts' Opinion" : "News & Sentiments"}
                            </div>
                        </div>
                        <ArticlesContentView type={this.props.type} selectedArticle={this.state.selectedArticle}/>
                    </div>
                </div>
            </div>
        ) : (
            <div className="ui centered grid nodata_container">
                <div className="sixteen wide column">
                    <div className="row">

                        <div className="heading_container">
                            <div className="article_heading">{this.props.type === "opinion" ? "Articles" : "News"}</div>
                        </div>
                        <div className="ui middle aligned centered grid">
                            <div className="twelve wide computer twelve wide tablet fourteen wide mobile column">
                                <div className="row">
                                    <NoData type={this.props.type}
                                            onCloseClickEventButton={this.props.onCloseClickEvent}/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );

        return (<div> {articleDetailContent} </div>);
    }
}
export default Articles;