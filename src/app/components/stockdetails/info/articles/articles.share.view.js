/**
 * Created by Rajasekaran on 20/09/16.
 */

import React from 'react';
import BtnAppStoreUrl from '../../../../../public/assets/images/btn_appstore.png';
import BtnPlayStoreUrl from '../../../../../public/assets/images/btn_playstore.png';

/* ArticlesShare Class */
class ArticlesShare extends React.Component {
    render() {
        return (
            <div className="ui grid appshare_section">
                <div className="ten wide computer ten wide tablet sixteen wide mobile column">
                    <div className="ui grid appshare_content">
                        <div className="appshare_title"> Share this article</div>
                        <div className="appshare_desc">You can share this article and create watchlists using Stockal app.</div>
                    </div>
                </div>
                <div className="four wide computer six wide tablet sixteen wide mobile column">
                    <div className="appshare_img_section">
                        <a href="http://bit.ly/stockal-apple" target="_blank"><img src={BtnAppStoreUrl} className="appshare_img"/></a>
                        <a href="http://bit.ly/stockal-android" target="_blank"><img src={BtnPlayStoreUrl} className="appshare_img"/></a>
                    </div>
                </div>
            </div>
        );
    }
}
export default ArticlesShare;




