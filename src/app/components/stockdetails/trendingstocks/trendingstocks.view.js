/**
 * Created by prayag on 04/08/16.
 */

import React from 'react';
import TrendingStocksStore from '../../../fixtures/trendingstocks/trendingstocks.store';
import TrendingStocksAction from '../../../fixtures/trendingstocks/trendingstocks.actions';
import connectToStores from 'alt-utils/lib/connectToStores';
import TrendingStocksSegment from './trendingstocks.segment';

/**
 * TrendingStocksView
 */
class TrendingStocksView extends React.Component {

    static getStores() {
        return [TrendingStocksStore];
    }

    static getPropsFromStores() {
        return TrendingStocksStore.getState();
    }

    /**
     * Constructor
     * @param props - properties
     */
    constructor(props) {
        super(props);
        let emptyTrendingStocks = [];
        for (var i = 0; i < 15; i++) {
            emptyTrendingStocks.push('');
        }
    }

    /**
     *
     * componentDidMount
     */
    componentDidMount() {
        window.scrollTo(0,0);
        TrendingStocksAction.fetch();
    }

    /**
     * componentWillUnmount
     */
    componentWillUnmount() {
        TrendingStocksAction.clear();
    }

    /**
     * Render
     * @returns {XML}
     */
    render() {
        /** prepared the content **/
        let content;
        if (this.props.trendingStocksLoaded) {
            content = this.props.trendingStocks.map(function (item, i) {
                return (
                    <TrendingStocksSegment key={i} stock={item}/>
                )
            }.bind(this))

        }
        else {
            content = (
                <div className="trending-progress">
                    <div className="trending-progress-bar"></div>
                </div>
            )
        }
        return (
            <div className="ui">
                {content}
            </div>);
    }
}
export default connectToStores(TrendingStocksView);