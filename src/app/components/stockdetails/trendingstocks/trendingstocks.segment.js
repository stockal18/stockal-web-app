/**
 * Created by sarovar on 12/07/16.
 */

import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router';

/**
 * TrendingStocksSegment
 */
class TrendingStocksSegment extends Component {

    /**
     * Render the TrendingStocksSegment
     * @returns {XML}
     */

    render(){
        return (
            <div className="single_stock">
                <Link to={"/stock-details/"+this.props.stock.code}>
                    <div className="single_stock_text">{this.props.stock.code}</div>
                </Link>
            </div>
        );
    }
}

export default TrendingStocksSegment;