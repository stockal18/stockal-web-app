/**
 * Created by prayag on 04/08/16.
 */

import React from 'react';
import SignalsStore from './../../fixtures/signals/signals.store';
import SignalsAction from './../../fixtures/signals/signals.actions';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlight from 'autosuggest-highlight';
import connectToStores from 'alt-utils/lib/connectToStores';
import {Link} from 'react-router';
import CurrencyImg from './../../../public/assets/images/icon_currency.png';

var data = require('./../../../public/assets/others/newSymbolsListLatest.csv');
var stockList = [];
for (var i = 1; i < data.length; i++) {
    var obj = {};
    obj = {code: data[i][0], company: data[i][1]}
    stockList.push(obj);
}

function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function getSuggestions(value) {
    const escapedValue = escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
        return [];
    }
    const regex = new RegExp('\\b' + escapedValue, 'i');
    return stockList.filter(stockList => regex.test(`${stockList.code}, ${stockList.company}`));
}

function getSuggestionValue(suggestion) {
    return `${suggestion.code}`;
}

function renderSuggestion(suggestion, {query}) {
    const suggestionText = `${suggestion.code}, ${suggestion.company}`;
    const matches = AutosuggestHighlight.match(suggestionText, query);
    const parts = AutosuggestHighlight.parse(suggestionText, matches);
    return (
        <span className="name">
        {
            parts.map((part, index) => {
                const className = part.highlight ? 'highlight' : null;

                return (
                    <span className={className} key={index}>{part.text}</span>
                );
            })
        }
        </span>
    );
}
/**
 * Default Layout
 */
class Signals extends React.Component {

    static getStores() {
        return [SignalsStore];
    }

    static getPropsFromStores() {
        return SignalsStore.getState();
    }

    /**
     * Constructor
     * @param props - properties
     */
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            suggestions: getSuggestions(''),
            search: false,
            searchSelectedCode: '',
            selectValue: 'Commodities'
        };
    }

    handleChangeOption(e) {
        this.setState({
            selectValue: e.target.value
        });
        this.props.commoditiesSelect(e.target.value);
    }

    /**
     * componentDidMount
     */
    componentDidMount() {
        SignalsAction.fetch(this.props.code)
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.code != nextProps.code) {
            this.setState({
                value: '',
                suggestions: getSuggestions(''),
                search: false,
                searchSelectedCode: ''
            });
        }
    }

    /**
     * componentWillUnmount
     */
    componentWillUnmount() {
        SignalsAction.clear();
    }

    onChange = (event, {newValue, method}) => {
        this.setState({
            value: newValue
        });
    };

    onSuggestionsUpdateRequested = ({value}) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    saveInput = autosuggest => {
        //this.input = autosuggest.input;
    };

    onSuggestionsFetchRequested = ({value}) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected = (event, {suggestion, suggestionValue, sectionIndex, method}) => {
        this.setState({
            search: true,
            searchSelectedCode: suggestionValue
        });
    };

    /**
     * Rendering part of Signals
     * @returns {XML}
     */
    render() {
        let stockData = "";
        let stockName = (
            <div className="stock_name">
                <div className="stock_symbol">{this.props.code}</div>
            </div>
        );
        let stockPrice = (
            <div>Fetching price <br/>data of {this.props.code}...</div>
        );
        const {value, suggestions} = this.state;
        const inputProps = {
            placeholder: "Search other stocks",
            value,
            onChange: this.onChange
        };

        let search = (
            <span className="search_icon"><i className="search icon"></i></span>
        );

        if (this.state.search) {
            search = (
                <Link to={"/stock-details/" + this.state.searchSelectedCode}>
                    <span className="search_icon"><i className="search icon"></i></span>
                </Link>
            )
        }

        if (this.props.signalsLoaded) {
            stockData = this.props.stockData;
            stockName = (
                <div className="stock_name">
                    <div className="stock_symbol">{stockData.code}</div>
                    <div className="stock_full_name">
                        {stockData.company}
                    </div>
                </div>
            );
            stockPrice = (
                <div className="stock_price_container">
                    <div className="stock_price">
                        <span className="currency_symbol">$</span>
                        <span className="amount">
                            {isNaN(stockData.lastTradePrice) ? (stockData.lastTradePrice = "N.A") : (stockData.lastTradePrice = parseFloat(stockData.lastTradePrice).toFixed(2))}
                         </span><br />
                        <span className="changed_percent green">
                            <i className="caret up icon">
                            </i>
                            {Math.abs(parseFloat(stockData.change).toFixed(2))}%
                        </span>
                    </div>
                </div>
            );
            console.log('LastTradePrice:' + (isNaN(stockData.lastTradePrice) ? (stockData.lastTradePrice = "N.A") : (stockData.lastTradePrice = parseFloat(stockData.lastTradePrice).toFixed(2))));
        }
        let averageTargetPrice = !isNaN(parseFloat(stockData.averageTargetPrice)) ? (
            <div className="stock_price_details">
                <img src={CurrencyImg}/>
                <div className="stock_price_details_text">
                    <div>&nbsp;&nbsp;Avg.Target Price : ${parseFloat(stockData.averageTargetPrice).toFixed(2)}</div>
                </div>
            </div>) : "";
        let content = (
            <div className="ui grid bg-green">
                <div className="sixteen wide column">
                    <div className="ui middle aligned vertically padded grid container">
                        <div
                            className="three wide computer five wide tablet eight wide mobile column">
                            {stockName}
                        </div>
                        <div
                            className="two wide computer five wide tablet eight wide mobile column">
                            {stockPrice}
                        </div>
                        <div className="three wide computer five wide tablet eight wide mobile column">
                            {averageTargetPrice}
                        </div>
                        <div className="four wide computer eight wide tablet eight wide mobile column">
                            <div className="commodity_section">
                                <select className="commodity_selection" value={this.state.selectValue}
                                        onChange={this.handleChangeOption.bind(this)}>
                                    <option className="commodity_option" value="Commodities">Commodities</option>
                                    <option className="commodity_option" value="Aluminum">Aluminum</option>
                                    <option className="commodity_option" value="Coal">Coal</option>
                                    <option className="commodity_option" value="Copper">Copper</option>
                                    <option className="commodity_option" value="Cotton">Cotton</option>
                                    <option className="commodity_option" value="Crude oil, Brendt">Crude oil, Brendt
                                    </option>
                                    <option className="commodity_option" value="Crude oil, WTI">Crude oil, WTI</option>
                                    <option className="commodity_option" value="Gold">Gold</option>
                                    <option className="commodity_option" value="Iron ore">Iron ore</option>
                                    <option className="commodity_option" value="Lead">Lead</option>
                                    <option className="commodity_option" value="Maize">Maize</option>
                                    <option className="commodity_option" value="Natural">Natural gas</option>
                                    <option className="commodity_option" value="Nickel">Nickel</option>
                                    <option className="commodity_option" value="Palladium">Palladium</option>
                                    <option className="commodity_option" value="Palm oil">Palm oil</option>
                                    <option className="commodity_option" value="Platinum">Platinum</option>
                                    <option className="commodity_option" value="Silver">Silver</option>
                                    <option value="Tin">Tin</option>
                                    <option value="Uranium">Uranium</option>
                                    <option value="Zinc">Zinc</option>
                                </select>
                            </div>
                        </div>
                        <div
                            className="four wide computer eight wide tablet sixteen wide mobile column">
                            <div className="stocks_top_search">
                                <Autosuggest className="stock_search_box"
                                             suggestions={suggestions}
                                             onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested}
                                             onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                             onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                             onSuggestionSelected={this.onSuggestionSelected}
                                             getSuggestionValue={getSuggestionValue}
                                             renderSuggestion={renderSuggestion}
                                             focusFirstSuggestion={true}
                                             inputProps={inputProps}
                                             ref={this.saveInput}/>
                                {search}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
        return (<div>{content}</div>)
    }
}

export default connectToStores(Signals);