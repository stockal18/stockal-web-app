/**
 * Created by prayag on 26/08/16.
 */
import React from 'react';
import {ResponsiveContainer, AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, Legend}  from 'recharts';
import CloseImg from  './../../../public/assets/images/closebtnimg.png';

class PriceHistory extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        return JSON.stringify(this.props.stockData.historicalPrice) != JSON.stringify(nextProps.stockData.historicalPrice);
    }

    render() {
        let historyData = this.props.historicalPrice.map(function (each) {
            return {date: formatDate(each.date), price: parseFloat((parseFloat(each.price)).toFixed(2))};
        });
        var areaColor = historyData[29].price - historyData[0].price > 0 ? "#38BD7C"
            : (historyData[29].price - historyData[0].price < 0 ? "#FF0006" : "#F7C850");
        return (
            <div className="ui middle aligned centered grid chart_container">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="heading_container">
                            <div className="chart_mobile_show chart_close" onClick={this.props.onCloseButtonClick}>
                                <img src={CloseImg}/>
                            </div>
                            <div className="article_heading">Price History</div>
                        </div>
                        <div className="price-history-container">
                            <ResponsiveContainer>
                                <AreaChart data={historyData}
                                           margin={{top: 20, right: 50, left: 20, bottom: 20}}>
                                    <defs>
                                        <linearGradient id="colorPrice" x1="0" y1="0" x2="0" y2="1">
                                            <stop offset="5%" stopColor={areaColor} stopOpacity={0.8}/>
                                            <stop offset="95%" stopColor={areaColor} stopOpacity={0}/>
                                        </linearGradient>
                                    </defs>
                                    <Area dot={true} type="monotone" dataKey="price" stroke="#F7BD33" strokeWidth={2}
                                          fillOpacity={1} fill="url(#colorPrice)"/>
                                    <XAxis dataKey="date" label="Date" axisLine={{stroke: '#AAAAAA'}}
                                           tick={<CustomizedAxisTick/>}/>
                                    <YAxis domain={['auto', 'auto']} allowDecimals={false} label="Price"
                                           axisLine={{stroke: '#AAAAAA'}}/>
                                    <CartesianGrid strokeDasharray="1 1"/>
                                    <Tooltip/>
                                </AreaChart>
                            </ResponsiveContainer>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function formatDate(date) {
    return new Date(date).toDateString().substring(4, 10);
}
class CustomizedAxisTick extends React.Component {
    render() {
        const {x, y, stroke, payload} = this.props;
        return (
            <g transform={`translate(${x},${y})`}>
                <text x={0} y={0} dy={16} textAnchor="end" fill="#666" transform="rotate(-60)">{payload.value}</text>
            </g>
        );
    }
}
export default PriceHistory;