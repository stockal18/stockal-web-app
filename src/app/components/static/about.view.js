/**
 * Created by Rajasekaran on 23/09/16.
 */

import React from 'react';
import ReactDOM from 'react-dom';

/**
 * About Class
 */
class About extends React.Component {

    render() {
        return (
            <div className="ui middle aligned centered grid">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="ui middle aligned centered grid">
                            <div className="nine wide computer ten wide tablet twelve wide mobile column title_section">
                                <div className="row">
                                    <div className="ui grid">
                                        <div className="title"> About Stockal</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid">
                            <div className="nine wide computer ten wide tablet fourteen wide mobile column content">
                                <div className="row">
                                    <div className="ui grid stockal_video">
                                        <iframe className="embed-responsive-item"
                                                src="https://www.youtube.com/embed/nQ6UkjaCPtE" frameBorder="0"></iframe>
                                    </div>
                                    <div className="answer">
                                        <p>
                                            Stockal is an investment intelligence platform that seeks to help you manage
                                            your stock investments better. It presents assimilated data, information and
                                            signals to help you reduce the amount you have to spend on researching
                                            stocks, talking to advisors, reading and following latest news updates and
                                            analyzing historic price-trends to - hopefully - predict future stock price
                                            movements. Stockal goes through thousands of online sources, processes
                                            millions of data points every day and curates just the kind of information
                                            you might need for making better investing decisions.
                                        </p>
                                        <p>
                                            We hope that with more time at hand, you will be able to create better
                                            strategies and come up with interesting investing ideas. All in all,
                                            hopefully you’ll trade better!
                                        </p>
                                        <p>
                                            That said, Stockal is still young and constantly evolving, improving. We
                                            would really appreciate your critique, ideas and thoughts for making Stockal
                                            better and hopefully the only stock market app you need to use. Please feel
                                            free to write to the founders of Stockal at friends@stockal.com.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default About;

