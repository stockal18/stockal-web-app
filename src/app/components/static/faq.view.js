/**
 * Created by Rajasekaran on 23/09/16.
 */

import React from 'react';

/**
 * Faq Class
 */
class Faq extends React.Component {
    render() {
        return (
            <div className="ui middle aligned centered grid">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="ui middle aligned centered grid">
                            <div className="sixteen wide column title_section">
                                <div className="row">
                                    <div className="ui middle aligned centered grid">
                                        <h3 className="title"> Frequently Asked Questions</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid">
                            <div className="ten wide computer twelve wide tablet fourteen wide mobile column content">
                                <div className="row">
                                    <div className="ui grid question">
                                        1. What are ‘Trending Stocks’ and what is the logic behind it?
                                    </div>
                                    <div className="ui grid answer">
                                        Trending stocks are ‘hot’ stocks which help you get more trading ideas.
                                        We use a proprietary algorithm to arrive at parameters - “Confidence Meter”,
                                        “Social Media Pulse”, and “News and Sentiment”. Using these parameters, we track
                                        the change in social media chatter and Analyst opinions and find out what stocks
                                        are trending.
                                    </div>
                                    <div className="ui grid question">
                                        2. Are the Trending Stocks Real-time?
                                    </div>
                                    <div className="ui grid answer">
                                        Trending stocks are populated every 30 minutes. It is refreshed every time the
                                        app is opened. It can also be refreshed by the user by pulling down the trending
                                        stock screen.
                                    </div>
                                    <div className="ui grid question">
                                        3. What is a ‘Watchlist’? How many stocks can I add in it?
                                    </div>
                                    <div className="ui grid answer">
                                        It is a list of stocks that you want to keep a track of. There’s no limit! You
                                        can add as many stocks as you want on the Watchlist.
                                    </div>
                                    <div className="ui grid question">
                                        4. How many stocks do Stockal cover?
                                    </div>
                                    <div className="ui grid answer">
                                        We cover 6000+ stocks listed in NASDAQ, NYSE and AMEX.
                                    </div>
                                    <div className="ui grid question">
                                        5. Are the prices shown for stocks real-time?
                                    </div>
                                    <div className="ui grid answer">
                                        The prices and analyst data are refreshed every 3 hours.
                                    </div>
                                    <div className="ui grid question">
                                        6. What is a Confidence Meter?
                                    </div>
                                    <div className="ui grid answer">
                                        Confidence Meter is an aggregate of the analyst sentiments. It shows an overall
                                        percentage in favour of ‘buy’ or ‘sell’ by Morgan Stanley, Barclays, Oppenheimer
                                        and other top analyst firms. We aggregate information from over 200 trusted
                                        analysts and arrive at Confidence Meter using Stockal proprietary algorithm.
                                    </div>
                                    <div className="ui grid question">
                                        7. How do I read the Confidence Meter?
                                    </div>
                                    <div className="ui grid answer">
                                        It indicates the percentage of analysts who have a positive or ‘buy’ opinion.
                                        If the Confidence Meter says ‘50%’ it means that there are equal number of
                                        analysts who have buy and sell opinions.
                                        <br/>
                                        E.g. AAPL Confidence Meter is 81%, which means that 81% analysts have a bullish
                                        opinion on the stock.
                                    </div>
                                    <div className="ui grid question">
                                        8. What do you mean by Social Media Pulse? How do I read it for my analysis?
                                    </div>
                                    <div className="ui grid answer">
                                        Social Media Pulse is a parameter which determines whether the social chatter
                                        for the stock is more or less than the median value. If the social media pulse
                                        is red (lower than normal), it means that chatter for the stock is lower than
                                        the median value and vice versa.
                                        <br/>
                                        E.g. AAPL Social Media Pulse is 80% higher than normal, means that, on an
                                        average, there are 80% more number of people talking about AAPL today.
                                    </div>
                                    <div className="ui grid question">
                                        9. What is News and Sentiment?
                                    </div>
                                    <div className="ui grid answer">
                                        As the name suggests, News and Sentiment is a section where you can find the
                                        latest news for the stock. Based on these, we calculate the sentiment for the
                                        stock.
                                    </div>
                                    <div className="ui grid question">
                                        10. How do you predict revenue for a company?
                                    </div>
                                    <div className="ui grid answer">
                                        We source in the revenue predictions from our partner – Estimize, which has a
                                        dedicated bunch of analysts who study and predict the revenues for over 5000
                                        stocks. The data is refreshed once in 2 days.
                                    </div>
                                    <div className="ui grid question">
                                        11. Can I place a trade using Stockal?
                                    </div>
                                    <div className="ui grid answer">
                                        Yes! Our app is powered by Trade It and you can Buy and sell stocks though our
                                        app. Search for the stock that you are interested in, and click on the “Buy” or
                                        “Sell” button to place your order via Trade It.
                                    </div>
                                    <div className="ui grid question">
                                        12. What brokerages do you support?
                                    </div>
                                    <div className="ui grid answer">
                                        Trade It currently supports all major brokers including E*TRADE, Fidelity,
                                        Scottrade and TD Ameritrade. The list of brokers supported by TRADE IT is
                                        growing and is always available at trade.it.
                                    </div>
                                    <div className="ui grid question">
                                        13. How can I search for stocks?
                                    </div>
                                    <div className="ui grid answer">
                                        After logging in, at the bottom tab bar, click on watch list. Click on the top
                                        right search Icon to search for your favourite stocks.
                                    </div>
                                    <div className="ui grid question">
                                        14. How do I add stocks to watchlist?
                                    </div>
                                    <div className="ui grid answer">
                                        After viewing the stock, you will find a Star icon at the left hand bottom
                                        corner of the screen. You can add stocks to watchlist by clicking on the star
                                        shaped icon.
                                    </div>
                                    <div className="ui grid question">
                                        15. How do I find my friends on Stockal?
                                    </div>
                                    <div className="ui grid answer">
                                        Click on the chat button at the bottom tab bar, and click on add friends at the
                                        top right corner. Search for your friend’s email id/ username and click on that.
                                        An invitation will be sent to your friend. Once your friend accepts your
                                        invitation, you will be able to chat.
                                    </div>
                                    <div className="ui grid question">
                                        16. Can I share articles with my friends on Stockal?
                                    </div>
                                    <div className="ui grid answer">
                                        Yes, if you are reading an article and found it interesting, you can easily
                                        share it with your friends by clicking on the paper plane icon at the bottom
                                        tab bar.
                                    </div>
                                    <div className="ui grid question">
                                        17. Can I view the article in my browser?
                                    </div>
                                    <div className="ui grid answer">
                                        Yes, just click on the window icon at the bottom tab bar while viewing the
                                        article to open it in your browser.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Faq;
