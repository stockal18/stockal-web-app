/**
 * Created by Rajasekaran on 05/12/16.
 */

import React from 'react';
import IndiaIconOne from  '../../../public/assets/images/india_icon_one.png';
import IndiaIconTwo from  '../../../public/assets/images/india_icon_two.png';
import IndiaIconThree from  '../../../public/assets/images/india_icon_three.png';
import IndiaPictureOne from  '../../../public/assets/images/segment_circle1.png';
import IndiaPictureTwo from  '../../../public/assets/images/segment_circle2.png';
import IndiaPictureThree from  '../../../public/assets/images/segment_circle3.png';
import PartnerLogoOne from '../../../public/assets/images/logo_sic.png';
import PartnerLogoTwo from '../../../public/assets/images/logo_drivewealth.png';
import EnterpriseInput from '../enterprise/enterprise.input.view';
import APIClient from '../../api/api.client';

/**
 * India Class
 */
class India extends React.Component {
    constructor() {
        super();
        this.state = {
            sendButtonText: "Send",
            message: ""
        };
    }
    sendContactMail(){
        let validField = 0;
        for(var i = 1; i >= 0; i--){
            validField += this.refs["input"+i].validateField();
        }
        if(validField === 2){
            this.setState({
                sendButtonText: "Sending..."
            });
            let data = {
                name: this.refs.input0.refs.input.value,
                email: this.refs.input1.refs.input.value,
            };
            APIClient.sendContactMail(data).then((data) => {
                this.setState({
                    sendButtonText: "Sent",
                    message:  (data.status === 200) ? "Thanks for getting in touch! Expect an answer from us very soon."
                        : "Oops! Looks like the server did not respond as expected. Pls try again after some time."
                });
                setTimeout(() => {
                    this.setState({
                        sendButtonText: "Send",
                        message: ""
                    });
                    this.refs.input0.refs.input.value="";
                    this.refs.input1.refs.input.value="";
                }, 3000)
            });

        }
    }
    render() {
        return (
            <div className="ui grid">
                <div className="sixteen wide column">
                    <div className="row">

                        <div className="ui grid india_section">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="ui middle aligned centered grid">
                                        <div className="fourteen wide column">
                                            <div className="row">
                                                <div className="ui grid">
                                                <div className="seven wide computer ten wide tablet fourteen wide mobile column">
                                                <div className="us_stock_heading">
                                                    Invest in US Stocks.<br />
                                                    From India. With ease.
                                                </div>
                                                    </div>
                                                    </div>
                                                <div className="ui grid">
                                                <div className="fourteen wide computer fourteen wide tablet sixteen wide mobile column">
                                                <div className="us_stock_subheading">
                                                    Participate in the journey of companies, whose products you use.<br /><br />
                                                    Invest in the likes of Apple, Google, McDonalds, Exxon & over 2000 other firms listed on NASDAQ/NYSE
                                                </div>
                                                    </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="ui grid contact_section">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="ui middle aligned centered grid">
                                        <div className="fourteen wide column">
                                            <div className="row">
                                                <div className="contact_title">Interested? We'd love to talk
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui centered grid contact_section">
                            <div className="four wide computer five wide tablet fourteen wide mobile column">
                                <EnterpriseInput placeholder="Your Name" ref="input0" fieldType="text"/>
                            </div>
                            <div className="six wide computer six wide tablet fourteen wide mobile column">
                                <EnterpriseInput placeholder="Your Email Address" ref="input1" fieldType="email"/>
                            </div>
                            <div className="four wide computer three wide tablet thirteen wide mobile column">
                                <button type="submit" value="send" className={"sub_button " + (this.state.sendButtonText !="Send" ? "button-disabled" : "")} onClick={this.sendContactMail.bind(this)}> {this.state.sendButtonText}</button>
                            </div>
                            <div className={"slider-message " + (this.state.message ? "slider-message-active" : "")}>{this.state.message}</div>
                        </div>
                        <div className="ui centered grid">
                            <div className="four wide computer five wide tablet twelve wide mobile column">
                                <div className="india_icon_section">
                                    <img src={IndiaIconOne}/>
                                    <div className="icon_title">No Physical<br /> Documents</div>
                                    <div className="icon_desc">
                                        Easy signup online. All
                                        set & ready to invest in
                                        &lt; 3 days
                                    </div>
                                </div>
                            </div>
                            <div className="four wide computer five wide tablet twelve wide mobile column">
                                <div className="india_icon_section"><img src={IndiaIconTwo}/><div className="icon_title">Fractional Stocks, Low<br /> commissions</div>
                                    <div className="icon_desc">
                                        Invest based on your comfort.
                                        Construct a diversified portfolio
                                        for under Rs.2000.
                                    </div>
                                </div>
                            </div>
                            <div className="four wide computer five wide tablet twelve wide mobile column">
                                <div className="india_icon_section"><img src={IndiaIconThree}/><div className="icon_title">World-class Investing<br /> Intelligence</div>
                                    <div className="icon_desc">
                                        Stay on top of your money with
                                        forward-looking stock
                                        indicators
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid section section-margin bg-pink">
                            <div className="six wide computer seven wide tablet eight wide mobile column">
                                <div className="row">
                                    <div className="section_heading">Stockal signals keep you on your toes.
                                    </div>
                                    <br />
                                    <div className="section_content">
                                        We processes over 4 million data points every day to bring you insights & intellingence that could help you make smart investing choices.<br /><br />

                                        Know which stocks are trending, what experts feel about the stocks you're interested in, what other investors/peers are saying and how these companies are predicted to perform in the coming months.
                                    </div>
                                </div>
                            </div>
                            <div className="five wide computer six wide tablet seven wide mobile column">
                                <div className="row">
                                    <div className="section_picture"><img src={IndiaPictureOne}/></div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid section section-margin bg-pink">
                            <div className="five wide computer six wide tablet seven wide mobile column">
                                <div className="row">
                                    <div className="section_picture"><img src={IndiaPictureTwo}/></div>
                                </div>
                            </div>
                            <div className="six wide computer seven wide tablet eight wide mobile column">
                                <div className="row">
                                    <div className="section_heading">Collaborate, to make more informed investments</div>
                                    <br />
                                    <div className="section_content">Prefer to check with friends or knowledgeable acquaintenaces before making investments? Quick check on Whatsapp to help? We get it.<br /><br />
                                        So we built, Stockal Chat, A contextual messaging product for stock investors, No need to back-and-forth between Whatsapp and Yahoo Finance to see how your stocks are doing. Get signals & place buy/sell orders in chat itself!
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid section section-margin bg-pink">
                            <div className="six wide computer seven wide tablet eight wide mobile column">
                                <div className="row">
                                    <div className="section_heading">Why invest in one company when you can invest in many. With the same money.</div>
                                    <br />
                                    <div className="section_content">
                                        Did we say there are no account minimums? So yes, there's no limit to how small you want to start, US stocks can be expensive, what with INR-USD conversion rates etc.<br /><br />
                                        For eg: If all you want to put is Rs.5000, you can still buy a little bit of Google, a fraction of Netflix and some of that Amazon stock you've been eyeing. You can always invest more as you gain confidence!
                                    </div>
                                </div>
                            </div>
                            <div className="five wide computer six wide tablet seven wide mobile column">
                                <div className="row">
                                    <div className="section_picture"><img src={IndiaPictureThree}/></div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="ui middle aligned centered grid">
                                        <div className="sixteen wide column">
                                            <div className="row">
                                                <div className="partner_title">
                                                    Our Brokerage Partners
                                                </div>
                                                <div className="partner_logo_container">
                                                    <div className="partner_logo">
                                                        <img src={PartnerLogoOne}/>
                                                        <img src={PartnerLogoTwo}/>
                                                            </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}
export default India;