/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from 'react';
import FilterValues from './filter.values.view'

/**
 * FilterCategory Class Definition
 */
class FilterCategory extends Component {

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        let content;
        if (this.props.data) {
            content = this.props.data.map(function (item, i) {
                return (
                    <li className="category" key={i}>
                        <div className="category-header">{item.category}</div>
                        <FilterValues data={item.values} categoryIndex={i}
                                      selectedCategoryIndex={this.props.selectedCategoryIndex}
                                      query={this.props.query} keyName={item.keyName}
                                      onUpdateSelection={this.props.onUpdateSelection} />

                    </li>
                )
            }.bind(this))
        } else {
            content = (
                <div>...</div>
            )
        }
        return (
            <ul className="autosuggest-list-container">
                {content}
            </ul>
        );
    }
}

export default FilterCategory;