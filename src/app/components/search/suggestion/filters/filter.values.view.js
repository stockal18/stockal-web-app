/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from 'react';
import AutosuggestHighlight from 'autosuggest-highlight';
import {Link} from 'react-router';


/**
 * FilterValues Class Definition
 */
class FilterValues extends Component {

    /**
     * PropTypes
     * @type {{Count: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            selectedCategoryIndex: this.props.selectedCategoryIndex,
            selectedFilter: -1
        }
    }

    onMouseEnterHandler(categoryIndex, filterIndex) {
        this.setState({
            selectedCategoryIndex: categoryIndex,
            selectedFilterIndex: filterIndex
        })
        this.props.onUpdateSelection(categoryIndex, filterIndex);
    }

    renderSuggestion(suggestion, query) {
        const suggestionText = `${suggestion}`;
        const matches = AutosuggestHighlight.match(suggestionText, query);
        const parts = AutosuggestHighlight.parse(suggestionText, matches);
        return (
            <span className="name">
            {
                parts.map((part, index) => {
                    const className = part.highlight ? 'highlight' : null;
                    return (
                        <span className={className} key={index}>{part.text}</span>
                    );
                })
            }
            </span>
        );
    }

    render() {
        let content;
        if (this.props.data) {
            content = this.props.data.map(function (item, i) {
                return (
                    <Link to={"/search/filter/" + this.props.keyName + "/" + item.value} key={i}>
                        <li className={"item" + (this.state.selectedCategoryIndex == this.props.selectedCategoryIndex
                        && this.state.selectedFilterIndex == i ? " active" : "")}
                            onMouseEnter={this.onMouseEnterHandler.bind(this, this.props.categoryIndex, i)}>
                            {this.renderSuggestion(item.value, this.props.query)}
                        </li>
                    </Link>
                )
            }.bind(this))
        } else {
            content = (
                <div>...</div>
            )
        }
        return (
            <ul className="category-items">
                {content}
            </ul>
        );
    }
}

export default FilterValues;