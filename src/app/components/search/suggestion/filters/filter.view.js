/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from 'react';
import FilterCategory from './filter.category.view'

/**
 * Filter Class Definition
 */
class Filter extends Component {

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
    }

    /**
     * componentDidMount
     */
    render() {
        return (
            <div className="six wide computer five wide tablet sixteen wide mobile column">
                <div className="search-results">
                    <FilterCategory data={this.props.filter}
                                    query={this.props.query}
                                    onUpdateSelection={this.props.onUpdateSelection}
                                    selectedCategoryIndex={this.props.selectedCategoryIndex}/>
                </div>
            </div>
        );
    }
}

export default Filter;