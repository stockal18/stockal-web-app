/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from "react";
import Autosuggest from "react-autosuggest";
var timeout = null;
/* Search className */
class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            suggestionData: this.props.suggestionData,
            filterData: this.props.filterData,
            suggestions: this.getSuggestions('', this.props.suggestionData)
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            suggestionData: nextProps.suggestionData,
            filterData: nextProps.filterData
        })
    }

    onChange = (event, {newValue, method}) => {
        this.setState({
            value: newValue
        });
    };

    onSuggestionsUpdateRequested = ({value}) => {
        clearTimeout(timeout);
        var current = this;
        timeout = setTimeout(function () {
            current.props.onUpdateSuggestion(current.getSuggestions(value, current.props.suggestionData), value);
            current.setState({
                suggestions: current.getSuggestions(value, current.props.suggestionData)
            });
        }, value == "" ? 1 : 500);
    };

    onSuggestionsFetchRequested = ({value}) => {
        clearTimeout(timeout);
        var current = this;
        timeout = setTimeout(function () {
            current.props.onUpdateSuggestion(current.getSuggestions(value, current.props.suggestionData), value);
            current.setState({
                suggestions: current.getSuggestions(value, current.props.suggestionData)
            });
        }, value == "" ? 1 : 500);
    };

    onSuggestionsClearRequested = () => {
        clearTimeout(timeout);
        var current = this;
        timeout = setTimeout(function () {
            current.props.onUpdateSuggestion(current.getSuggestions(value, current.props.suggestionData), value);
            current.setState({
                suggestions: current.getSuggestions(value, current.props.suggestionData)
            });
        }, value == "" ? 1 : 500);
    };


    escapeRegexCharacters(str) {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }

    getSuggestions(value, data) {
        const escapedValue = this.escapeRegexCharacters(value.trim());
        const regex = new RegExp('\\b' + escapedValue, 'i');
        let counter = 0;
        if (value === "") {
            return this.props.suggestionData;
        } else {
            return data.map(section => {
                return {
                    category: section.category,
                    values: section.values.filter(value => regex.test(`${value.value}`)),
                    displayType: section.displayType,
                    keyName: section.keyName
                };
            })
                .filter(section => section.values.length > 0);
        }

    }

    getSuggestionValue(suggestion) {
        return suggestion.text;
    }

    renderSuggestion(suggestion) {
        return null;
    }

    renderSectionTitle(section) {
        return null;
    }

    getSectionSuggestions(section) {
        return section.values;
    }

    render() {
        const {value, suggestions} = this.state;
        const inputProps = {
            placeholder: "Enter stock, sector or theme",
            value,
            onChange: this.onChange
        };

        return (
            <div className="search-container">
                <div className="ui grid">
                    <div className="twelve wide computer twelve wide tablet sixteen wide mobile column">
                        <Autosuggest
                            multiSection={true}
                            suggestions={suggestions}
                            onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested}
                            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                            getSuggestionValue={this.getSuggestionValue}
                            renderSuggestion={this.renderSuggestion}
                            renderSectionTitle={this.renderSectionTitle}
                            getSectionSuggestions={this.getSectionSuggestions}
                            focusFirstSuggestion={true}
                            alwaysRenderSuggestions={true}
                            inputProps={inputProps}/>
                    </div>
                    <div className="four wide computer four wide tablet only column">
                        <input type="button" value="SEARCH"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchBox;