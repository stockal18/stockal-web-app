/**
 * Created by prayag on 29/11/16.
 */

import React, {Component} from 'react';
import SearchBox from './searchbox/searchbox.view'
import Filter from './filters/filter.view'
import Results from './results/result.view'
import FiltersStore from '../../../fixtures/filters/filters.store'
import FiltersAction from '../../../fixtures/filters/filters.actions'
import TrendingStocksStore from './../../../fixtures/trendingstocks/trendingstocks.store';
import TrendingStocksAction from './../../../fixtures/trendingstocks/trendingstocks.actions';
import connectToStores from 'alt-utils/lib/connectToStores';
import APIClient from './../../../api/api.client';

var data = require('./../../../../public/assets/others/newSymbolsListLatest.csv');
var stockList = [];
for (var i = 1; i < data.length; i++) {
    var obj = {};
    obj = {value: data[i][0] + ", " + data[i][1], symbol: data[i][0]}
    stockList.push(obj);
}

/**
 * Suggestion Class Definition
 */
class Suggestion extends Component {

    static getStores() {
        return [FiltersStore, TrendingStocksStore];
    }

    static getPropsFromStores() {
        return {
            ...FiltersStore.getState(),
            ...TrendingStocksStore.getState()
        }
    }

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */

    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            filterData: [],
            suggestionData: [],
            renderedData: [],
            query: '',
            selectedCategoryIndex: -1,
            selectedFilterIndex: -1,
            searching: false,
            resultData: [],
            trendingStocks : []
        }
    }

    onUpdateSuggestion(suggestionData, query, shouldRenderSuggestions) {
        /*if (suggestionData == []) {
         suggestionData = this.state.suggestionData;
         }*/
        this.setState({
            searching: query !== ''
        });
        this.setState({
            renderedData: query !== '' ? suggestionData : this.state.filterData,
            query: query,
            selectedCategoryIndex: -1,
            selectedFilterIndex: -1,
            resultData: this.state.trendingStocks
        });
    }

    onUpdateSelection(categoryIndex, filterIndex) {
        this.setState({
            selectedCategoryIndex: categoryIndex,
            selectedFilterIndex: filterIndex,
            resultData: []
        });
        let filter = {};
        if (this.state.renderedData[categoryIndex].keyName === "stock") {
            filter["stock"] = [this.state.renderedData[categoryIndex].values[filterIndex].symbol];
        } else {
            filter[this.state.renderedData[categoryIndex].keyName] = [this.state.renderedData[categoryIndex].values[filterIndex].value];
        }
        APIClient.getSearchResults(JSON.stringify(filter)).then((response) => {
            if (response.status === 200) {
                this.setState({
                    resultData: response.data
                })
            }
        });
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.trendingStocksLoaded){
            console.log(nextProps.trendingStocks);
            this.setState({
                trendingStocks: nextProps.trendingStocks,
                resultData: nextProps.trendingStocks
            })
        }
        let filterData = [], suggestionData = [];
        if (nextProps.filterLoaded) {
            let stocks = {
                category: "Stocks",
                values: stockList,
                displayType: "single",
                keyName: "stock"
            }
            suggestionData.push(stocks);

            let sectors = {};
            sectors.category = "Popular Sectors";
            sectors.displayType = "grid";
            sectors.keyName = "sectors";

            let sectorValues = nextProps.filters.sectors.filter(function (item, i) {
                return item.active
            });
            sectors.values = sectorValues;
            suggestionData.push(JSON.parse(JSON.stringify(sectors)));
            sectorValues = sectorValues.filter(function (item, i) {
                return item.popular
            });
            sectors.values = sectorValues;
            filterData.push(sectors);

            let themes = {};
            themes.category = "Popular Themes";
            themes.displayType = "grid";
            themes.keyName = "themes";

            let themeValues = nextProps.filters.themes.filter(function (item, i) {
                return item.active
            })
            themes.values = themeValues;
            suggestionData.push(JSON.parse(JSON.stringify(themes)));

            themeValues = themeValues.filter(function (item, i) {
                return item.popular
            })
            themes.values = themeValues;
            filterData.push(themes);

            let selectionTags = {};
            selectionTags.category = "Trending Selections";
            selectionTags.displayType = "row";
            let selectionTagValues = nextProps.filters.selectionTags.filter(function (item, i) {
                return item.active
            })
            selectionTags.values = selectionTagValues;
            //suggestionData.push(selectionTags);
            //filterData.push(selectionTags);

            this.setState({
                suggestionData: suggestionData,
                renderedData: filterData,
                filterData: filterData
            })
        }

    }

    componentDidMount() {
        FiltersAction.fetch();
        TrendingStocksAction.fetch();
    }

    componentWillUnmount() {
        FiltersAction.clear();
        TrendingStocksAction.clear();
    }

    render() {
        return (
            <div className="fourteen wide large screen fifteen wide tablet fifteen wide mobile column">
                <div className="row">
                    <div className="search-heading">
                        ADVANCED DISCOVERY
                    </div>
                </div>

                <div className="row">
                    <SearchBox suggestionData={this.state.suggestionData} filterData={this.state.filterData}
                               onUpdateSuggestion={this.onUpdateSuggestion.bind(this)}/>
                    <div className="search-data-container">
                        <div className="ui grid">
                            <Filter filter={this.state.searching ? this.state.renderedData : this.state.filterData}
                                    query={this.state.query}
                                    selectedCategoryIndex={this.state.selectedCategoryIndex}
                                    onUpdateSelection={this.onUpdateSelection.bind(this)}/>
                            <Results filter={this.state.searching ? this.state.renderedData : this.state.filterData}
                                     selectedCategoryIndex={this.state.selectedCategoryIndex}
                                     selectedFilterIndex={this.state.selectedFilterIndex}
                                     resultData={this.state.resultData}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connectToStores(Suggestion);