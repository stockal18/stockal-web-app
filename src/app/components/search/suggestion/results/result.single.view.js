/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from 'react';
import {Link} from 'react-router';

/**
 * InitialSearchResultRow Class Definition
 */
class InitialSearchResultSingle extends Component {

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        return (
            <div className="stock-row-container">
                <Link to="/stock-details/AAPL">
                    <div className="each-stock-container">
                        <div className="stock-name-container">
                            <div className="stock-code">
                                <span>REGN</span>
                            </div>
                            <div className="stock-name">
                                Regeneron PharmaceuticalsInc
                            </div>
                        </div>
                        <div className="stock-confidence-meter-container">
                            <div className="stock-confidence-meter">
                                75%
                            </div>
                            <div className="stock-confidence-meter-text">
                                Confidence Meter
                            </div>
                        </div>
                        <div className="stock-sentiment-container">
                            <div className="stock-sentiment green">
                                <i className="caret up icon"></i>
                            </div>
                            <div className="stock-sentiment-text">
                                Sentiment
                            </div>
                        </div>
                        <div className="stock-price-container">
                            <div className="stock-price">
                                $123.45
                            </div>
                            <div className="stock-price-text">
                                Price
                            </div>
                        </div>
                        <div className="stock-favourite-icon-container">
                            <div className="stock-favourite-icon">
                                <i className="empty star icon"></i>
                            </div>
                        </div>
                    </div>
                </Link>

            </div>
        );
    }
}

export default InitialSearchResultSingle;