/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from "react";
import ResultGrid from "./result.grid.view";
import ResultRow from "./result.row.view";
import {Link} from "react-router";

/**
 * Result Class Definition
 */
class Result extends Component {

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */

    render() {
        let result = this.props.selectedCategoryIndex >= 0 &&
        (this.props.filter[this.props.selectedCategoryIndex].displayType == "row"
        || this.props.filter[this.props.selectedCategoryIndex].displayType == "single") ? (
            <ResultRow resultData={this.props.resultData}/>
        ) : (
            <ResultGrid resultData={this.props.resultData}/>
        )

        let resultTitle = this.props.selectedCategoryIndex >= 0 && this.props.selectedFilterIndex >= 0 ?
            this.props.filter[this.props.selectedCategoryIndex]
                .values[this.props.selectedFilterIndex].value : "Trending Stocks";

        let seeAllButton = "";
        if (this.props.selectedCategoryIndex >= 0
            && this.props.filter[this.props.selectedCategoryIndex].displayType != "single" && this.props.resultData.length > 0) {
            seeAllButton = (
                <Link to="/search/filter">
                    <div className="see-all-button">
                        <input type="button" value="SEE ALL STOCKS"/>
                    </div>
                </Link>
            )
        } else if (this.props.selectedCategoryIndex < 0 && this.props.resultData.length > 0) {
            seeAllButton = (
                <Link to="/search/filter">
                    <div className="see-all-button">
                        <input type="button" value="SEE ALL STOCKS"/>
                    </div>
                </Link>
            )
        }


        return (
            <div className="ten wide computer eleven wide tablet only column">
                <div className="row">
                    <div className="stock-container">
                        <div className="ui grid">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="stock-heading">
                                        {resultTitle}
                                    </div>
                                </div>
                                <div className="row">
                                    {result}
                                </div>
                                <div className="row">
                                    {seeAllButton}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Result;