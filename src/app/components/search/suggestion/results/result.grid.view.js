/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from 'react';
import {Link} from 'react-router';

/**
 * InitialSearchResultGrid Class Definition
 */
class InitialSearchResultGrid extends Component {

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        let result;
        if (this.props.resultData && this.props.resultData.length > 0) {
            result = (this.props.resultData.map(function (item, i) {
                    return (
                        <div className="four wide column" key={i}>
                            <Link to={"/stock-details/" + item.code}>
                                <div className="each-stock-container">
                                    <div className="stock-confidence-meter">
                                        {item.confidencemeter ? item.confidencemeter : item.benzingaBuyPercent}%
                                    </div>
                                    <div className="stock-code">
                                        <span>{item.code}</span>
                                    </div>
                                    <div className="stock-name">
                                        {item.company}
                                    </div>
                                    <div className="stock-price">
                                        ${item.price ? item.price : item.lastTradePrice}
                                    </div>
                                </div>
                            </Link>
                        </div>
                    )
                })
            );
        }
        else {
            result = (
                    <div className="sixteen wide column">
                        <div className="ui result_grid_loader">
                            <div className="ui active inverted dimmer">
                                <div className="ui centered text loader">Loading</div>
                            </div>
                        </div>
                    </div>);
        }
        //     result = (
        //         <div className="sixteen wide column">
        //             <div className="nodata_section">
        //                 <div className="nodata_title"> Sorry, we don’t have this data yet</div>
        //                 <div className="nodata_desc">We show you filter only for those stocks where we
        //                     are confident about the data. We’ll get you relevant data for this stock soon.
        //                 </div>
        //             </div>
        //         </div>);
        // }
        return (
            <div className="stock-grid-container">
                <div className="ui grid">
                    {result}
                </div>
            </div>
        );
    }
}

export default InitialSearchResultGrid;

