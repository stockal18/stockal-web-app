/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from "react";
import MainFilterCategory from "./filters/filters.view";
import ResultRow from "./results/result.row.view";
import {Link} from "react-router";
import APIClient from "./../../../api/api.client";

/**
 * MainFilter Class Definition
 */
class MainFilter extends Component {

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            showFilter: false,
            filterButtonText: "+ Show Filters",
            selectedRatio: {text: "P/E Ratio"},
            selectedTab: 0,
            resultData: [],
            sort: {by: 'pe1', order: 1},
            doSort: false,
            allFilters: {}
        }
    }


    setSelectedRatio(item) {
        this.setState({
            selectedRatio: item
        })
    }

    selectTab(index) {
        this.setState({
            selectedTab: index
        })
    }

    sortBy(sort) {
        let filter = this.state.allFilters;
        filter.sort = sort;
        this.setState({
            sort: sort
        })
        this.searchData(filter);
    }

    toggleFilter() {
        this.setState({
            showFilter: !this.state.showFilter,
            filterButtonText: !this.state.showFilter ? "- Hide Filters" : "+ Show Filters"
        })
    }

    searchData(filter) {
        APIClient.getFullSearchResults(filter).then((response) => {
            if (response.status === 200) {
                console.log(response.data);
                this.setState({
                    resultData: response.data
                })
            }
        });
    }

    setFilters(allFilters) {
        this.setState({
            allFilters: allFilters
        })
    }

    render() {
        let current = this;
        let mainFilterCategory = (
            <MainFilterCategory showFilter={this.state.showFilter} toggleFilter={this.toggleFilter.bind(this)}
                                setFilters={this.setFilters.bind(this)} params={this.props.params}
                                searchData={this.searchData.bind(this)} sort={this.state.sort}/>
        )
        return (
            <div className="fourteen wide large screen fifteen wide tablet fifteen wide mobile column">
                <div className="row">
                    <div className="ui grid">
                        <div className="twelve wide computer nine wide tablet sixteen wide mobile column">
                            <div className="search-heading_advanced">
                                ADVANCED DISCOVERY
                            </div>
                        </div>
                        <div className="two wide computer three wide tablet seven wide mobile column">
                            <Link to="/search">
                                <div className="back_btn_search">
                                    <i className="search icon"></i>Search
                                </div>
                            </Link>
                        </div>
                        <div className="two wide computer three wide tablet seven wide mobile column">
                            <div className="show-filter-text" onClick={this.toggleFilter.bind(this)}>
                                {this.state.filterButtonText}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    {mainFilterCategory}
                    <div className="ui grid search-result-container">
                        <div className="six wide column">
                            <div className="search-heading">SEARCH RESULTS</div>
                        </div>
                        <div className="ten wide column">
                            <div className="ui four item stackable tabs menu tabsmenu">
                                <a className={"item" + (this.state.selectedTab == 0 ? " active" : "")}
                                   onClick={this.selectTab.bind(this, 0)} data-tab="definition">Default</a>
                                <a className={"item" + (this.state.selectedTab == 1 ? " active" : "")}
                                   onClick={this.selectTab.bind(this, 1)} data-tab="examples">Market Ratios</a>
                                <a className={"item" + (this.state.selectedTab == 2 ? " active" : "")}
                                   onClick={this.selectTab.bind(this, 2)} data-tab="usage">Profitability Ratios</a>
                                <a className={"item" + (this.state.selectedTab == 3 ? " active" : "")}
                                   onClick={this.selectTab.bind(this, 3)} data-tab="settings">Liquidity Ratios</a>
                            </div>
                        </div>
                    </div>
                    <ResultRow selectedTab={this.state.selectedTab} resultData={this.state.resultData}
                               sortBy={this.sortBy.bind(this)}/>
                </div>
            </div>
        );
    }
}

export default MainFilter;