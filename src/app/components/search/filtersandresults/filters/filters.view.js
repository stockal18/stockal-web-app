/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from "react";
import Slider from "rc-slider";
import MainFilterValues from "./mainfilter.values.view";
import AllFiltersStore from "./../../../../fixtures/allfilters/allfilters.store";
import AllFiltersAction from "./../../../../fixtures/allfilters/allfilters.actions";
import connectToStores from "alt-utils/lib/connectToStores";
require('rc-slider/assets/index.css');

/**
 * Filters Class Definition
 */

class Filters extends Component {

    static getStores() {
        return [AllFiltersStore];
    }

    static getPropsFromStores() {
        return AllFiltersStore.getState();
    }

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            initialAllFilters: {},
            allFilters: {},
            sectors: [],
            themes: [],
            selectionTags: [],
            marketCapTypes: [],
            selectedMarketCapTypeIndex: 0,
            signals: [],
            ratios: [],
            selectedRatio: -1,
            selectedSectors: [],
            selectedThemes: [],
            ratioSlider: {},
            selectedRatioRange: [],
            selectedConfidenceMeterRange: {min: 0, max: 100},
            sort: {by: this.props.sortBy, order: 1}
        }

    }

    componentDidMount() {
        let filter = {}
        if (this.props.params.category && this.props.params.value) {
            filter[this.props.params.category] = [this.props.params.value];
        }
        if (this.props.allFiltersLoaded) {
            let allFilters = this.props.allFilters[this.props.params.category].map(function (item, i) {
                if (item.value === this.props.params.value) {
                    item.selected = true;
                }
            });
            this.setState({
                allFilters: allFilters
            })
        }
        this.props.searchData(filter);
        AllFiltersAction.fetch();
    }

    componentWillUnmount() {
        AllFiltersAction.clear();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.allFiltersLoaded) {
            this.setState({
                initialAllFilters: JSON.parse(JSON.stringify(nextProps.allFilters)),
                allFilters: JSON.parse(JSON.stringify(nextProps.allFilters)),
                sectors: nextProps.allFilters.sectors,
                themes: nextProps.allFilters.themes,
                selectionTags: nextProps.allFilters.selectionTags,
                marketCapTypes: nextProps.allFilters.marketCapTypes,
                signals: nextProps.allFilters.signals,
                ratios: nextProps.allFilters.ratios,
                sort: {
                    by: nextProps.sortBy,
                    order: this.props.sortBy === nextProps.sortBy ? this.state.sort.order * -1 : -1
                }

            });
        }
    }

    resetFilters() {
        let sectors = this.state.sectors.map(function (item, i) {
            item.selected = false;
            return item;
        });

        let themes = this.state.themes.map(function (item, i) {
            item.selected = false;
            return item;
        });

        this.setState({
            sectors: sectors,
            themes: themes,
            selectedMarketCapTypeIndex: 0
        })
    }

    getMarks() {
        let marks = {};
        this.props.filterCategoryData.values.map(function (item, i) {
            marks[i] = item.text;
        });
        return marks;
    }

    capTypeTipFormatter(v) {
        return this.props.allFilters.marketCapTypes[v].name;
    }

    toggleSectorSelection(i) {
        let sectors = this.state.sectors;
        sectors[i].selected = !sectors[i].selected
        this.setState({
            sectors: sectors
        })
    }

    toggleThemesSelection(i) {
        let themes = this.state.themes;
        themes[i].selected = !themes[i].selected
        this.setState({
            themes: themes
        })
    }

    toggleSelectionTagsSelection(i) {
        let selectionTags = this.state.selectionTags;
        selectionTags[i].selected = !selectionTags[i].selected
        let allFilters = JSON.parse(JSON.stringify(this.state.allFilters));
        this.resetFilters();
    }

    ratioSelection(i) {
        //this.props.setSelectedRatio(item);
        if (this.state.selectedRatio >= 0) {
            this.setState({
                selectedRatio: -1,
                selectedRatioRange: {
                    min: this.props.allFilters.ratios[i].range.min,
                    max: this.props.allFilters.ratios[i].range.max
                }
            })
        }
        this.setState({
            selectedRatio: i == this.state.selectedRatio ? -1 : i
        });
    }

    onCapTypeChange(i) {
        let marks = {}
        this.setState({
            selectedMarketCapTypeIndex: i
        })
    }

    onRatioChange(i) {
        this.setState({
            selectedRatioRange: {min: i[0], max: i[1]}
        })
    }

    onConfidenceMeterChange(i) {
        this.setState({
            selectedConfidenceMeterRange: {min: i[0], max: i[1]}
        })
    }

    prepareFilter() {
        //Prepare filters here
        let filter = {};

        let selectedSectors = this.state.sectors.filter(function (item, i) {
            return item.selected
        }).map(function (item, i) {
            return item.value
        });

        let selectedThemes = this.state.themes.filter(function (item, i) {
            return item.selected
        }).map(function (item, i) {
            return item.value
        });

        let selectedMarketCapTypes = [this.state.marketCapTypes[this.state.selectedMarketCapTypeIndex].name]

        if (selectedMarketCapTypes[0] === "Any") {
            selectedMarketCapTypes = []
        } else if (selectedMarketCapTypes[0] === "Nano/Micro") {
            selectedMarketCapTypes = ["Nano", "Micro"]
        }
        if (this.state.selectedRatio >= 0) {
            let ratio = {}
            ratio.name = this.props.allFilters.ratios[this.state.selectedRatio].key;
            let range = {};
            range = this.state.selectedRatioRange;
            ratio.range = range;
            filter.ratio = ratio;
        }
        filter.sectors = selectedSectors;
        filter.themes = selectedThemes;
        filter.marketCapTypes = selectedMarketCapTypes;
        filter.sort = this.state.sort;
        filter.confidencemeter = this.state.selectedConfidenceMeterRange;
        console.log(filter);
        this.props.setFilters(filter);
        this.props.toggleFilter();
    }

    searchData() {
        this.prepareFilter();
        this.props.searchData();
    }

    render() {
        let marks = {}, selectedRatio, ratioRange = {}, ratioSlider = "";
        if (this.props.allFiltersLoaded) {
            this.props.allFilters.marketCapTypes.map(function (item, i) {
                marks[i] = item.name;
            });
            if (this.state.selectedRatio >= 0) {
                selectedRatio = this.props.allFilters.ratios[this.state.selectedRatio];
                ratioRange[parseInt(selectedRatio.range.min)] = parseInt(selectedRatio.range.min);
                ratioRange[parseInt(selectedRatio.range.max)] = parseInt(selectedRatio.range.max);
                let ratioValue = [parseInt(selectedRatio.range.min), parseInt(selectedRatio.range.max)]
                ratioSlider = (
                    <ul className="category-items slider">
                        <Slider tipTransitionName="rc-slider-tooltip-zoom-down"
                                range={true}
                                included={true}
                                marks={ratioRange}
                                min={parseInt(selectedRatio.range.min)}
                                max={parseInt(selectedRatio.range.max)}
                                defaultValue={ratioValue}
                                onAfterChange={this.onRatioChange.bind(this)}/>
                    </ul>
                )
            }
        }


        let current = this;
        return this.props.allFiltersLoaded ? (
            <div className={"search-filter-container " + (this.props.showFilter ? "shown" : "hidden")}>
                <ul className="ui grid search-filter-list">
                    <li className="eight wide computer eight wide tablet sixteen wide mobile column category bg-green">
                        <div className="category-header">Popular Sectors</div>
                        <ul className="category-items default">
                            {
                                this.state.sectors.map(function (item, i) {
                                    return (
                                        <MainFilterValues filterValuesData={item} category="Popular Categories" key={i}
                                                          onItemClick={current.toggleSectorSelection.bind(current, i)}/>
                                    )
                                })
                            }
                        </ul>
                    </li>
                    <li className="eight wide computer eight wide tablet sixteen wide mobile column category bg-purple">
                        <div className="category-header">Popular Themes</div>
                        <ul className="category-items default">
                            {
                                this.state.themes.map(function (item, i) {
                                    return (
                                        <MainFilterValues filterValuesData={item} category="Popular Themes" key={i}
                                                          onItemClick={current.toggleThemesSelection.bind(current, i)}/>
                                    )
                                })
                            }
                        </ul>
                    </li>
                    <li className="eight wide computer eight wide tablet sixteen wide mobile column category bg-green">
                        <div className="category-header">Market Cap Types</div>
                        <ul className="category-items slider">
                            <Slider tipTransitionName="rc-slider-tooltip-zoom-down"
                                    dots={true}
                                    range={false}
                                    included={false}
                                    marks={marks}
                                    step={1}
                                    min={0}
                                    max={5}
                                    tipFormatter={this.capTypeTipFormatter.bind(this)}
                                    onAfterChange={this.onCapTypeChange.bind(this)}/>
                        </ul>
                    </li>
                    <li className="eight wide computer eight wide tablet sixteen wide mobile column category bg-purple">
                        <div className="category-header">Trending Selections</div>
                        <ul className="category-items default">
                            {
                                this.state.selectionTags.map(function (item, i) {
                                    return (
                                        <MainFilterValues filterValuesData={item} category="Trending Selections" key={i}
                                                          onItemClick={current.toggleSelectionTagsSelection.bind(current, i)}/>
                                    )
                                })
                            }
                        </ul>
                    </li>
                    <li className="eight wide computer eight wide tablet sixteen wide mobile column category bg-blue">
                        <div className="category-header">Ratios</div>
                        <ul className="category-items default ratio">
                            {
                                this.state.ratios.map(function (item, i) {
                                    return (
                                        <MainFilterValues filterValuesData={item} category="Ratios" key={i}
                                                          nameKey="name" selected={current.state.selectedRatio == i}
                                                          onItemClick={current.ratioSelection.bind(current, i)}/>
                                    )
                                })
                            }
                        </ul>
                        {ratioSlider}
                        <div className="ratio-text">
                            {this.state.selectedRatio >= 0 ? this.state.ratios[this.state.selectedRatio].name : ""}
                        </div>
                    </li>
                    <li className="eight wide computer eight wide tablet sixteen wide mobile column category bg-purple">
                        <div className="category-header">Confidence Meter</div>
                        <ul className="category-items slider">
                            <Slider tipTransitionName="rc-slider-tooltip-zoom-down"
                                    range={true}
                                    included={true}
                                    marks={{0: 0, 100: 100}}
                                    step={1}
                                    min={0}
                                    max={100}
                                    defaultValue={[0, 100]}
                                    onAfterChange={this.onConfidenceMeterChange.bind(this)}/>
                        </ul>
                        <div className="category-header">Social Media Pulse</div>
                        <ul className="category-items slider">
                            <Slider tipTransitionName="rc-slider-tooltip-zoom-down"
                                    range={true}
                                    included={true}
                                    marks={{0: 0, 100: 100}}
                                    step={1}
                                    min={0}
                                    max={100}
                                    defaultValue={[0, 100]}/>
                        </ul>
                    </li>
                </ul>
                <div className="ui centered grid">
                    <div className="six wide computer eight wide tablet sixteen wide mobile column">
                        <div className="search-button-container" onClick={this.searchData.bind(this)}>
                            <input type="button" value="SEARCH AGAIN"/>
                        </div>
                    </div>
                </div>
            </div>
        ) : (<div></div>);
    }
}

export default connectToStores(Filters);