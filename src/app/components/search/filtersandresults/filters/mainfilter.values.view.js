/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from 'react';

/**
 * MainFilterValues Class Definition
 */
class MainFilterValues extends Component {

    /**
     * PropTypes
     * @type {{Count: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            selectedIndex: -1
        }
    }

    componentDidMount() {
        this.setState({
            selectedIndex: 0,
            active: false
        })
    }

    toggleActive() {
    }

    handleClick() {
        this.props.onItemClick();

    }

    componentWillReceiveProps(nextProps) {
        this.setState({})
    }

    componentWillMount() {

    }

    render() {
        return (
            <li className={"item" + (this.props.filterValuesData.selected || this.props.selected ? " active" : "")}
                onClick={this.handleClick.bind(this)} key={this.props.index}>
                <span>{this.props.nameKey ? this.props.filterValuesData[this.props.nameKey] : this.props.filterValuesData.value}</span>
            </li>
        );
    }
}

export default MainFilterValues;