/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from "react";
import {Link} from "react-router";

/**
 * InitialSearchResultRow Class Definition
 */

class InitialSearchResultRow extends Component {

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Constructor
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            resultData: this.props.resultData,
            sort: {by: 'pe1', order: 1}
        }
    }

    componentDidMount() {

    }

    sortBy(sortBy) {
        let sort = {by: sortBy, order: this.state.sort.by === sortBy ? this.state.sort.order * -1 : -1}
        this.setState({
            sort: sort
        })
        this.props.sortBy(sort);
    }

    render() {
        let resultHeads =
            [
                [
                    {
                        name: "Confidence Meter",
                        key: "confidencemeter"
                    }, {
                    name: "Sentiment",
                    key: ""
                }, {
                    name: "Price",
                    key: "price"
                }, {
                    name: "Target Price",
                    key: "targetprice"
                }
                ], [
                {
                    name: "P/E",
                    key: "pe1"
                }, {
                    name: "P/BV",
                    key: "pb"
                }, {
                    name: "Price/Sales",
                    key: "ps1"
                }, {
                    name: "EV/EBITDA",
                    key: "evebitda"
                }
            ], [
                {
                    name: "RoE",
                    key: "roe"
                }, {
                    name: "Profit Margin",
                    key: "netmargin"
                }, {
                    name: "Dividend Yield",
                    key: "divyield"
                }, {
                    name: "Dividend Payout",
                    key: "payoutratio"
                }
            ], [
                {
                    name: "FCF/Share",
                    key: "fcfps"
                }, {
                    name: "Current Ratio",
                    key: "currentratio"
                }, {
                    name: "Debt/Equity",
                    key: "de"
                }, {
                    name: "Tangible Asset BV/Share",
                    key: "tbvps"
                }
            ]
            ];
        let resultHead = [["Confidence Meter", "Sentiment", "Price", "Target Price"],
            ["P/E", "P/BV", "Price/Sales", "EV/EBITDA"],
            ["RoE", "Profit Margin", "Dividend Yield", "Dividend Payout Ratio"],
            ["FCF/Share", "Current Ratio", "Debt/Equity", "Tangible Asset BV/Share"]];
        let resultDataKey = [["confidencemeter", "", "price", "targetprice"],
            ["pe1", "pb", "ps1", "evebitda"],
            ["roe", "netmargin", "divyield", "payoutratio"],
            ["fcfps", "currentratio", "de", "tbvps"]];

        let results = [];

        let upIcon = (<i className="caret up icon"></i>);
        let downIcon = (<i className="caret down icon"></i>);
        let result;
        if (this.props.resultData && this.props.resultData.length > 0) {
            result = this.props.resultData.map(function (item, i) {
                console.log(item[resultHeads[this.props.selectedTab][3].key]);
                return (
                    <Link to={"/stock-details/" + item.code} key={i}>
                        <div className="each-stock-container">
                            <div className="stock-name-container">
                                <div className="stock-code">
                                    <span>{item.code}</span>
                                </div>
                                <div className="stock-name">
                                    {item.company}
                                </div>
                            </div>
                            <div className="stock-confidence-meter-container">
                                <div className="stock-confidence-meter">
                                    {(isNaN(item[resultHeads[this.props.selectedTab][0].key])) || ((item[resultHeads[this.props.selectedTab][0].key]) === null) ? (item[resultHeads[this.props.selectedTab][0].key] = "N.A.") : (item[resultHeads[this.props.selectedTab][0].key] = parseFloat(item[resultHeads[this.props.selectedTab][0].key]).toFixed(3))}
                                </div>
                            </div>
                            <div className="stock-sentiment-container">
                                <div
                                    className={(this.props.selectedTab == 0 ? "stock-sentiment" + (((item % 2 == 0) ? " green" : " red")) : "stock-confidence-meter")}>
                                    {this.props.selectedTab == 0 ? ((item % 2 == 0) ? upIcon : downIcon) :
                                        (((isNaN(item[resultHeads[this.props.selectedTab][1].key])) || ((item[resultHeads[this.props.selectedTab][1].key]) === null)) ? (item[resultHeads[this.props.selectedTab][1].key] = "N.A.") : (item[resultHeads[this.props.selectedTab][1].key] = parseFloat(item[resultHeads[this.props.selectedTab][1].key]).toFixed(3)))}
                                </div>
                            </div>
                            <div className="stock-price-container">
                                <div className="stock-price">
                                    {(isNaN(item[resultHeads[this.props.selectedTab][2].key])) || ((item[resultHeads[this.props.selectedTab][2].key]) === null) ? (item[resultHeads[this.props.selectedTab][2].key] = "N.A.") : (item[resultHeads[this.props.selectedTab][2].key] = parseFloat(item[resultHeads[this.props.selectedTab][2].key]).toFixed(3))}
                                </div>
                            </div>
                            <div className="stock-target-price-container">
                                <div
                                    className={"stock-target-price" + (this.props.selectedTab == 0 ? ((item % 2 == 0) ? " red" : " green") : "")}>
                                    {(isNaN(item[resultHeads[this.props.selectedTab][3].key])) || ((item[resultHeads[this.props.selectedTab][3].key]) === null) ? (item[resultHeads[this.props.selectedTab][3].key] = "N.A.") : (item[resultHeads[this.props.selectedTab][3].key] = parseFloat(item[resultHeads[this.props.selectedTab][3].key]).toFixed(3))}
                                </div>
                            </div>
                            <div className="stock-favourite-icon-container">
                                <div className="stock-favourite-icon">
                                    <i className="empty star icon"></i>
                                </div>
                            </div>
                        </div>
                    </Link>
                )
            }.bind(this));
        }
        else {
            result = (
                <div className="ui grid">
                    <div className="sixteen wide column">
                        <div className="ui result_row_loader">
                            <div className="ui active inverted dimmer">
                                <div className="ui centered text loader">Loading</div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <div className="stock-main-container">
                <div className="ui centered grid stock_heading_section">
                    <div className="sixteen wide computer sixteen wide tablet fifteen wide mobile column">
                        <div className="row">
                            <div className="stock-row-heading-container">
                                <div className="each-stock-heading-container">
                                    <div className="head-name"><br />
                                    </div>
                                    <div className="head-confidence-meter"
                                         onClick={this.sortBy.bind(this, resultHeads[this.props.selectedTab][0].key)}>
                                        {resultHeads[this.props.selectedTab][0].name}
                                    </div>
                                    <div className="head-sentiment"
                                         onClick={this.sortBy.bind(this, resultHeads[this.props.selectedTab][1].key)}>
                                        {resultHeads[this.props.selectedTab][1].name}
                                    </div>
                                    <div className="head-price"
                                         onClick={this.sortBy.bind(this, resultHeads[this.props.selectedTab][2].key)}>
                                        {resultHeads[this.props.selectedTab][2].name}
                                    </div>
                                    <div className="head-target-price"
                                         onClick={this.sortBy.bind(this, resultHeads[this.props.selectedTab][3].key)}>
                                        {resultHeads[this.props.selectedTab][3].name}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {result}
            </div>
        );
    }
}

export default InitialSearchResultRow;