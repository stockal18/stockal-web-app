/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from 'react';

/**
 * Search Class Definition
 */
class Detail extends Component {
    constructor(props) {
        super(props);
    }

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Set the defaultProps
     * @type {{initialCount: number, title: string, todoSegmentTitle: string}}
     */
    static defaultProps = {
        title: 'Stockal'
    };

    /**
     * App Rendering Starts Here
     * @returns {XML}
     */
    render() {
        return (
            <div title={this.props.title} className="iframe-detail">
                <iframe src="http://ir.stockal.com/"></iframe>
            </div>
        );
    }
}

export default Detail;