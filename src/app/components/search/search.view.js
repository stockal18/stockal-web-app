/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from 'react';

/**
 * Search Class Definition
 */
class Search extends Component {
    /**
     * Constructor
     * @param props - properties
     */
    constructor(props) {
        super(props);
    }

    /**
     * PropTypes
     * @type {{initialCount: *, title: *, todoSegmentTitle: *}}
     */
    static propTypes = {
        title: React.PropTypes.string
    };

    /**
     * Set the defaultProps
     * @type {{initialCount: number, title: string, todoSegmentTitle: string}}
     */
    static defaultProps = {
        title: 'Stockal'
    };

    /**
     * App Rendering Starts Here
     * @returns {XML}
     */
    render() {
        return (
            <div title={this.props.title}>
                <div className="ui centered grid ">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Search;