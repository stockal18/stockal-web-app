/**
 * Created by prayag on 23/11/16.
 */

import React, {Component} from 'react';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlight from 'autosuggest-highlight';

function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function getSuggestions(value) {
    const escapedValue = escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
        return [];
    }
    const regex = new RegExp('\\b' + escapedValue, 'i');
    return stockList.filter(stockList => regex.test(`${stockList.code}, ${stockList.company}`));
}

function getSuggestionValue(suggestion) {
    return `${suggestion.code}`;
}

function renderSuggestion(suggestion, { query }) {
    const suggestionText = `${suggestion.code}, ${suggestion.company}`;
    const matches = AutosuggestHighlight.match(suggestionText, query);
    const parts = AutosuggestHighlight.parse(suggestionText, matches);
    return (
        <span className="name">
        {
            parts.map((part, index) => {
                const className = part.highlight ? 'highlight' : null;

                return (
                    <span className={className} key={index}>{part.text}</span>
                );
            })
        }
        </span>
    );
}

/* Search className */
class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            suggestions: getSuggestions(''),
            search: false,
            selectedCode : ''
        };
    }
    onChange = (event, { newValue, method }) => {
        this.setState({
            value: newValue
        });
    };
    onSuggestionsUpdateRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };


    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected = (event, { suggestion, suggestionValue, sectionIndex, method }) => {
        this.setState({
            search: true,
            selectedCode : suggestionValue
        });
    }

    render(){
        return (
            <div className="row">
                <div className="search-container">
                    <div className="ui grid">
                        <div className="twelve wide computer twelve wide tablet sixteen wide mobile column">
                            <input type="text" placeholder="Enter stock symbol, sector or decision indicator"/>
                        </div>
                        <div className="four wide computer four wide tablet only column">
                            <input type="button" value="SEARCH"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchBox;