/**
 * Created by prayag on 06/10/16.
 */

import React from 'react';
/**
 * EnterpriseFormInput Class
 */

class EnterpriseInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
        }
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            errorMessage: nextProps.errorMessage
        })
    }

    validateField(){
        let currentInput = this.refs.input;
        const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (currentInput.value.trim().length === 0) {
            this.setState({
                errorMessage : "Please fill this in"
            });
            currentInput.focus();
            return false;
        } else if (this.props.fieldType === "email" && !regexEmail.test(currentInput.value.trim())){
            this.setState({
                errorMessage : "Email is not valid"
            });
            currentInput.focus();
            return false;
        }
        this.setState({
            errorMessage : ""
        });
        return true;
    }

    render() {
        return (
            <div>
                <input type="text" placeholder={this.props.placeholder} className="input_boxes" ref="input"/>
                <div className="row">
                    <span className="popup_form_error">{this.state.errorMessage}</span>
                </div>
            </div>
        );
    }
}
export default EnterpriseInput;
