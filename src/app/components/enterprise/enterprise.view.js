/**
 * Created by Rajasekaran on 23/09/16.
 */

import React from 'react';
import IconOne from  './../../../public/assets/images/enterprise_icon1.png';
import IconTwo from  './../../../public/assets/images/enterprise_icon2.png';
import IconThree from  './../../../public/assets/images/enterprise_icon3.png';
import IconFour from  './../../../public/assets/images/enterprise_icon4.png';
import PictureOne from  './../../../public/assets/images/enterprise_bigpic1.png';
import PictureTwo from  './../../../public/assets/images/enterprise_bigpic2.png';
import PictureThree from  './../../../public/assets/images/enterprise_bigpic3.png';
import EnterpriseInput from './enterprise.input.view';
import EnterpriseForm from './enterprise.form.view';
import APIClient from './../../api/api.client';

/**
 * Enterprise Class
 */
class Enterprise extends React.Component {
    constructor() {
        super();
        this.state = {
            showPopup: false,
            sendButtonText: "Send",
            message: ""
        };
    }
    showPopup() {
        this.setState({
            showPopup: true
        });
    }
    sendContactMail(){
        let validField = 0;
        for(var i = 1; i >= 0; i--){
            validField += this.refs["input"+i].validateField();
        }
        if(validField === 2){
            this.setState({
                sendButtonText: "Sending..."
            });
            let data = {
                name: this.refs.input0.refs.input.value,
                email: this.refs.input1.refs.input.value,
            };
            APIClient.sendContactMail(data).then((data) => {
                this.setState({
                    sendButtonText: "Sent",
                    message:  (data.status === 200) ? "Thanks for getting in touch! Expect an answer from us very soon."
                        : "Oops! Looks like the server did not respond as expected. Pls try again after some time."
                });
                setTimeout(() => {
                    this.setState({
                        sendButtonText: "Send",
                        message: ""
                    });
                    this.refs.input0.refs.input.value="";
                    this.refs.input1.refs.input.value="";
                }, 3000)
            });
        }
    }
    render() {
        return (
            <div className="ui grid">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="ui grid enterprise_section">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="ui middle aligned centered grid">
                                        <div className="fourteen wide column">
                                            <div className="row">
                                                <div className="enterprise_title">
                                                    Build Next-Gen Fintech Products
                                                </div>
                                                <div className="enterprise_subtitle">
                                                    Powered by Stockal APIs for unique,<br />
                                                    investing and research, insights
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui grid contact_section">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="ui middle aligned centered grid">
                                        <div className="fourteen wide column">
                                            <div className="row">
                                                <div className="contact_title">Contact us to know more
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui grid centered contact_section">
                            <div className="four wide computer five wide tablet fourteen wide mobile column">
                                <EnterpriseInput placeholder="Your Name" ref="input0" fieldType="text"/>
                            </div>
                            <div className="six wide computer six wide tablet fourteen wide mobile column">
                                <EnterpriseInput placeholder="Your Email Address" ref="input1" fieldType="email"/>
                            </div>
                            <div className="four wide computer three wide tablet fourteen wide mobile column">
                                <button type="submit" value="send" className={"sub_button " + (this.state.sendButtonText !="Send" ? "button-disabled" : "")} onClick={this.sendContactMail.bind(this)}> {this.state.sendButtonText}</button>
                            </div>
                            <div className={"slider-message " + (this.state.message ? "slider-message-active" : "")}>{this.state.message}</div>
                        </div>
                        <div className="ui centered grid section">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="section_title">Smart Analysis</div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid">
                            <div className="ten wide computer fifteen wide tablet fifteen wide mobile column">
                                <div className="row">
                                    <div className="section_subtitle">
                                        Thousands of investors are using decision-support signals
                                        and data from Stockal for making better & smarter investing choices.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui grid">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="section_italic"> Your clients and users can benefit too
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui centered grid">
                            <div className="three wide computer three wide tablet eight wide mobile column">
                                <div className="icon"><img src={IconOne}/><h5>Analyst Openion</h5></div>
                            </div>
                            <div className="three wide computer three wide tablet eight wide mobile column">
                                <div className="icon"><img src={IconTwo}/><h5>Stock Sentiment</h5></div>
                            </div>
                            <div className="three wide computer three wide tablet eight wide mobile column">
                                <div className="icon"><img src={IconThree}/><h5>Curated Social Feeds</h5></div>
                            </div>
                            <div className="three wide computer three wide tablet eight wide mobile column">
                                <div className="icon"><img src={IconFour}/><h5>News/Analyst Articles</h5></div>
                            </div>
                        </div>
                        <div className="ui centered grid section">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="section_italic"> Insights from over 6,000 stocks on NYSE/NASDAQ
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui centered grid section bg-pink">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="section_title">A Big Picture</div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid bg-pink">
                            <div className="ten wide computer fifteen wide tablet fifteen wide mobile column">
                                <div className="row">
                                    <div className="section_subtitle">
                                        Keep your users informed smartly with a holistic
                                        view of the markets using Stockal APIs.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid section section-margin bg-pink">
                            <div className="six wide computer seven wide tablet eight wide mobile column">
                                <div className="row">
                                    <div className="section_heading">Analysts’ Aggregated View</div>
                                    <br />
                                    <div className="section_content">
                                        What do top Wall St. Analysts and banks think about a stock
                                        you are interested in? <br /><br />
                                        "Confidence Meter” from Stockal gives the aggregated expert opinion for any
                                        stock. Also see how the Confidence Meter has changed historically & if it has
                                        impacted prices
                                    </div>
                                </div>
                            </div>
                            <div className="five wide computer six wide tablet seven wide mobile column">
                                <div className="row">
                                    <div className="section_picture"><img src={PictureOne}/></div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid section section-margin bg-pink">
                            <div className="five wide computer six wide tablet seven wide mobile column">
                                <div className="row">
                                    <div className="section_picture"><img src={PictureTwo}/></div>
                                </div>
                            </div>
                            <div className="six wide computer seven wide tablet eight wide mobile column">
                                <div className="row">
                                    <div className="section_heading">Traders’ Collective Wisdom</div>
                                    <br />
                                    <div className="section_content">What to expect from traders’ actions and
                                        conversations.<br /><br />
                                        Which way is the crowd moving – every few hours. Crowd sentiment anyone?
                                        Know the directions in which other investor’s emotions are pointing.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid section section-margin bg-pink">
                            <div className="six wide computer seven wide tablet eight wide mobile column">
                                <div className="row">
                                    <div className="section_heading">Curated, not just aggregated, data</div>
                                    <br />
                                    <div className="section_content">
                                        There are gazillion content aggregators out there but
                                        very few of them add layers of intelligence
                                        for stock news and analysis the way Stockal does.<br /><br />
                                        Conversations, news bytes and analyst articles – curated for your users’
                                        reading and research purposes.<br /><br />
                                        No more scouring through tons of documents to figure out what’s happening
                                    </div>
                                </div>
                            </div>
                            <div className="five wide computer six wide tablet seven wide mobile column">
                                <div className="row">
                                    <div className="section_picture"><img src={PictureThree}/></div>
                                </div>
                            </div>
                        </div>
                        <div className="ui middle aligned centered grid">
                            <div className="sixteen wide column">
                                <div className="row">
                                    <div className="ui middle aligned centered grid">
                                        <div className="sixteen wide column">
                                            <div className="row">
                                                <div className="popup_link" onClick={this.showPopup.bind(this)}>
                                                    Interesting! I'd would like to know more...
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <EnterpriseForm showPopup={this.state.showPopup}/>
                    </div>
                </div>
            </div>
        );
    }
}
export default Enterprise;