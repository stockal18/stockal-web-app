/**
 * Created by Rajasekaran on 30/09/16.
 */

import React from 'react';
import EnterpriseFormInput from "./enterprise.form.input.view";
import APIClient from "./../../api/api.client";
import CloseImg from  './../../../public/assets/images/closebtnimg.png';
var Scroll  = require('react-scroll');

/**
 * EnterpriseForm Class
 */

class EnterpriseForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPopup: this.props.showPopup,
            selectedQuestion: 0,
            submitButtonText: "Submit",
            popupMessage: ""
        }
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            showPopup: nextProps.showPopup
        });
        document.getElementsByTagName("body")[0].style.overflow = nextProps.showPopup ? "hidden" : "";
        this.refs.input0.refs.input.autofocus = true;
    }
    closePopup() {
        this.setState({
            showPopup: false,
            selectedQuestion: 0
        });
        for(var i = 0; i < 5; i++){
            document.getElementsByClassName("qInput")[i].value = "";
        }
        document.getElementsByTagName("body")[0].style.overflow="";
    }
    onFocus(selectedQuestion){
        this.setState({
            selectedQuestion: selectedQuestion
        });
        this.scrollTo(selectedQuestion);
    }
    onSubmit(){
        let validField = 0;
        for(var i = 4; i >= 0; i--){
            validField += this.refs["input"+i].validateField(this.refs["input"+i].refs.input, true);
        }
        if(validField === 5){
            this.setState({
                submitButtonText: "Submitting..."
            })
            let data = {
                name: this.refs.input0.refs.input.value,
                company: this.refs.input1.refs.input.value,
                email: this.refs.input2.refs.input.value,
                country: this.refs.input3.refs.input.value,
                desc: this.refs.input4.refs.input.value
            }
            APIClient.sendPopupContactMail(data).then((data) => {
                this.setState({
                    submitButtonText: "Submitted",
                    popupMessage:  (data.status === 200) ? "Thanks for getting in touch! Expect an answer from us very soon."
                        : "Oops! Looks like the server did not respond as expected. Pls try again after some time."
                })
                setTimeout(() => {
                    this.closePopup();
                    this.setState({
                        submitButtonText: "Submit",
                        popupMessage: ""
                    })
                }, 3000)
            });

        }
    }

    scrollTo(selectedQuestion){
        var Element = Scroll.Element;
        var scroller = Scroll.scroller;
        var scroll = Scroll.animateScroll;
        scroll.scrollTo(this.refs["input" + selectedQuestion].refs.input.offsetTop - this.refs.input0.refs.input.offsetTop, {
            containerId:"enterprise"
        });
    }

    handleEscKey(event) {
        if(event.keyCode==27){
            this.closePopup();
        }
    }

    render() {
        return (
            <div className={"popup-overlay" + (this.state.showPopup ? " enterprise_popup_show": "" )} id="enterprise" onKeyDown={this.handleEscKey.bind(this)}>
                <div className="ui middle aligned centered grid">
                    <div className="sixteen wide column">
                        <div className="row">
                            <div className="ui centered grid">
                                <div className="sixteen wide column">
                                    <div className="row">
                                        <div className="closeimg" onClick={this.closePopup.bind(this)}>
                                            <img src={CloseImg}/>
                                        </div>
                                        <div className="ui centered grid countContainer">
                                            <div className={"qCircle" + (this.state.selectedQuestion == 0 ? " activeQ" : "")}>5</div>
                                            <div className={"qCircle" + (this.state.selectedQuestion == 1 ? " activeQ" : "")}>4</div>
                                            <div className={"qCircle" + (this.state.selectedQuestion == 2 ? " activeQ" : "")}>3</div>
                                            <div className={"qCircle" + (this.state.selectedQuestion == 3 ? " activeQ" : "")}>2</div>
                                            <div className={"qCircle" + (this.state.selectedQuestion == 4 ? " activeQ" : "")}>1</div>
                                        </div>
                                        <div className="ui centered grid">
                                            <div className="eight wide computer twelve wide tablet fifteen wide mobile column">
                                                <EnterpriseFormInput fieldType="text" onFocus={this.onFocus.bind(this, 0)}
                                                                     ref="input0" label="1. Please tell us your name"
                                                                     opacityClass={this.state.selectedQuestion == 0 ? "active" : ""}/>
                                                <EnterpriseFormInput fieldType="text" onFocus={this.onFocus.bind(this, 1)}
                                                                     ref="input1" label="2. Company Name"
                                                                     opacityClass={this.state.selectedQuestion == 1 ? "active" : ""}/>
                                                <EnterpriseFormInput fieldType="email" onFocus={this.onFocus.bind(this, 2)}
                                                                     ref="input2" label="3. Your Email (official email preferred)"
                                                                     opacityClass={this.state.selectedQuestion == 2 ? "active" : ""}/>
                                                <EnterpriseFormInput fieldType="text" onFocus={this.onFocus.bind(this, 3)}
                                                                     ref="input3" label="4. Which country are you in?"
                                                                     opacityClass={this.state.selectedQuestion == 3 ? "active" : ""}/>
                                                <EnterpriseFormInput fieldType="textarea" onFocus={this.onFocus.bind(this, 4)}
                                                                     ref="input4" label="5. Tell us what you are trying to build or how
                                                                     you plan to use Stockal APIs"
                                                                     opacityClass={this.state.selectedQuestion == 4 ? "active" : ""}/>
                                                <div className=" qRow">
                                                    <div className="questionBtn">
                                                        <button type="button" className={"ui teal button " + (this.state.submitButtonText !="Submit" ? "button-disabled" : "")} onClick={this.onSubmit.bind(this)}>
                                                            {this.state.submitButtonText}
                                                        </button>
                                                        <div className="row">{this.state.popupMessage}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default EnterpriseForm;
