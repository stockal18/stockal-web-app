/**
 * Created by prayag on 06/10/16.
 */

import React from 'react';
/**
 * EnterpriseFormInput Class
 */

class EnterpriseFormInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: "",
            opacityClass: this.props.opacityClass
        }
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            opacityClass : nextProps.opacityClass
        });
    }
    handleKeyDown(event) {
        if(event.currentTarget.value.trim().length > 0) {
            this.setState({
                errorMessage : ""
            });
        }
        if((event.keyCode==13  && !event.shiftKey) || event.keyCode==40 || (event.keyCode==9 && !event.shiftKey)){
            event.preventDefault();
            var currentInput = event.currentTarget;
            this.validateField(currentInput, false);
        }
    }

    validateField(currentInput, onSubmitClick){
        let inputArr = document.getElementsByClassName(currentInput.className);
        let formButton = document.getElementsByClassName("questionBtn")[0].children[0];
        const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (currentInput.value.trim().length === 0) {
            this.setState({
                errorMessage : "Please fill this in",
                opacityClass : "active"
            });
            currentInput.focus();
            return false;
        } else if (this.props.fieldType === "email" && !regexEmail.test(currentInput.value.trim())){
            this.setState({
                errorMessage : "Email is not valid",
                opacityClass : "active"
            });
            currentInput.focus();
            return false;
        } else {
            let nextInput = inputArr[Array.prototype.indexOf.call(inputArr, currentInput) + 1] ? inputArr[Array.prototype.indexOf.call(inputArr, currentInput) + 1] : formButton;
            if(!onSubmitClick) {
                nextInput.focus();
            }
            this.setState({
                opacityClass : ""
            });
            return true;
        }
    }
    render() {
        let field = (
            <input type="text" className="qInput" onKeyDown={this.handleKeyDown.bind(this)}
                   onFocus={this.props.onFocus} ref="input"/>
        )
        if(this.props.fieldType==="textarea"){
            field = (
                <textarea className="qInput" onKeyDown={this.handleKeyDown.bind(this)}
                       onFocus={this.props.onFocus} ref="input"/>
            )
        }

        return (
            <div className={"row question-content " + this.state.opacityClass}>
                <div className="row qRow">
                    <div className="questionsection">
                        {this.props.label}
                    </div>
                </div>
                <div className="row">
                    <div className="questioninput">
                        <div className="row">
                            {field}
                            <span>press <span className="input_enter">Enter</span></span>
                        </div>
                        <div className="row">
                            <span className="popup_form_error">{this.state.errorMessage}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default EnterpriseFormInput;
