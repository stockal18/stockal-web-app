/**
 * Created by Rajasekaran on 16/11/16.
 */

import React from 'react';
import PageNotFoundImage from './../../../public/assets/images/pagenotfound.png';
import Autosuggest from 'react-autosuggest';
import AutosuggestHighlight from 'autosuggest-highlight';
import {Link} from 'react-router';
var data = require('./../../../public/assets/others/newSymbolsListLatest.csv');
var stockList = [];
for (var i = 1; i < data.length; i++) {
    var obj = {};
    obj = {code: data[i][0], company: data[i][1]}
    stockList.push(obj);
}

function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function getSuggestions(value) {
    const escapedValue = escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
        return [];
    }
    const regex = new RegExp('\\b' + escapedValue, 'i');
    return stockList.filter(stockList => regex.test(`${stockList.code}, ${stockList.company}`));
}

function getSuggestionValue(suggestion) {
    return `${suggestion.code}`;
}

function renderSuggestion(suggestion, {query}) {
    const suggestionText = `${suggestion.code}, ${suggestion.company}`;
    const matches = AutosuggestHighlight.match(suggestionText, query);
    const parts = AutosuggestHighlight.parse(suggestionText, matches);
    return (
        <span className="name">
        {
            parts.map((part, index) => {
                const className = part.highlight ? 'highlight' : null;

                return (
                    <span className={className} key={index}>{part.text}</span>
                );
            })
        }
        </span>
    );
}

/* Search className */
class PageNotFound extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            suggestions: getSuggestions(''),
            search: false,
            selectedCode: ''
        };
    }

    onChange = (event, {newValue, method}) => {
        this.setState({
            value: newValue
        });
    };
    onSuggestionsUpdateRequested = ({value}) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };


    onSuggestionsFetchRequested = ({value}) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected = (event, {suggestion, suggestionValue, sectionIndex, method}) => {
        this.setState({
            search: true,
            selectedCode: suggestionValue
        });
    }

    /**
     * Render Header Component
     * @returns {XML}
     */
    render() {
        const {value, suggestions} = this.state;
        const inputProps = {
            placeholder: "Enter symbol or company name",
            value,
            onChange: this.onChange
        };

        let search = (
            <div className="search_btn">SEARCH</div>
        )

        if (this.state.search) {
            search = (
                <Link to={"/stock-details/" + this.state.selectedCode}>
                    <div className="search_btn">SEARCH</div>
                </Link>
            )
        }
        return (
            <div className="ui grid">
                <div className="sixteen wide column">
                    <div className="row">
                        <div className="ui centered grid">
                            <div className="five wide computer nine wide tablet fifteen wide mobile column error_img_section">
                                    <img src={PageNotFoundImage}/>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="ui centered grid error_info">
                            <div className="eight wide computer ten wide tablet fifteen wide mobile column">
                                <div className="row">
                                    <div className="error_heading"> Oops!</div>
                                    <div className="error_desc">
                                        Looks like we do not have enough information for that stock.
                                        <br /><br />
                                        If it’s genuine stock, we should have some data in a couple of days. Care to
                                        come back, then? Or search again, below.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="ui centered grid stock_search_section">
                            <div className="eight wide computer ten wide tablet fifteen wide mobile column">
                                <div className="row">
                                    <div className="stock_search_heading">
                                        Search for another Stock
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="sixteen wide column stock_search_input">
                                    <Autosuggest
                                        suggestions={suggestions}
                                        onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested}
                                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                        onSuggestionSelected={this.onSuggestionSelected}
                                        getSuggestionValue={getSuggestionValue}
                                        renderSuggestion={renderSuggestion}
                                        focusFirstSuggestion={true}
                                        inputProps={inputProps}
                                    />
                                    {search}
                                </div>
                                    </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}
export default PageNotFound;
