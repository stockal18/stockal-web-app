/**
 * Created by sarovar on 11/07/16.
 */
/**
 * Class TodoStore Definition
 */
var axios = require('axios');

/**
 * Public Utility Functions
 * @type {{getTrendingStocks: helpers.getTrendingStocks}}
 */
var ApiClient = {
    /**
     * Get Trending Stocks
     */
    getTrendingStocks: function () {
        return axios.get("/api/trending")
        .then(function (response) {
            return {
                status: response.status,
                docs: response.data.docs
            }
        })
        .catch(function (error) {
            return {
                status: 500,
                error: error
            }
        });
    },
    getRatioTable: function (code) {
        return axios.get("/api/ratiotable/" + code)
            .then(function (response) {
                return {
                    status: response.status,
                    docs: response.data.docs
                }
            })
            .catch(function (error) {
                return {
                    status: 500,
                    error: error
                }
            });
    },
    getRecommend: function () {
        return axios.post("/api/recommendation")
            .then(function (response) {
                return {
                    status: response.status,
                    docs: response.data.docs
                }
            })
            .catch(function (error) {
                return {
                    status: 500,
                    error: error
                }
            });
    },
    getSignals: function (code) {
        return axios.get("/api/signals/" + code)
        .then(function (response) {
            if(response.data.docs.length === 0) {
                window.location.pathname = '/error404';
            }
            else {
                return {
                    status: response.status,
                    docs: response.data.docs,
                }
            }
        })
        .catch(function (error) {
            return {
                status: 500,
                error: error
            }
        });
    },
    getInfo: function (code) {
        return axios.get("/api/info/" + code)
        .then(function (response) {
            return {
                status: response.status,
                info: response.data.info
            }
        })
        .catch(function (error) {
            return {
                status: 500,
                error: error
            }
        });
    },


    sendContactMail: function (data) {
        return axios(
            {
                method: 'post',
                url: '/api/sendContactMail',
                data: data,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(function (response) {
                return {
                    status: response.status,
                    message: response.data.message
                }
            })
            .catch(function (error) {
                return {
                    status: 500,
                    error: error
                }
            });

    },
    sendPopupContactMail: function (data) {
        return axios(
            {
                method: 'post',
                url: '/api/sendPopupContactMail',
                data: data,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        .then(function (response) {
            return {
                status: response.status,
                message: response.data.message
            }
        })
        .catch(function (error) {
            return {
                status: 500,
                error: error
            }
        });
    },
    socialLogin: function (data) {
        return axios({
            method: 'post',
            url: '/api/social-login',
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(function (response) {
            return {
                data: response.data,
                referralToken: response.data.referralToken,
                registeredCount: response.data.registeredCount
            }
        })
        .catch(function (error) {
            return {
                status: 500,
                error: error
            }
        });
    },
    getFilters: function () {
        return axios.get("/api/filters")
            .then(function (response) {
                return {
                    status: response.status,
                    docs: response.data.docs
                }
            })
            .catch(function (error) {
                return {
                    status: 500,
                    error: error
                }
            });
    },
    getAllFilters: function () {
        return axios.get("/api/allFilters")
            .then(function (response) {
                return {
                    status: response.status,
                    docs: response.data.docs
                }
            })
            .catch(function (error) {
                return {
                    status: 500,
                    error: error
                }
            });
    },
    getSearchResults: function (data) {
        return axios({
            method: 'post',
            url: '/api/search',
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(function (response) {
            return {
                data: response.data.docs,
                status: response.status
            }
        })
        .catch(function (error) {
            return {
                status: 500,
                error: error
            }
        });
    },
    getFullSearchResults: function (data) {
        return axios({
            method: 'post',
            url: '/api/fullsearch',
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(function (response) {
            return {
                data: response.data.docs,
                status: response.status
            }
        })
        .catch(function (error) {
            return {
                status: 500,
                error: error
            }
        });
    }

};

module.exports = ApiClient;