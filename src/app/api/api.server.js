var request = require('request');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    host: 'md-43.webhostbox.net',
    port: 465,
    secure: true,
    auth: {
        user: 'prayag@stockal.com',
        pass: 'prayag@8765'
    }
}));

//registeredCount
var registeredCount = 9036;

exports.getTrending = function (req, res) {
    var options = {
        url: 'https://www.stockalapi.com/api/streams/symbols/2/trending/16',
        headers: {
            'Accept': 'application/json'
        },
    };
    var data = '';
    request.get(options).on('data', function (chunk) {
        data += chunk;
    }).on("end", function () {
        try {
            data = JSON.parse(data);
            res.send(data).end();
        }
        catch (err) {
            res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
        }
    }).on('error', function (err) {
        res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
    });
};
exports.getRatioTable = function (req, res) {
    var code = req.params.code;
    var options = {
        url: 'https://www.stockalapi.com/api/streams/symbols/3/ratio/' + code,
        headers: {
            'Accept': 'application/json'
        },
    };
    var data = '';
    request.get(options).on('data', function (chunk) {
        data += chunk;
    }).on("end", function () {
        try {
            data = JSON.parse(data);
            res.send(data).end();
        }
        catch (err) {
            res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
        }
    }).on('error', function (err) {
        res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
    });
};
exports.getRecommend = function (req, res) {
    var options = {
        url: 'https://www.stockalapi.com/api/streams/reco/get/56a21da9431ddd457314626e',
        headers: {
            'stockal-secret-key': 'CSeeVMAcZwo60ZHRlvhSeD8gwviaF1FfECbRAd3mKb1wNccCWF',
            'Accept': 'application/json, text/plain, /',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: {symbols: JSON.stringify(['AAPL', 'TSLA', 'JPM', 'AA'])}
    };
    var data = '';
    request.post(options).on('data', function (chunk) {
        data += chunk;
    }).on("end", function () {
        try {
            data = JSON.parse(data);
            res.send(data).end();
        }
        catch (err) {
            res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
        }
    }).on('error', function (err) {
        res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
    });
};

exports.getSignals = function (req, res) {
    var code = req.params.code;
    var options = {
        url: 'https://www.stockalapi.com/api/streams/symbols/2/signals/' + code,
        headers: {
            'Accept': 'application/json'
        },
    };
    var data = '';
    request.get(options).on('data', function (chunk) {
        data += chunk;
    }).on("end", function () {
        try {
            data = JSON.parse(data);
            res.send(data).end();
        }
        catch (err) {
            res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
        }
    }).on('error', function (err) {
        res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
    });
};

exports.getInfo = function (req, res) {
    var code = req.params.code;
    var options = {
        url: 'https://www.stockalapi.com/api/streams/symbols/2/info/all/' + code + '/limit/20',
        headers: {
            'Accept': 'application/json'
        },
    };
    var data = '';
    request.get(options).on('data', function (chunk) {
        data += chunk;
    }).on("end", function () {
        try {
            data = JSON.parse(data);
            res.send(data).end();
        }
        catch (err) {
            res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
        }
    }).on('error', function (err) {
        res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
    });
};

exports.contactUs = function (req, res) {
    //res.send({status: 200, docs: req.body}).end();
    var htmlContent = '<div style="font-family: calibri;font-size:12px"> <pre><br>'
        + 'Name: ' + req.body.name + '<br>' + 'Email: ' + req.body.email + '<br>' +
        '</pre></div>';
    transporter.sendMail({
        from: req.body.name + " <prayag@stockal.com>",
        to: "prayag@stockal.com",
        subject: 'Enterprise user info(Know more)',
        html: htmlContent
    }, function (err, info) {
        if (err) {
            //callback(false);
            res.send({status: 500, message: 'fail'}).end();
        }
        else {
            res.send({status: 200, message: 'success'}).end();
        }
    });
};
exports.popupContactUs = function (req, res) {
    //res.send({status: 200, docs: req.body}).end();
    var htmlContent = '<div style="font-family: calibri;font-size:12px"> <pre><br>'
        + 'Name: ' + req.body.name + '<br>'
        + 'Company: ' + req.body.company + '<br>'
        + 'Email: ' + req.body.email + '<br>'
        + 'Country: ' + req.body.country + '<br>'
        + 'Query: ' + req.body.desc + '<br>'
        + '</pre></div>';
    transporter.sendMail({
        from: req.body.name + " <prayag@stockal.com>",
        to: "prayag@stockal.com",
        subject: 'Enterprise user info',
        html: htmlContent
    }, function (err, info) {
        if (err) {
            //callback(false);
            res.send({status: 500, message: 'fail'}).end();
        }
        else {
            res.send({status: 200, message: 'success'}).end();
        }
    });
};
exports.socialLogin = function (req, res) {
    var options = {
        url: 'https://www.stockalapi.com/api/user/3/social-login',
        headers: {
            'Accept': 'application/json',
            "stockal-secret-key": "CSeeVMAcZwo60ZHRlvhSeD8gwviaF1FfECbRAd3mKb1wNccCWF"
        },
        form: req.body
    };
    var data = '';
    request.post(options).on('data', function (chunk) {
        data += chunk;
    }).on("end", function () {
        try {
            registeredCount += Math.floor(Math.random() * 10) + 5;
            data = JSON.parse(data);
            data.registeredCount = registeredCount;
            res.send(data).end();
        }
        catch (err) {
            res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
        }
    }).on('error', function (err) {
        res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
    });
};
exports.getFilters = function (req, res) {
    var options = {
        url: 'https://www.stockalapi.com/api/filters/getFilters',
        headers: {
            'Accept': 'application/json'
        },
    };
    var data = '';
    request.get(options).on('data', function (chunk) {
        data += chunk;
    }).on("end", function () {
        try {
            data = JSON.parse(data);
            res.send(data).end();
        }
        catch (err) {
            res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
        }
    }).on('error', function (err) {
        res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
    });
};
exports.getAllFilters = function (req, res) {
    var options = {
        url: 'https://www.stockalapi.com/api/filters/getAllFilters',
        headers: {
            'Accept': 'application/json'
        },
    };
    var data = '';
    request.get(options).on('data', function (chunk) {
        data += chunk;
    }).on("end", function () {
        try {
            data = JSON.parse(data);
            res.send(data).end();
        }
        catch (err) {
            res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
        }
    }).on('error', function (err) {
        res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
    });
};
exports.getSearchResults = function (req, res) {
    var options = {
        url: 'https://www.stockalapi.com/api/search/stock/20',
        //url: 'http://192.168.1.5:9000/api/search/stock/20',
        headers: {
            'Accept': 'application/json',
            "stockal-secret-key": "CSeeVMAcZwo60ZHRlvhSeD8gwviaF1FfECbRAd3mKb1wNccCWF"
        },
        form: {"filter" : JSON.stringify(req.body)}
    };
    var data = '';
    request.post(options).on('data', function (chunk) {
        data += chunk;
    }).on("end", function () {
        try {
            data = JSON.parse(data);
            res.send(data).end();
        }
        catch (err) {
            res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
        }
    }).on('error', function (err) {
        res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
    });
};
exports.getFullSearchResults = function (req, res) {
    var options = {
        //url: 'http://192.168.1.2:9000/api/search/stock/1/10',
        url: 'https://www.stockalapi.com/api/search/stock/1/10',
        headers: {
            'Accept': 'application/json',
            "stockal-secret-key": "CSeeVMAcZwo60ZHRlvhSeD8gwviaF1FfECbRAd3mKb1wNccCWF"
        },
        form: {"filter" : JSON.stringify(req.body)}
    };
    var data = '';
    request.post(options).on('data', function (chunk) {
        data += chunk;
    }).on("end", function () {
        try {
            data = JSON.parse(data);
            res.send(data).end();
        }
        catch (err) {
            res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
        }
    }).on('error', function (err) {
        res.send({status: 500, message: 'Oops! Something went wrong.'}).end();
    });
};