/**
 * Created by prayag on 11/08/16.
 */

import React from 'react';
import moment from 'moment';

export function getCalculatedTime(timestamp) {
    var seconds = parseInt(moment.utc().diff(moment.utc(timestamp).toDate(), 'seconds'));
    var minutes = parseInt(seconds / 60);
    var hours = parseInt(minutes / 60);
    var days = parseInt(hours / 24);
    var timestamp = '';
    if (seconds < 60)
        timestamp = seconds + " SECONDS AGO";
    else if (minutes === 1)
        timestamp = minutes + " MINUTE AGO";
    else if (minutes < 60)
        timestamp = minutes + " MINUTES AGO";
    else if (hours === 1)
        timestamp = hours + " HOUR AGO";
    else if (hours < 24)
        timestamp = hours + " HOURS AGO";
    else if (days === 1)
        timestamp = days + " DAY AGO";
    else if (days > 1)
        timestamp = days + " DAYS AGO";
    return timestamp
}

export function decodeEntities(encodedString) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = encodedString;
    return textArea.value;
}

